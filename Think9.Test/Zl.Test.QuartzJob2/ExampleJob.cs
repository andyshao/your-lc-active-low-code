﻿using Quartz;
using System;
using System.Threading.Tasks;

namespace Think9.Test.QuartzJob2
{
    public class ExampleJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Console.Out.WriteLineAsync("哈哈" + this.GetType().FullName);
        }
    }
}