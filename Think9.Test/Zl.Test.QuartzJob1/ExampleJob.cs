﻿using Quartz;
using System;
using System.Threading.Tasks;

namespace Think9.Test.QuartzJob1
{
    public class ExampleJob : IJob
    {
        public async Task Execute(IJobExecutionContext context)
        {
            await Console.Out.WriteLineAsync(this.GetType().FullName);
        }
    }
}