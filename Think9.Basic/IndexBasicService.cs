﻿using System;
using System.Collections.Generic;
using System.Data;
using Think9.Models;
using Think9.Services.Base;

namespace Think9.Services.Basic
{
    public class IndexBasicService : BaseService<IndexBasicEntity>
    {
        public List<IndexBasicEntity> GetIndexListBySearch(IEnumerable<dynamic> list, string tbid, string show)
        {
            ComService ComService = new ComService();

            string _tbname;
            List<IndexBasicEntity> newlist = new List<IndexBasicEntity>();

            string sql = "select * from sys_datatype";
            DataTable dtDataType = ComService.GetDataTable(sql);

            //sql = "select * from sys_sort where ClassID='CAT_index' ORDER BY SortOrder";
            //DataTable dtSort = ComService.GetDataTable(sql);

            //sql = @"SELECT a.TbId,a.IndexId,a.IndexName,b.TbName FROM tbindex a
            //               INNER JOIN tbbasic b ON a.TbId=b.TbId ORDER BY TbId ASC";
            //DataTable dtTbName = ComService.GetDataTable(sql);

            foreach (Object obj in list)
            {
                bool badd = false;
                if (show == "y")
                {
                    if (ComService.GetTotal("tbindex", "where TbId='" + tbid + "' and IndexId='" + ((IndexBasicEntity)obj).IndexId + "'") == 0)
                    {
                        badd = true;
                    }
                }
                else
                {
                    badd = true;
                }

                if (badd)
                {
                    IndexBasicEntity newmodel = new IndexBasicEntity();
                    newmodel.IndexDataType = ((IndexBasicEntity)obj).IndexDataType;
                    newmodel.IndexId = ((IndexBasicEntity)obj).IndexId;
                    newmodel.IndexName = ((IndexBasicEntity)obj).IndexName;
                    newmodel.IndexExplain = ((IndexBasicEntity)obj).IndexExplain;

                    //foreach (DataRow dr in dtDataType.Rows)
                    //{
                    //    if (dr["TypeId"].ToString() == ((IndexBasicEntity)obj).IndexDataType)
                    //    {
                    //        newmodel.DataTypeName = dr["TypeName"].ToString();
                    //        break;
                    //    }
                    //}

                    //分类
                    //foreach (DataRow dr in dtSort.Rows)
                    //{
                    //    if (dr["SortID"].ToString() == ((IndexBasicEntity)obj).IndexSort)
                    //    {
                    //        newmodel.IndexSort = dr["SortName"].ToString();
                    //        break;
                    //    }
                    //}

                    _tbname = "";
                    //foreach (DataRow dr in dtTbName.Select("IndexId='" + ((IndexBasicEntity)obj).IndexId + "'"))
                    //{
                    //    if (dr["IndexId"].ToString() == ((IndexBasicEntity)obj).IndexId)
                    //    {
                    //        _tbname += dr["TbName"].ToString() + "  ";
                    //        break;
                    //    }
                    //}
                    newmodel.TableName = _tbname;

                    newlist.Add(newmodel);
                }
            }

            return newlist;
        }

        public List<IndexBasicEntity> GetIndexListBySearch(ref long total, IEnumerable<dynamic> list, string tbid, string show)
        {
            ComService ComService = new ComService();

            string _tbname;
            List<IndexBasicEntity> newlist = new List<IndexBasicEntity>();

            string sql = "select * from sys_datatype";
            DataTable dt = ComService.GetDataTable(sql);

            sql = "select * from sys_sort where ClassID='CAT_index' ORDER BY SortOrder";
            DataTable dt2 = ComService.GetDataTable(sql);

            sql = @"SELECT a.TbId,a.IndexId,a.IndexName,b.TbName FROM tbindex a
                           INNER JOIN tbbasic b ON a.TbId=b.TbId ORDER BY TbId ASC";
            DataTable dt3 = ComService.GetDataTable(sql);

            foreach (Object obj in list)
            {
                bool badd = false;
                if (show == "y")
                {
                    if (ComService.GetTotal("tbindex", "where TbId='" + tbid + "' and IndexId='" + ((IndexBasicEntity)obj).IndexId + "'") == 0)
                    {
                        badd = true;
                    }
                }
                else
                {
                    badd = true;
                }

                if (badd)
                {
                    IndexBasicEntity newmodel = new IndexBasicEntity();
                    newmodel.IndexDataType = ((IndexBasicEntity)obj).IndexDataType;
                    newmodel.IndexId = ((IndexBasicEntity)obj).IndexId;
                    newmodel.IndexName = ((IndexBasicEntity)obj).IndexName;
                    newmodel.IndexExplain = ((IndexBasicEntity)obj).IndexExplain;

                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["TypeId"].ToString() == ((IndexBasicEntity)obj).IndexDataType)
                        {
                            newmodel.DataTypeName = dr["TypeName"].ToString();
                            break;
                        }
                    }

                    foreach (DataRow dr in dt2.Rows)
                    {
                        if (dr["SortID"].ToString() == ((IndexBasicEntity)obj).IndexSort)
                        {
                            newmodel.IndexSort = dr["SortName"].ToString();
                            break;
                        }
                    }

                    _tbname = "";
                    foreach (DataRow dr in dt3.Select("IndexId='" + ((IndexBasicEntity)obj).IndexId + "'"))
                    {
                        if (dr["IndexId"].ToString() == ((IndexBasicEntity)obj).IndexId)
                        {
                            _tbname += dr["TbName"].ToString() + "  ";
                            break;
                        }
                    }
                    newmodel.TableName = _tbname;

                    newlist.Add(newmodel);
                }
            }

            return newlist;
        }
    }
}