﻿using System.Collections.Generic;
using System.Data;
using Think9.Models;
using Think9.Services.Base;

namespace Think9.Services.Basic
{
    public class ServiceFlowPrcsNext : BaseService<FlowPrcsNextEntity>
    {
        private ComService ComService = new ComService();

        public IEnumerable<valueTextEntity> GetSelectNextList(string id, string fid)
        {
            DataTable dt = DataTableHelp.NewValueTextDt();

            string sql = "select PrcsId,PrcsName  from flowprcs where FlowId='" + fid + "' order by PrcsOrder";
            foreach (DataRow dr in ComService.GetDataTable(sql).Rows)
            {
                if (dr["PrcsId"].ToString() != id)
                {
                    DataRow row = dt.NewRow();
                    row["ClassID"] = "";
                    row["Value"] = dr["PrcsId"].ToString();
                    row["Text"] = dr["PrcsName"].ToString();
                    dt.Rows.Add(row);
                }
            }

            DataRow row1 = dt.NewRow();
            row1["ClassID"] = "";
            row1["Value"] = "";
            row1["Text"] = "结束";
            dt.Rows.Add(row1);

            return DataTableHelp.ToEnumerable<valueTextEntity>(dt);
        }
    }
}