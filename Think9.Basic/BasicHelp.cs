﻿using System;
using System.Collections.Generic;
using System.Data;
using Think9.Models;
using Think9.Services.Base;

namespace Think9.Services.Basic
{
    public class BasicHelp
    {
        public static string GetDefaultValue(CurrentUserEntity user, DataTable dsindex, string indexid)
        {
            string DefaultV = "";
            string ruid = "";
            string strValue = "";
            foreach (DataRow dr in dsindex.Rows)
            {
                if (dr["IndexId"].ToString() == indexid)
                {
                    ruid = dr["RuleId"].ToString();
                    DefaultV = dr["DefaultV"].ToString();
                    break;
                }
            }

            if (ruid.StartsWith("sys"))
            {
                if (ruid == "sysusername")
                {
                    strValue = user == null ? "" : user.RealName; ;
                }

                if (ruid == "sysuserid")
                {
                    strValue = user == null ? "" : user.Account;
                }

                if (ruid == "sysdeptno")
                {
                    strValue = user == null ? "" : user.DeptNo;
                }

                if (ruid == "sysdeptname")
                {
                    strValue = user == null ? "" : user.DeptName;
                }

                if (ruid == "sysroleno")
                {
                    strValue = user == null ? "" : user.RoleNo;
                }

                if (ruid == "sysrolename")
                {
                    strValue = user == null ? "" : user.RoleName;
                }

                if (ruid == "sysday")
                {
                    strValue = DateTime.Today.ToString("yyyy-MM-dd");
                }

                if (ruid == "systime")
                {
                    strValue = DateTime.Now.ToString();
                }
            }
            else
            {
                strValue = DefaultV;
            }

            return strValue;
        }

        public static string GetGridLockStr(DataTable dsindex, string indexid, int rownumber, string from)
        {
            string slock = "";
            string isLock = "";
            string strLock = "";
            foreach (DataRow dr in dsindex.Rows)
            {
                if (dr["IndexId"].ToString() == indexid)
                {
                    isLock = dr["isLock"].ToString();//是否锁定？1是2否3按行锁定
                    strLock = " " + dr["isLock2"].ToString() + " ";//锁定的行号 以空格间隔
                    break;
                }
            }

            //锁定
            if (isLock == "1")
            {
                slock = "disabled='disabled'";
            }
            //按行锁定
            if (isLock == "3" && strLock.Contains(" " + rownumber.ToString() + " "))
            {
                slock = "disabled='disabled'";
            }
            //编辑时锁定
            if (isLock == "9" && from != "add")
            {
                slock = "disabled='disabled'";
            }

            return slock;
        }

        public static string GetWhereByFlowId(CurrentUserEntity user, string flowid)
        {
            string where = "where 1=1 ";
            ComService ComService = new ComService();

            string userid = user == null ? ";un_defined;" : user.Account;
            string deptno = user == null ? ";un_defined;" : user.DeptNo;

            /// 查看编辑模式 可在录入表管理-权限管理中设置
            ///11查看编辑用户本人新建的数据
            ///22查看用户所属单位(部门)新建的数据 编辑本人新建的数据
            ///21查看编辑用户所属单位(部门)新建的数据
            ///32查看用户所属下级部门新建的数据  编辑本人新建的数据
            ///31查看编辑用户所属下级部门新建的数据
            ///42查看所有数据  编辑本人新建的数据
            ///41查看编辑所有数据
            string searchmode = "";
            DataTable dt = ComService.GetDataTable("select SearchMode from flow where flowid='" + flowid + "'");
            if (dt.Rows.Count > 0)
            {
                searchmode = dt.Rows[0]["SearchMode"].ToString();
            }
            searchmode = string.IsNullOrEmpty(searchmode) ? "11" : searchmode;

            //基础信息表
            if (flowid.StartsWith("bi_"))
            {
                where = "where 1=1";
                switch (searchmode.Substring(0, 1))
                {
                    case "1"://查看编辑用户本人新建的数据
                        where = "where (createUser = '" + userid + "') ";
                        break;

                    case "2"://查看用户所属单位(部门)新建的数据
                        where = "where (createDept = '" + deptno + "') ";
                        break;

                    case "3"://查看用户所属下级部门新建的数据
                        string str = ";" + deptno + ";";
                        where = "where (createDeptStr  like'%" + str + "%') ";
                        break;

                    case "4"://查看所有数据
                        where = "where (1=1) ";
                        break;

                    default:
                        where = "where (1=1) ";
                        break;
                }
            }
            else
            {
                where = "where 1=1";
                switch (searchmode.Substring(0, 1))
                {
                    case "1"://查看编辑用户本人新建的数据
                        where = "where (FlowRunList.createUser = '" + userid + "') ";
                        break;

                    case "2"://查看用户所属单位(部门)新建的数据
                        where = "where (FlowRunList.createDept = '" + deptno + "') ";
                        break;

                    case "3"://查看用户所属下级部门新建的数据
                        string str = ";" + deptno + ";";
                        where = "where (FlowRunList.createDeptStr  like'%" + str + "%') ";
                        break;

                    case "4"://查看所有数据
                        where = "where (1=1) ";
                        break;

                    default:
                        where = "where (1=1) ";
                        break;
                }
            }

            return where;
        }

        public static string GetSearchMode(string flowid)
        {
            string searchmode = "";
            ComService ComService = new ComService();

            DataTable dt = ComService.GetDataTable("select SearchMode from flow where flowid='" + flowid + "'");
            if (dt.Rows.Count > 0)
            {
                searchmode = dt.Rows[0]["SearchMode"].ToString();
            }
            searchmode = string.IsNullOrEmpty(searchmode) ? "11" : searchmode;

            string Some = "";
            switch (searchmode)
            {
                case "11":
                    Some = "【可查看本人新建的数据 || 可编辑删除本人新建的数据】";
                    break;

                case "22":
                    Some = "【可查看所属单位(部门)用户新建的数据 || 可编辑删除本人新建的数据】";
                    break;

                case "21":
                    Some = "【可查看所属单位(部门)用户新建的数据 || 可编辑删除所属单位(部门)用户新建的数据】";
                    break;

                case "32":
                    Some = "【可查看下级单位(部门)用户新建的数据 || 可编辑删除本人新建的数据】";
                    break;

                case "31":
                    Some = "【可查看下级单位(部门)用户新建的数据 || 可编辑删除下级单位(部门)用户新建的数据】";
                    break;

                case "42":
                    Some = "【可查看所有数据 || 可编辑删除本人新建的数据】";
                    break;

                case "41":
                    Some = "【可查看所有数据 || 可编辑删除所有数据】";
                    break;

                default:
                    Some = "";
                    break;
            }

            return Some;
        }

        /// <summary>
        ///
        /// </summary>
        public static object GetParamObject(CurrentUserEntity user)
        {
            if (user == null)
            {
                return new { sysusername = "un_defined", sysuserid = "un_defined", sysdeptno = "un_defined", sysdeptname = "un_defined", sysroleno = "un_defined", sysrolename = "un_defined", sysday = DateTime.Now.ToShortDateString(), systimenow = DateTime.Now };
            }
            else
            {
                return new { sysusername = user.RealName, sysuserid = user.Account, sysdeptno = user.DeptNo, sysdeptname = user.DeptName, sysroleno = user.RoleNo, sysrolename = user.RoleName, sysday = DateTime.Now.ToShortDateString(), systimenow = DateTime.Now };
            }
        }

        /// <summary>
        /// 取值
        /// </summary>
        /// <returns></returns>
        public static string GetTextFromList(IEnumerable<valueTextEntity> list, string classid, string value)
        {
            string str = "";
            foreach (valueTextEntity item in list)
            {
                if (item.ClassID == classid && item.Value == value)
                {
                    str = item.Text == null ? "" : item.Text;
                    break;
                }
            }

            return str;
        }

        /// <summary>
        /// 取值
        /// </summary>
        /// <returns></returns>
        public static string GetTextFromList(IEnumerable<ControlEntity> list, string controlid)
        {
            string str = "";
            foreach (ControlEntity item in list)
            {
                if (item.ControlID == controlid)
                {
                    str = item.ControlValue == null ? "" : item.ControlValue;
                    break;
                }
            }

            return str;
        }

        /// <summary>
        /// 取值
        /// </summary>
        /// <returns></returns>
        public static int? GetTextFromListToInt(IEnumerable<ControlEntity> list, string controlid)
        {
            string str = "";
            foreach (ControlEntity item in list)
            {
                if (item.ControlID == controlid)
                {
                    str = item.ControlValue == null ? "" : item.ControlValue;
                    break;
                }
            }

            return ExtConvert.ToIntOrNull(str);
        }

        /// <summary>
        /// 取值
        /// </summary>
        /// <returns></returns>
        public static decimal? GetTextFromListToDecimal(IEnumerable<ControlEntity> list, string controlid)
        {
            string str = "";
            foreach (ControlEntity item in list)
            {
                if (item.ControlID == controlid)
                {
                    str = item.ControlValue == null ? "" : item.ControlValue;
                    break;
                }
            }

            return ExtConvert.ToDecimalOrNull(str);
        }

        /// <summary>
        /// 取值
        /// </summary>
        /// <returns></returns>
        public static DateTime? GetTextFromListToDate(IEnumerable<ControlEntity> list, string controlid)
        {
            string str = "";
            foreach (ControlEntity item in list)
            {
                if (item.ControlID == controlid)
                {
                    str = item.ControlValue == null ? "" : item.ControlValue;
                    break;
                }
            }

            return ExtConvert.ToDateOrNull(str);
        }

        public static void GetRowAndIdByFlag(string flag, ref string id, ref string row)
        {
            string[] arr = BaseUtil.GetStrArray(flag, "#");

            id = arr[1].ToString();
            if (arr[2].ToString() == "0")
            {
                row = " 当前行";
            }
            else
            {
                row = " 第" + arr[2].ToString() + "行";
            }
        }

        public static void GetTbAndIdByFlag(string flag, ref string tbid, ref string id)
        {
            string[] arr = BaseUtil.GetStrArray(flag, "#");

            id = arr[1].ToString();
            tbid = arr[0].ToString();
        }
    }
}