﻿using System;
using System.Collections.Generic;
using System.Linq;
using Think9.Models;
using Think9.Services.Base;

namespace Think9.Services.Basic
{
    public class ItemsService : BaseService<ItemsEntity>
    {
        public dynamic GetListByFilter(ItemsEntity filter, PageInfoEntity pageInfo)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 递归遍历treeSelectList
        /// </summary>
        private void GetItemsChildren(List<TreeSelectEntity> treeSelectList, IEnumerable<ItemsEntity> moduleList, TreeSelectEntity tree, int id)
        {
            var childModuleList = moduleList.OrderBy(x => x.OrderNo);
            if (childModuleList != null && childModuleList.Count() > 0)
            {
                List<TreeSelectEntity> _children = new List<TreeSelectEntity>();
                foreach (var item in childModuleList)
                {
                    TreeSelectEntity _tree = new TreeSelectEntity
                    {
                        id = item.Id.ToString(),
                        name = item.FullName,
                        open = false
                    };
                    _children.Add(_tree);
                    tree.children = _children;
                    GetItemsChildren(treeSelectList, moduleList, _tree, item.Id);
                }
            }
        }
    }
}