﻿using System.IO;
using Think9.Models;
using Think9.Services.Base;

namespace Think9.Services.Basic
{
    public class AttachmentService : BaseService<AttachmentEntity>
    {
        private static string uploads = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\UserFile\\");

        public static void UpdateAttachmentId(int listid, string fwid, string attachmentId)
        {
            if (!string.IsNullOrEmpty(attachmentId))
            {
                ComService ComService = new ComService();
                if (fwid.StartsWith("bi_"))
                {
                    ComService.ExecuteSql("update " + fwid.Replace("bi_", "tb_") + " set attachmentId='" + attachmentId + "'   WHERE listid= " + listid);
                }
                else
                {
                    ComService.ExecuteSql("update flowrunlist set attachmentId='" + attachmentId + "'   WHERE listid= " + listid);
                }

                //修改ListId=0时上传附件的记录
                ComService.ExecuteSql("update recordrun set ListId=" + listid + "   WHERE listid = 0 and RecordFlag = '" + attachmentId + "' and FlowId = '" + fwid + "' ");
            }
        }

        public static void DelAttachment(int listid, string fwid)
        {
            ComService ComService = new ComService();
            string attId = "";
            if (fwid.StartsWith("bi_"))
            {
                attId = ComService.GetSingle("select attachmentId  FROM " + fwid.Replace("bi_", "tb_") + " WHERE listid= " + listid);
            }
            else
            {
                attId = ComService.GetSingle("select attachmentId  FROM flowrunlist WHERE listid= " + listid);
            }

            if (!string.IsNullOrEmpty(attId))
            {
                ComService.ExecuteSql("delete from flowattachment where attachmentId = '" + attId + "'");

                Think9.Util.Helper.FileHelper.DeleteDirectory(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\UserFile\\" + attId));
            }
        }

        public static void GetAttachmentId(int listid, string fwid)
        {
            string attid = "";
            ComService ComService = new ComService();
            if (fwid.StartsWith("bi_"))
            {
                if (listid != 0)
                {
                    attid = ComService.GetSingle("select attachmentId  FROM " + fwid.Replace("bi_", "tb_") + " WHERE listid= " + listid);
                }
            }
            else
            {
                if (listid != 0)
                {
                    attid = ComService.GetSingle("select attachmentId  FROM flowrunlist WHERE listid= " + listid);
                }
            }
        }

        /// <summary>
        ///流程步骤对附件的操作权限
        /// </summary>
        /// <param name="fwid"></param>
        /// <param name="prcid"></param>
        /// <param name="A1">新建 1有权限2无</param>
        /// <param name="A2">下载 1有权限2无</param>
        /// <param name="A3">删除 1有权限2无</param>
        public void GetFileAuthority(string fwid, string prcid, ref string A1, ref string A2, ref string A3)
        {
            if (fwid.StartsWith("bi_"))
            {
                A1 = "1";
                A2 = "1";
                A3 = "1";
            }
            else
            {
                ServiceFlow flow = new ServiceFlow();
                FlowEntity mflow = flow.GetByWhereFirst("where FlowId=@FlowId ", new { FlowId = fwid });
                if (mflow == null)
                {
                    A1 = "2";
                    A2 = "2";
                    A3 = "2";
                }
                else
                {
                    if (mflow.flowType == "2")
                    {
                        A1 = "1";
                        A2 = "1";
                        A3 = "1";
                    }
                    else
                    {
                        ServiceFlowPrcs FlowPrcsService = new ServiceFlowPrcs();
                        FlowPrcsEntity model = FlowPrcsService.GetByWhereFirst("where PrcsId=@id", new { id = prcid });
                        if (model == null)
                        {
                            A1 = "2";
                            A2 = "2";
                            A3 = "2";
                        }
                        else
                        {
                            if (model.BAttachment.Length >= 3)
                            {
                                A1 = model.BAttachment.Substring(0, 1);
                                A2 = model.BAttachment.Substring(1, 1);
                                A3 = model.BAttachment.Substring(2, 1);
                            }
                            else
                            {
                                A1 = "2";
                                A2 = "2";
                                A3 = "2";
                            }
                        }
                    }
                }
            }
        }
    }
}