﻿#if mysql
using DapperExtensions.MySQLExt;
#endif
#if sqlserver
using DapperExtensions.SqlServerExt;
#endif

using System.Data;
using System.Collections.Generic;

namespace Think9.Repository
{
    public class ComRepository
    {
        public DbContext dbContext = new DbContext();


        ///// <summary>
        ///// dapper通用分页方法
        ///// </summary>
        public DataTable GetDataTablePageList(string tableName, string files, string where, string orderby, int pageIndex, int pageSize, object param)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.GetDataTablePageList(files, tableName, where, orderby, pageIndex, pageSize, param);
            }
        }

        /// <summary>
        /// 求值 可以是正常的sql 也可以是算术表达式
        /// </summary>
        public string GetSingle(string sql)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.GetSingle(sql);
            }
        }

        /// <summary>
        /// 得到最大值 
        /// </summary>
        public int GetMaxID(string FieldName, string TableName, string strWhere)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.GetMaxID(FieldName, TableName, strWhere);
            }
        }

        /// <summary>
        /// 返回自动增长列id 
        /// </summary>
        public int InsertAndReturnID(string tableName, List<string> columns, object param)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.InsertAndReturnID(tableName, columns, param);
            }
        }

        /// <summary>
        /// 获得数据表包含字段列表 
        /// </summary>
        public DataTable GetTbFieldist(string tbid)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.GetTbFieldist(tbid);
            }
        }

        /// <summary>
        /// 根据录入表编码、指标编码、及类型生成数据库插入字段sql
        /// </summary>
        public string GetAddTbFieldSql(string tbid, string indexid, string datatype)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.GetAddTbFieldSql(tbid, indexid, datatype);
            }
        }

        /// <summary>
        /// 根据录入表编码、指标编码、及类型生成数据库插入字段sql
        /// </summary>
        public string GetAlterTbFieldSql(string tbid, string indexid, string datatype)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.GetAlterTbFieldSql(tbid, indexid, datatype);
            }
        }

        /// <summary>
        /// 判断是否存在某表的某个字段 
        /// </summary>
        public bool ColumnExists(string TableName, string columnName)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.ColumnExists(TableName, columnName);
            }
        }

        /// <summary>
        /// 获得数据表包含字段的字符串以#分割 
        /// </summary>
        public string GetTbColumnStr(string tbid)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.GetTbColumnStr(tbid);
            }
        }

        /// <summary>
        /// 表是否存在 
        /// </summary>
        public bool TabExists(string TableName)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.TabExists(TableName);
            }
        }

        /// <summary>
        /// 根据设置的指标，生成数据表语句 
        /// </summary>
        public string GetCreatDBSql(string tbid, string type, string flid)
        {
            using (var conn = dbContext.GetConnection())
            {
                if (type == "1")
                {
                    return conn.GetCreatMainDBSql(tbid, flid);
                }
                else
                {
                    return conn.GetCreatGridDBSql(tbid);
                }
            }
        }

        /// <summary>
        /// 执行sql语句
        /// </summary>
        public int ExecuteSql(string Sql, object param)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.ExecuteSql(Sql, param);
            }
        }

        /// <summary>
        /// 生成自动编号数据建表语句
        /// </summary>
        public string GetCreatRuleAutoTbSql(string rid)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.GetCreatRuleAutoTbSql(rid);
            }
        }

        /// <summary>
        /// 返回DataTable
        /// </summary>
        public DataTable GetDataTable(string sql, object param = null)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.GetDataTable(sql, param);
            }
        }

        /// <summary>
        /// 获取总数
        /// </summary>
        public long GetTotal(string tablename, string where = null, object param = null)
        {
            using (var conn = dbContext.GetConnection())
            {
                return conn.GetTotal(tablename, where, param);
            }
        }
    }
}