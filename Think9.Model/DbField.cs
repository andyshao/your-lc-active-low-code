﻿namespace Think9.Models
{
    public class DbFieldEntity
    {
        public string ColumnName { get; set; }
        public string DataType { get; set; }
        public string IndexName { get; set; }
    }
}