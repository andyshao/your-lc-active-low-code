﻿using DapperExtensions;

namespace Think9.Models
{
    [Table("RelationRDParameter")]
    public class RelationRDParameterEntity
    {
        public int RelationId { get; set; }
        public string ParameterId { get; set; }
        public string ParameterValue { get; set; }
        public string ParameterType { get; set; }
    }
}