﻿using DapperExtensions;
using System;

namespace Think9.Models
{
    [Table("relationwriterecord")]
    public class RelationWriteRecordEntity
    {
        /// <summary>
        ///
        /// </summary>
        public int ID
        {
            get; set;
        }

        /// <summary>
        /// RelationId
        /// </summary>
        public int RelationId
        {
            get; set;
        }

        /// <summary>
        /// 流程编码
        /// </summary>
        public string FlowId
        {
            get; set;
        }

        /// <summary>
        /// 流程步骤编码
        /// </summary>
        public string PrcsId
        {
            get; set;
        }

        /// <summary>
        /// 源表编码
        /// </summary>
        public string FromTbId
        {
            get; set;
        }

        /// <summary>
        /// 目标表编码
        /// </summary>
        public string WriteTbId
        {
            get; set;
        }

        /// <summary>
        /// 源表对应的listid+id
        /// </summary>
        public string FromId
        {
            get; set;
        }

        /// <summary>
        /// 目标表对应的listid+id
        /// </summary>
        public string WriteId
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public string strSql
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? WriteTime
        {
            get; set;
        }

        /// <summary>
        /// err标识生成失败（有错误） 正数或者零标识影响行数
        /// </summary>
        public string Flag
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public string StrErr
        {
            get; set;
        }

        /// <summary>
        /// //回写次数1表示只回写一次，其余均回写
        /// </summary>
        [Computed]
        public string NumberType
        {
            get; set;
        }

        /// <summary>
        /// 数据回写名称
        /// </summary>
        [Computed]
        public string RelationName
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        [Computed]
        public string ValueStr
        {
            get; set;
        }
    }
}