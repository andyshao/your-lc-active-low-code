﻿using DapperExtensions;
using System;
using System.ComponentModel.DataAnnotations;

namespace Think9.Models
{
    public class OptionsEntity
    {
        public string title { get; set; }
        public string value { get; set; }
        public string _checked { get; set; }
    }
}