﻿namespace Think9.Models
{
    public class UploadFileEntity
    {
        public int code { get; set; }
        public string msg { get; set; }
        public string src { get; set; }
        public string filename { get; set; }
    }
}