﻿using DapperExtensions;
using System;
using System.ComponentModel.DataAnnotations;

namespace Think9.Models
{
    [Table("tbbut")]
    public class TbButEntity
    {

        public string TbId { get; set; }
        public string ButId { get; set; }
        public string ButText { get; set; }
        public string ButWarn { get; set; }
    }
}