﻿namespace Think9.Models
{
    public class RuleAutoNoEntity
    {
        /// <summary>
        ///
        /// </summary>
        public int id
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public int listid
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public int itemid
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public string tbid
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public string indexid
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public string deptno
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public string userid
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public string timeno
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public int autono
        {
            get; set;
        }

        /// <summary>
        ///
        /// </summary>
        public string autoid
        {
            get; set;
        }
    }
}