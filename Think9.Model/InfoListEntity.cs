﻿namespace Think9.Models
{
    public class InfoListEntity
    {
        public string Id { get; set; }
        public string Value { get; set; }
        public string info1 { get; set; }
        public string info2 { get; set; }
        public string info3 { get; set; }
        public string info4 { get; set; }
        public string info5 { get; set; }
        public string info6 { get; set; }
        public string info7 { get; set; }
        public string info8 { get; set; }
        public string info9 { get; set; }
        public string info10 { get; set; }
    }
}