﻿/// <summary>
/// 基本信息表Models即ListId自增长的 都继承此类
/// </summary>
namespace Think9.Models
{
    public class MainTBEntity
    {
        /// <summary>
        /// 主键 自增长
        /// </summary>
        [DapperExtensions.Key(true)]
        public int ListId { get; set; }

        /// <summary>
        /// 0未锁定1已锁定
        /// </summary>
        public string isLock { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        public string createTime { get; set; }

        /// <summary>
        /// 创建用户
        /// </summary>
        public string createUser { get; set; }

        /// <summary>
        /// 创建用户所在部门编码
        /// </summary>
        public string createDept { get; set; }

        /// <summary>
        /// 所有的上级单位(部门)编码通过.相连的字符
        /// </summary>
        public string createDeptStr { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string runName { get; set; }

        /// <summary>
        /// 公共附件id
        /// </summary>
        public string attachmentId { get; set; }
    }
}