﻿namespace Think9.Models
{
    public class ReturnEntity
    {
        public int code { get; set; }
        public string msg { get; set; }
        public int count { get; set; }
        public dynamic data;
    }
}