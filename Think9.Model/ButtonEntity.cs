﻿using DapperExtensions;

namespace Think9.Models
{
    [Table("sys_Button")]
    public class ButtonEntity : Entity
    {
        /// <summary>
        /// 编码
        /// </summary>
        public string EnCode { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// 位置 0：表内 1：表外
        /// </summary>
        public int Location { get; set; }

        /// <summary>
        /// 按钮样式
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
    }
}