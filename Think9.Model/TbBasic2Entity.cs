﻿using DapperExtensions;

namespace Think9.Models
{
    [Table("TbBasic2")]
    public class TbBasic2Entity
    {
        public string TbId { get; set; }
        public int ColumnsNumber { get; set; }

        /// <summary>
        /// 1可动态添加删除行(包含增加和删除按钮,可单独增加删除行)
        /// </summary>
        public string InType { get; set; }
    }
}