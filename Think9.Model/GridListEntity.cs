﻿using DapperExtensions;

namespace Think9.Models
{
    public class GridListEntity
    {
        /// <summary>
        /// 主键
        /// </summary>
        [DapperExtensions.Key(true)]
        public long Id { get; set; }

        public long ListId { get; set; }

        [Computed]
        public string TbId { get; set; }

        [Computed]
        public string flag { get; set; }

        [Computed]
        public string v1 { get; set; }

        [Computed]
        public string v2 { get; set; }

        [Computed]
        public string v3 { get; set; }

        [Computed]
        public string v4 { get; set; }

        [Computed]
        public string v5 { get; set; }

        [Computed]
        public string v6 { get; set; }

        [Computed]
        public string v7 { get; set; }

        [Computed]
        public string v8 { get; set; }

        [Computed]
        public string v9 { get; set; }

        [Computed]
        public string v10 { get; set; }

        [Computed]
        public string v11 { get; set; }

        [Computed]
        public string v12 { get; set; }

        [Computed]
        public string v13 { get; set; }

        [Computed]
        public string v14 { get; set; }

        [Computed]
        public string v15 { get; set; }

        [Computed]
        public string v16 { get; set; }

        [Computed]
        public string v17 { get; set; }

        [Computed]
        public string v18 { get; set; }

        [Computed]
        public string v19 { get; set; }

        [Computed]
        public string v20 { get; set; }

        [Computed]
        public string v21 { get; set; }

        [Computed]
        public string v22 { get; set; }

        [Computed]
        public string v23 { get; set; }

        [Computed]
        public string v24 { get; set; }

        [Computed]
        public string v25 { get; set; }
    }
}