﻿namespace Think9.IdGenerator
{
    /// <summary>
    /// 生成数据库主键Id
    /// </summary>
    public class IdGeneratorHelper
    {
        //private int SnowFlakeWorkerId = 77;
        private int SnowFlakeWorkerId = 1;

        private Snowflake snowflake;

        private static readonly IdGeneratorHelper instance = new IdGeneratorHelper();

        private IdGeneratorHelper()
        {
            snowflake = new Snowflake(SnowFlakeWorkerId, 0, 0);
        }

        public static IdGeneratorHelper Instance
        {
            get
            {
                return instance;
            }
        }

        public long GetId()
        {
            return snowflake.NextId();
        }
    }
}