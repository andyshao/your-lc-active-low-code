﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using Think9.Models;
using Think9.Services.Base;
using Think9.Util.Helper;

namespace Think9.Controllers.Web
{
    public class HandlerErrorAttribute : Attribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            context.ExceptionHandled = true;
            context.HttpContext.Response.StatusCode = 200;
            context.Result = new ContentResult { Content = new AjaxResult { state = ResultType.error.ToString(), message = context.Exception.Message }.ToJson() };
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
        }
    }
}