﻿/**
 * Create:admin 2022-08-29 17:28:01
 * description:YoursLC有源低代码 自定义扩展 此文件放置于wwwroot/self_js文件夹中
 */

layui.define(['jquery', 'table'], function (exports) {
	let table = layui.table;
	let $ = layui.$;

	var api = {
		searchShow: function () {
			var display = $('#searchfield').css('display');
			if (display == 'none') {
				$("#searchfield").show();
			}
			else {
				$("#searchfield").hide();
			}
		},

		getPopUpSelect: function (obj) {
			var strv = "";
			var checkStatus = table.checkStatus(obj.config.id); //获取选中行状态
			if (checkStatus.data.length > 0) {
				strv = checkStatus.data[0].Value;
			}
			$(window.parent.document).find('#Pu_value').val(strv);
			$(window.parent.document).find('#Pu_tbid').val($('#Pu_tbid').val());
			$(window.parent.document).find('#Pu_indexid').val($('#Pu_indexid').val());
			$(window.parent.document).find('#Pu_id').val($('#Pu_id').val());
			$(window.parent.document).find('#Pu_v').val($('#Pu_v').val());
			$(window.parent.document).find('#Pu_value').click();
			parent.layer.close(parent.layer.getFrameIndex(window.name));
		},

		//固定流程 根据流程编码设置控件读写状态 
		setStateByFlow: function (prcno) {
			//无流程
		},

		//使用后台返回的lists，为控件赋值
		setValueByList: function (lists) {
			//主表
			if ($('#Pu_tbid').val() == '_main') {
				for (var item in lists) {
					var _value = lists[item].ControlValue;
					var _id = lists[item].ControlID;
					var _type = lists[item].ControlType;
					var _list = lists[item].ListValue;
					if (_list == null) {
						//text文本框
						if (_type == "1") {
							$("#" + _id).val(_value);
						}
						//select下拉选择
						if (_type == "2") {
							$("select[name=" + _id + "]").val(_value);
							/* $("#" + _id).val(_value);*/
						}
						//checkbox复选框
						if (_type == "3") {
						}
						//radio单选框
						if (_type == "4") {
							$("input[name=" + _id + "]" + "[value='" + _value + "']").prop('checked', 'checked');
						}
						//img图片
						if (_type == "5") {
						}
					}
					else {
						var _controlid = "#" + _id;
						$("" + _controlid + "").empty(); //清空控件
					    if (_type == "2") {
                            $("" + _controlid + "").append(new Option('==请选择==', ''))
                        }
						$.each(_list, function (i, item) {
							$("" + _controlid + "").append(new Option(item.Text, item.Value));
						});
					}
				}
			}
            else {//子表
                var _starts = "#" + $('#Pu_tbid').val() + "#" + $('#Pu_id').val() + "#";
                var trList = $(".layui-table").find("tr");
                for (var i = 0; i < trList.length; i++) {
                    var tdArr = trList.eq(i).find("td");
                    if (tdArr.eq(0).text().startsWith(_starts)) {
                        for (var item in lists) {
                            var _value = lists[item].ControlValue;
                            var _id = lists[item].ControlID.replace("v", "");
                            var _type = lists[item].ControlType;
                            if (_type == "2") {
                                tdArr.eq(_id).find('select').val(_value);
                            }
                            else {
                                tdArr.eq(_id).find('input').val(_value);
                            }
                        }
                    }
                }
            }
		},

		getValueFromPopUp: function (tbid, indexid, id) {
			var _tbid = $('#Pu_tbid').val();
			var _id = $('#Pu_id').val();
			var _indexid = $('#Pu_indexid').val();
			var _v = $('#Pu_v').val();
			var _value = $('#Pu_value').val();

			if (_tbid == '_main') {
				$("#" + _indexid + "").val(_value);
			}
			else {
				var _starts = "#" + _tbid + "#" + _id + "#";
				var trList = $(".layui-table").find("tr");
				for (var i = 0; i < trList.length; i++) {
					var tdArr = trList.eq(i).find("td");
					var flag = tdArr.eq(0).text();
					if (flag.startsWith(_starts)) {
						tdArr.eq(_v).find('input').val(_value);
					}
				}
			}
		},
		//增加行的Grid
		getTopGridTable: function (_list) {
			var trList = $(".layui-table").find("tr");  //获取table下的所有tr
			for (var i = 0; i < trList.length; i++) {  //遍历所有的tr
				var tdArr = trList.eq(i).find("td"); //获取该tr下的所有td
				var flag = tdArr.eq(0).text();

			}
		},
		//选择行的Grid
		getSelectGridTable: function (_list, tbid, id) {
			var trList = $(".layui-table").find("tr");  //获取table下的所有tr
			for (var i = 0; i < trList.length; i++) {  //遍历所有的tr
				var tdArr = trList.eq(i).find("td"); //获取该tr下的所有td
				var flag = tdArr.eq(0).text();

			}
		},
		//所有行的Grid
		foreachGridTable: function (_list) {
			var trList = $(".layui-table").find("tr");  //获取table下的所有tr
			for (var i = 0; i < trList.length; i++) {  //遍历所有的tr
				var tdArr = trList.eq(i).find("td"); //获取该tr下的所有td
				var flag = tdArr.eq(0).text();

			}
		}

	};

	//暴露接口
	exports('Data03', api);
});