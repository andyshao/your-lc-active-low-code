﻿/*******************************************************************************
 * Creator:admin 2022-08-29 12:33:14
 * Description: YoursLC有源低代码
*********************************************************************************/
using System;
using System.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Basic;
using Think9.Services.Table;
using Think9.Services.Flow;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace Think9.Services.CodeBuild
{
	/// <summary>
	/// 主表Service
	/// </summary>
	public class Demo03Service : BaseService<Demo03Model>
	{
		private ComService ComService = new ComService();
		private readonly string _split = BaseUtil.ComSplit;//字符分割 用于多选项的分割等
		private string sql;
		private string str;

		#region Before处理
		/// <summary>
		/// 添加前数据处理，默认处理自动编号及子表数据初始化
		/// </summary>
		/// <param name="err">错误信息</param>
		/// <param name="flowid">流程编码</param>
		/// <param name="maintbid">主表编码</param>
		/// <param name="tbname">主表名称</param>
		/// <param name="mPrcs">当前流程步骤，基础信息表为空</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
		public int BeforeAdd(ref string err, string flowid, string maintbid, string tbname, CurrentPrcsEntity mPrcs, CurrentUserEntity CurrentUser)
		{
			int listid = 0;

			//处理自动编号--录入表指标设置了自动编号，则增加一条空数据,返回自增长的listid;如果没有则什么也不做返回0
			listid = AutoNo.SetAutoNumber(ref err, flowid, tbname, mPrcs, CurrentUser);



			return listid;
		}

		/// <summary>
		/// 编辑前数据处理，默认使用通用函数标志接手办理，可自定义
		/// </summary>
		/// <param name="flowid">流程编码</param>
		/// <param name="listid">主表数据id</param>
		/// <param name="mPrcs">当前流程步骤，基础信息表为空</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
        public string BeforeEdit(string flowid, string listid, CurrentPrcsEntity mPrcs, CurrentUserEntity CurrentUser)
        {
            string err = "";
            if (flowid.StartsWith("fw_"))
            {
                err = FlowCom.TakeOverPrcs(CurrentUser, mPrcs);//标志接手办理
            }
			//可自定义

            return err;
        }

		/// <summary>
		/// 删除前数据处理，默认什么也不做，可自定义
		/// </summary>
		/// <param name="flowid">流程编码</param>
		/// <param name="listid">主表数据id</param>
		/// <param name="mPrcs">当前流程步骤，基础信息表为空</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
        public string BeforeDelete(string flowid, string listid, CurrentPrcsEntity mPrcs, CurrentUserEntity CurrentUser)
        {
            string err = "";
			//可自定义
            return err;
        }
		#endregion Before处理

		#region After处理
		/// <summary>
		/// 编辑后数据处理，默认调用通用函数实现数据回写，可自定义
		/// </summary>
		/// <param name="flowid">流程编码</param>
		/// <param name="listid">主表数据id</param>
		/// <param name="prcsid">当前流程步骤id</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
		public string AfterEdit(string listid, string flowid, string prcsid, CurrentUserEntity user)
        {
			string err = "";

			//调用通用函数实现数据回写，用户可自定义 数据回写在录入表管理/数据读写中设置
			WriteBack WriteBack = new WriteBack();
			string maintbid = flowid.Replace("bi_", "tb_").Replace("fw_", "tb_");
			err = WriteBack.WriteValueByRelationSet(listid, flowid, maintbid, prcsid, user);

			//可自定义

			return err;
		}

		/// <summary>
		/// 完成后数据处理，默认调用通用函数实现数据回写，可自定义
		/// </summary>
		/// <param name="flowid">流程编码</param>
		/// <param name="listid">主表数据id</param>
		/// <param name="prcsid">当前流程步骤id</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
		public string AfterFinish(string listid, string flowid, string prcsid, CurrentUserEntity user)
		{
			string err = AfterEdit(listid, flowid, prcsid, user);
			if(string.IsNullOrEmpty(err))
            {
				//数据锁定等
				FlowCom.FinishFlowPrcs(listid, flowid, prcsid, user);

				//可自定义
            }

			return err;
		}

		/// <summary>
		/// 删除后数据处理，默认什么也不做，可自定义
		/// </summary>
		/// <param name="flowid">流程编码</param>
		/// <param name="listid">主表数据id</param>
		/// <param name="mPrcs">当前流程步骤，基础信息表为空</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
		public string AfterDelete(string flowid, string listid, CurrentPrcsEntity mPrcs, CurrentUserEntity CurrentUser)
		{
			string err = "";
			//可自定义
			return err;
		}
		#endregion After处理

		#region 查询
		/// <summary>
        /// 数据查询列表
        /// </summary>
        /// <param name="total">总数据数量</param>
        /// <param name="model">主表数据model</param>
        /// <param name="pageInfo">页面信息，包括行数、排序等</param>
        /// <param name="user">当前用户信息</param>
        /// <param name="flowid">流程编码</param>
        /// <param name="tbid">主表编码</param>
        /// <param name="isAll">为all则显示所有</param>
        /// <returns></returns>
        public IEnumerable<dynamic> GetSearchList(ref long total, Demo03Model model, PageInfoEntity pageInfo, CurrentUserEntity user, string flowid, string tbid, string isAll)
        {
            object param = BasicHelp.GetParamObject(user);

            Demo03Model entity = new Demo03Model();
            if (string.IsNullOrEmpty(isAll))
            {
                entity = model;
            }

            //主表查询返回数据列表，备注：查询条件可在录入表指标属性中设置
            IEnumerable<dynamic> list = GetPageListBySearch(ref total, entity, pageInfo, user, flowid, tbid);
            //下拉选择、多选、单选准备动态数据源
            IEnumerable<valueTextEntity> SelectList = GetSelectList("list", param);

            //处理列表数据select、checkbox、radio等Value与Text转化
            foreach (Demo03Model obj in list)
            {
				//类别
				foreach (valueTextEntity sel in SelectList.Where(x => x.ClassID == "inCategory"))
				{
					if (obj.inCategory == sel.Value)
					{
						obj.inCategory = sel.Text;
						break;
					}
				}
				//计量单位
				foreach (valueTextEntity sel in SelectList.Where(x => x.ClassID == "inUnitOfMeasurement"))
				{
					if (obj.inUnitOfMeasurement == sel.Value)
					{
						obj.inUnitOfMeasurement = sel.Text;
						break;
					}
				}
            }

            return list;
        }

		/// <summary>
		/// 主表查询调用，返回列表，查询条件可在录入表指标属性中设置
		/// </summary>
		/// <param name="total">每页行数</param>
		/// <param name="model">主表数据model</param>
		/// <param name="pageInfo">页面信息，包括行数、排序等</param>
		/// <param name="user">当前用户信息</param>
		/// <param name="flowid">流程编码</param>
		/// <param name="tbid">主表编码</param>
		/// <returns></returns>
		private IEnumerable<dynamic> GetPageListBySearch(ref long total, Demo03Model model, PageInfoEntity pageInfo, CurrentUserEntity user, string flowid, string tbid)
		{
			pageInfo.returnFields = "FlowRunList.isLock,FlowRunList.createTime, FlowRunList.createUser," + tbid + ".* ";
			string some = "  FlowRunList INNER JOIN " + tbid + " ON FlowRunList.listid = " + tbid + ".listid ";
			string where = BasicHelp.GetWhereByFlowId(user, flowid);

			//调薪时间
			if(model.inSalaryAdjustmentTime != null)
			{
				where += " and (tb_Demo03.inSalaryAdjustmentTime >= @inSalaryAdjustmentTime)"; 
			}
			if(model.inSalaryAdjustmentTime_Exa  != null)
			{
				where += " and (tb_Demo03.inSalaryAdjustmentTime <= @inSalaryAdjustmentTime_Exa)"; 
			}
			//编码
			if (!string.IsNullOrEmpty(model.inCode))
			{
				where += " and (tb_Demo03.inCode like @inCode)"; 
				model.inCode = string.Format("%{0}%", model.inCode); 
			}
			//名称
			if (!string.IsNullOrEmpty(model.inName))
			{
				where += " and (tb_Demo03.inName like @inName)"; 
				model.inName = string.Format("%{0}%", model.inName); 
			}
			//类别
			if (!string.IsNullOrEmpty(model.inCategory))
			{
				where += " and (tb_Demo03.inCategory = @inCategory)"; 
			}
			//计量单位
			if (!string.IsNullOrEmpty(model.inUnitOfMeasurement))
			{
				where += " and (tb_Demo03.inUnitOfMeasurement = @inUnitOfMeasurement)"; 
			}
			//备注2
			if (!string.IsNullOrEmpty(model.inNote2))
			{
				where += " and (tb_Demo03.inNote2 like @inNote2)"; 
				model.inNote2 = string.Format("%{0}%", model.inNote2); 
			}
			//员工编码
			if (!string.IsNullOrEmpty(model.inEmployeeCode))
			{
				where += " and (tb_Demo03.inEmployeeCode = @inEmployeeCode)"; 
			}
			//员工姓名
			if (!string.IsNullOrEmpty(model.inEmployeeName))
			{
				where += " and (tb_Demo03.inEmployeeName like @inEmployeeName)"; 
				model.inEmployeeName = string.Format("%{0}%", model.inEmployeeName); 
			}
			//员工部门
			if (!string.IsNullOrEmpty(model.inEmployeeDepartment))
			{
				where += " and (tb_Demo03.inEmployeeDepartment like @inEmployeeDepartment)"; 
				model.inEmployeeDepartment = string.Format("%{0}%", model.inEmployeeDepartment); 
			}
			//员工岗位
			if (!string.IsNullOrEmpty(model.inEmployeePosition))
			{
				where += " and (tb_Demo03.inEmployeePosition like @inEmployeePosition)"; 
				model.inEmployeePosition = string.Format("%{0}%", model.inEmployeePosition); 
			}
			//图片
			if (!string.IsNullOrEmpty(model.inPicture))
			{
				where += " and (tb_Demo03.inPicture like @inPicture)"; 
				model.inPicture = string.Format("%{0}%", model.inPicture); 
			}
			//整数
			if(model.Integer01 != null)
			{
				where += " and (tb_Demo03.Integer01 >= @Integer01)"; 
			}
			if(model.Integer01_Exa  != null)
			{
				where += " and (tb_Demo03.Integer01 <= @Integer01_Exa)"; 
			}
			//数字
			if(model.inNumber != null)
			{
				where += " and (tb_Demo03.inNumber >= @inNumber)"; 
			}
			if(model.inNumber_Exa  != null)
			{
				where += " and (tb_Demo03.inNumber <= @inNumber_Exa)"; 
			}
			//数量
			if(model.inSL != null)
			{
				where += " and (tb_Demo03.inSL >= @inSL)"; 
			}
			if(model.inSL_Exa  != null)
			{
				where += " and (tb_Demo03.inSL <= @inSL_Exa)"; 
			}
			//备注9
			if (!string.IsNullOrEmpty(model.inNote9))
			{
				where += " and (tb_Demo03.inNote9 like @inNote9)"; 
				model.inNote9 = string.Format("%{0}%", model.inNote9); 
			}
			//测试0001
			if (!string.IsNullOrEmpty(model.inCS0001))
			{
				where += " and (tb_Demo03.inCS0001 like @inCS0001)"; 
				model.inCS0001 = string.Format("%{0}%", model.inCS0001); 
			}
			return base.GetPageUnite(ref total, model, pageInfo, some + where);

		}

		/// <summary>
		/// 主表-为下拉选择、多选、单选准备动态数据源
		/// 处理列表显示时value与text转化，备注：动态数据源来源于为指标指定的数据规范
		/// </summary>
		/// <param name="from">edit或list</param>
		/// <param name="param">条件参数</param>
		/// <returns></returns>
		public IEnumerable<valueTextEntity> GetSelectList(string from, object param)
		{
			DataTable dt = DataTableHelp.NewValueTextDt();

			//类别
			sql = "select DetailName as id,DetailName as name FROM sys_itemsdetail where ItemCode = 'RawMaterialsOrProducts' ORDER BY OrderNo";
			foreach (DataRow dr in ComService.GetDataTable(sql, param).Rows)
			{
				DataRow row = dt.NewRow();
				row["ClassID"] = "inCategory";
				row["Value"] = dr["id"].ToString();
				row["Text"] = dr["name"].ToString();
				dt.Rows.Add(row);
			}
			//计量单位
			sql = "select DetailName as id,DetailName as name FROM sys_itemsdetail where ItemCode = 'UnitOfMeasure' ORDER BY OrderNo";
			foreach (DataRow dr in ComService.GetDataTable(sql, param).Rows)
			{
				DataRow row = dt.NewRow();
				row["ClassID"] = "inUnitOfMeasurement";
				row["Value"] = dr["id"].ToString();
				row["Text"] = dr["name"].ToString();
				dt.Rows.Add(row);
			}

			//DataTable转换成IEnumerable
			return DataTableHelp.ToEnumerable<valueTextEntity>(dt);
		}

		/// <summary>
		/// 弹出页面的查询条件有下拉选择、多选、单选时，为其绑定动态数据源
		/// 动态数据源来源于为指标指定的数据规范
		/// </summary>
		/// <param name="tbid">_main或者空</param>
		/// <param name="indexid">指标编码</param>
		/// <param name="from">edit或list</param>
		/// <param name="param">条件参数</param>
		/// <returns></returns>
		public IEnumerable<valueTextEntity> GetSelectList(string tbid, string indexid, string from, object param)
		{
			DataTable dt = DataTableHelp.NewValueTextDt();

			//主表指标弹出页面
			if (tbid == "_main")
			{

			}
			else//子表指标弹出页面
			{

			}

			//DataTable转换成IEnumerable
			return DataTableHelp.ToEnumerable<valueTextEntity>(dt);
		}

		/// <summary>
		/// 弹出页面获得数据列表
		/// </summary>
		/// <param name="total">数据count</param>
		/// <param name="indexid">主表触发时为指标编码，子表触发时为子表编码+指标编码</param>
		/// <param name="from">list或edit list时会解除条件参数中包含的系统指标，如用户登录名等</param>
		/// <param name="list">封装查询条件</param>
		public IEnumerable<dynamic> GetPopUpTableList(ref long total, PageInfoEntity pageInfo, string indexid, string from, IEnumerable<valueTextEntity> list)
        {
			string tbname;
			string some;
			string where;
			string order;
			PopUpTableListService tblist = new PopUpTableListService();

			IEnumerable<dynamic> _list = null;
				
			if (indexid == "inEmployeeCode")
			{
				tbname = "tb_Data03";
				some = "inEmployeeCode as Value,inEmployeeCode as info1,inEmployeeName as info2";
				where = " where 1=1";
				order =" ORDER BY inEmployeeCode";
				
				object param = new {  };
				
				 _list = tblist.GetPageList(ref total, pageInfo, some, tbname, where, order,  param);
			}


			return _list;
		}
		#endregion 查询

		/// <summary>
		/// 数据删除
		/// </summary>
		/// <param name="id">主表数据id</param>
		/// <param name="fwid">流程编码</param>
		/// <returns></returns>
		public bool DeleteByID(string id, string fwid)
		{
			AttachmentService.DelAttachment(int.Parse(id), fwid);//删除附件

			//删除flowrunlist、flowrunprcslist中关联数据
			if (fwid.StartsWith("fw_"))
			{
				ComService.ExecuteSql("delete from flowrunlist where ListId = " + id + "");
				ComService.ExecuteSql("delete from flowrunprcslist where ListId = " + id + "");
			}

			AutoNo.DelAutoNo(id, fwid);//删除自动编号

			return base.DeleteByWhere("where ListId=" + id + "");//删除主表数据
		}

		/// <summary>
		///返回可修改字段 自由流程排除了隐藏指标 固定流程排除了隐藏指标及不可写字段
		/// </summary>
		/// <param name="prcno">流程步骤编码</param>
		/// <returns></returns>
		public string GetUpdateFields(string prcno)
		{
			//固定流程排除了隐藏指标、隐藏及不可写字段 
			string UpdateFields = "";
			//新建数据 步骤编码new
			if (prcno == "new") 
			{
				UpdateFields = "inSalaryAdjustmentTime,inCode,inName,inCategory,inUnitOfMeasurement,inNote2,inEmployeeCode,inEmployeeName,inEmployeeDepartment,inEmployeePosition,inPicture,Integer01,inNumber,inSL,inNote9,inCS0001";
			}
			return UpdateFields;
		}
		
		//主表数据读取
		public List<ControlEntity> ReadValue(CurrentUserEntity user, IEnumerable<ControlEntity> controlslist, string tbid, string indexid, string value)
		{
			List<ControlEntity> list = new List<ControlEntity>();
			
			string tbname = "";
			string some = "";
			string where = "";
			string order = "";
			
			if (indexid == "inEmployeeCode")
			{
				//读取员工信息
				tbname = "tb_data03";
				some = "inEmployeeCode AS inEmployeeCode,inEmployeeDepartment AS inEmployeeDepartment,inEmployeeName AS inEmployeeName,inEmployeePosition AS inEmployeePosition";
				where = "where tb_data03.inemployeecode = @inEmployeeCode";
				order = " order by listid";
				object param = new { inEmployeeCode = value };
				
				DataTable dt = ComService.GetDataTable(tbname, some, where, order, param);
				if (dt.Rows.Count > 0)
				{
					//员工编码
					list.Add(new ControlEntity { ControlID = "inEmployeeCode", ControlValue = dt.Rows[0]["inEmployeeCode"].ToString(), ControlType = "1" });
					
					//员工部门
					list.Add(new ControlEntity { ControlID = "inEmployeeDepartment", ControlValue = dt.Rows[0]["inEmployeeDepartment"].ToString(), ControlType = "1" });
					
					//员工姓名
					list.Add(new ControlEntity { ControlID = "inEmployeeName", ControlValue = dt.Rows[0]["inEmployeeName"].ToString(), ControlType = "1" });
					
					//员工岗位
					list.Add(new ControlEntity { ControlID = "inEmployeePosition", ControlValue = dt.Rows[0]["inEmployeePosition"].ToString(), ControlType = "1" });
					
				}
			}
			
			return list;
		}


		#region extra 发布模式下将会被调用
		public Demo03Model NewModel( )
		{
			Demo03Model model = new Demo03Model();

			return model;
		}

		public Demo03Model ToModel(string json)
		{
			Demo03Model model = new Demo03Model();
			if (!string.IsNullOrEmpty(json))
            {
               model = (Demo03Model)JsonConvert.DeserializeObject<Demo03Model>(json);
            }

			return model;
		}

		//主表指标赋初始值
		public Demo03Model GetModel(string type, int listid, CurrentUserEntity CurrentUser)
        {
            Demo03Model model = new Demo03Model();

            if (type == "add")
            {
                model.ListId = listid;
                if (listid != 0)
                {
                    model = GetByWhereFirst("where listid=" + listid + "");
                }
                //赋初始值--系统指标或默认值
				model.inSalaryAdjustmentTime = DateTime.Today;//系统指标当前日期
				model.inCode = CurrentUser == null ? "un_defined" : CurrentUser.Account;//系统指标当前用户登陆账号
				model.inName = CurrentUser == null ? "un_defined" : CurrentUser.RealName;//系统指标当前用户真实姓名
            }
            else
            {
                model = GetByWhereFirst("where listid=" + listid + "");
            }

            return model;
        }

		//同时处理复选框和图片指标
		public Demo03Model GetModel(string json)
        {
            Demo03Model model = new Demo03Model();

			if (!string.IsNullOrEmpty(json))
            {
               model = (Demo03Model)JsonConvert.DeserializeObject<Demo03Model>(json);
			   //处理主表复选框和图片
 				model.inPicture = model.inPicture_Exa;//处理图片
            }

            return model;
        }

		public DataTable ModelToDataTable(Demo03Model model)
		{
			return DataTableHelp.ModelToDataTable <Demo03Model> (model);
		}

		public bool InsertModel(Demo03Model model, int listid)
		{
			model.ListId = listid;
			return base.Insert(model);
		}
		#endregion extra 发布版中使用
	}
}