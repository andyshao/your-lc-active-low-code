﻿/*******************************************************************************
 * Create:admin 2022-08-29 17:28:02
 * Description: YoursLC有源低代码 实体类
*********************************************************************************/

using System;
using DapperExtensions;
namespace Think9.Models 
{
	/// <summary>
	/// 实体类  生成时间：2022-08-29 17:28:02
	/// </summary>
	 [Table("tb_Data04")]
	public class Data04Model : MainTBEntity
	{
		#region Model
		/// <summary>
		/// 类别
		/// </summary>
		public string inCategory { get; set; }
		/// <summary>
		/// 编码
		/// </summary>
		public string inCode { get; set; }
		/// <summary>
		/// 名称
		/// </summary>
		public string inName { get; set; }
		/// <summary>
		/// 计量单位
		/// </summary>
		public string inUnitOfMeasurement { get; set; }
		/// <summary>
		/// 数量
		/// </summary>
		public decimal? inSL { get; set; }
		/// <summary>
		/// 数量 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public decimal? inSL_Exa { get; set; }
		#endregion Model
	}
}
