﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Basic;
using Think9.Services.CodeBuild;
using Think9.Services.Flow;
using Think9.Services.Table;
using DapperExtensions;
using System.Linq;
using Newtonsoft.Json.Linq;

/*******************************************************************************
 * Create:admin 2022-08-29 17:28:01
 * Description: YoursLC有源低代码 实体类
*********************************************************************************/

//
//
namespace Think9.Models 
{
	/// <summary>
	/// 实体类  生成时间：2022-08-29 17:28:01
	/// </summary>
	 [Table("tb_Data02")]
	public class Data02Model : MainTBEntity
	{
		#region Model
		/// <summary>
		/// 编码
		/// </summary>
		public string inCode { get; set; }
		/// <summary>
		/// 名称
		/// </summary>
		public string inName { get; set; }
		/// <summary>
		/// 备注
		/// </summary>
		public string inRemarks { get; set; }
		#endregion Model
	}
}
/*******************************************************************************
 * Creator:admin 2022-08-29 17:28:01
 * Description: YoursLC有源低代码
*********************************************************************************/
//
//
//
//
//
//
//
//
//
//
//

namespace Think9.Services.CodeBuild
{
	/// <summary>
	/// 主表Service
	/// </summary>
	public class Data02Service : BaseService<Data02Model>
	{
		private ComService ComService = new ComService();
		private readonly string _split = BaseUtil.ComSplit;//字符分割 用于多选项的分割等
		private string sql;
		private string str;

		#region Before处理
		/// <summary>
		/// 添加前数据处理，默认处理自动编号及子表数据初始化
		/// </summary>
		/// <param name="err">错误信息</param>
		/// <param name="flowid">流程编码</param>
		/// <param name="maintbid">主表编码</param>
		/// <param name="tbname">主表名称</param>
		/// <param name="mPrcs">当前流程步骤，基础信息表为空</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
		public int BeforeAdd(ref string err, string flowid, string maintbid, string tbname, CurrentPrcsEntity mPrcs, CurrentUserEntity CurrentUser)
		{
			int listid = 0;

			//处理自动编号--录入表指标设置了自动编号，则增加一条空数据,返回自增长的listid;如果没有则什么也不做返回0
			listid = AutoNo.SetAutoNumber(ref err, flowid, tbname, mPrcs, CurrentUser);



			return listid;
		}

		/// <summary>
		/// 编辑前数据处理，默认使用通用函数标志接手办理，可自定义
		/// </summary>
		/// <param name="flowid">流程编码</param>
		/// <param name="listid">主表数据id</param>
		/// <param name="mPrcs">当前流程步骤，基础信息表为空</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
        public string BeforeEdit(string flowid, string listid, CurrentPrcsEntity mPrcs, CurrentUserEntity CurrentUser)
        {
            string err = "";
            if (flowid.StartsWith("fw_"))
            {
                err = FlowCom.TakeOverPrcs(CurrentUser, mPrcs);//标志接手办理
            }
			//可自定义

            return err;
        }

		/// <summary>
		/// 删除前数据处理，默认什么也不做，可自定义
		/// </summary>
		/// <param name="flowid">流程编码</param>
		/// <param name="listid">主表数据id</param>
		/// <param name="mPrcs">当前流程步骤，基础信息表为空</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
        public string BeforeDelete(string flowid, string listid, CurrentPrcsEntity mPrcs, CurrentUserEntity CurrentUser)
        {
            string err = "";
			//可自定义
            return err;
        }
		#endregion Before处理

		#region After处理
		/// <summary>
		/// 编辑后数据处理，默认调用通用函数实现数据回写，可自定义
		/// </summary>
		/// <param name="flowid">流程编码</param>
		/// <param name="listid">主表数据id</param>
		/// <param name="prcsid">当前流程步骤id</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
		public string AfterEdit(string listid, string flowid, string prcsid, CurrentUserEntity user)
        {
			string err = "";

			//调用通用函数实现数据回写，用户可自定义 数据回写在录入表管理/数据读写中设置
			WriteBack WriteBack = new WriteBack();
			string maintbid = flowid.Replace("bi_", "tb_").Replace("fw_", "tb_");
			err = WriteBack.WriteValueByRelationSet(listid, flowid, maintbid, prcsid, user);

			//可自定义

			return err;
		}

		/// <summary>
		/// 完成后数据处理，默认调用通用函数实现数据回写，可自定义
		/// </summary>
		/// <param name="flowid">流程编码</param>
		/// <param name="listid">主表数据id</param>
		/// <param name="prcsid">当前流程步骤id</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
		public string AfterFinish(string listid, string flowid, string prcsid, CurrentUserEntity user)
		{
			string err = AfterEdit(listid, flowid, prcsid, user);
			if(string.IsNullOrEmpty(err))
            {
				//数据锁定等
				FlowCom.FinishFlowPrcs(listid, flowid, prcsid, user);

				//可自定义
            }

			return err;
		}

		/// <summary>
		/// 删除后数据处理，默认什么也不做，可自定义
		/// </summary>
		/// <param name="flowid">流程编码</param>
		/// <param name="listid">主表数据id</param>
		/// <param name="mPrcs">当前流程步骤，基础信息表为空</param>
		/// <param name="CurrentUser">当前用户</param>
		/// <returns></returns>
		public string AfterDelete(string flowid, string listid, CurrentPrcsEntity mPrcs, CurrentUserEntity CurrentUser)
		{
			string err = "";
			//可自定义
			return err;
		}
		#endregion After处理

		#region 查询
		/// <summary>
        /// 数据查询列表
        /// </summary>
        /// <param name="total">总数据数量</param>
        /// <param name="model">主表数据model</param>
        /// <param name="pageInfo">页面信息，包括行数、排序等</param>
        /// <param name="user">当前用户信息</param>
        /// <param name="flowid">流程编码</param>
        /// <param name="tbid">主表编码</param>
        /// <param name="isAll">为all则显示所有</param>
        /// <returns></returns>
        public IEnumerable<dynamic> GetSearchList(ref long total, Data02Model model, PageInfoEntity pageInfo, CurrentUserEntity user, string flowid, string tbid, string isAll)
        {
            object param = BasicHelp.GetParamObject(user);

            Data02Model entity = new Data02Model();
            if (string.IsNullOrEmpty(isAll))
            {
                entity = model;
            }

            //主表查询返回数据列表，备注：查询条件可在录入表指标属性中设置
            IEnumerable<dynamic> list = GetPageListBySearch(ref total, entity, pageInfo, user, flowid, tbid);
            //下拉选择、多选、单选准备动态数据源
            IEnumerable<valueTextEntity> SelectList = GetSelectList("list", param);

            //处理列表数据select、checkbox、radio等Value与Text转化
            foreach (Data02Model obj in list)
            {

            }

            return list;
        }

		/// <summary>
		/// 主表查询调用，返回列表，查询条件可在录入表指标属性中设置
		/// </summary>
		/// <param name="total">每页行数</param>
		/// <param name="model">主表数据model</param>
		/// <param name="pageInfo">页面信息，包括行数、排序等</param>
		/// <param name="user">当前用户信息</param>
		/// <param name="flowid">流程编码</param>
		/// <param name="tbid">主表编码</param>
		/// <returns></returns>
		private IEnumerable<dynamic> GetPageListBySearch(ref long total, Data02Model model, PageInfoEntity pageInfo, CurrentUserEntity user, string flowid, string tbid)
		{
			string where = BasicHelp.GetWhereByFlowId(user, flowid);

			//编码
			if (!string.IsNullOrEmpty(model.inCode))
			{
				where += " and (tb_Data02.inCode like @inCode)"; 
				model.inCode = string.Format("%{0}%", model.inCode); 
			}
			//名称
			if (!string.IsNullOrEmpty(model.inName))
			{
				where += " and (tb_Data02.inName like @inName)"; 
				model.inName = string.Format("%{0}%", model.inName); 
			}
			//备注
			if (!string.IsNullOrEmpty(model.inRemarks))
			{
				where += " and (tb_Data02.inRemarks like @inRemarks)"; 
				model.inRemarks = string.Format("%{0}%", model.inRemarks); 
			}
			return base.GetPageByFilter(ref total, model, pageInfo, where);

		}

		/// <summary>
		/// 主表-为下拉选择、多选、单选准备动态数据源
		/// 处理列表显示时value与text转化，备注：动态数据源来源于为指标指定的数据规范
		/// </summary>
		/// <param name="from">edit或list</param>
		/// <param name="param">条件参数</param>
		/// <returns></returns>
		public IEnumerable<valueTextEntity> GetSelectList(string from, object param)
		{
			DataTable dt = DataTableHelp.NewValueTextDt();


			//DataTable转换成IEnumerable
			return DataTableHelp.ToEnumerable<valueTextEntity>(dt);
		}

		/// <summary>
		/// 弹出页面的查询条件有下拉选择、多选、单选时，为其绑定动态数据源
		/// 动态数据源来源于为指标指定的数据规范
		/// </summary>
		/// <param name="tbid">_main或者空</param>
		/// <param name="indexid">指标编码</param>
		/// <param name="from">edit或list</param>
		/// <param name="param">条件参数</param>
		/// <returns></returns>
		public IEnumerable<valueTextEntity> GetSelectList(string tbid, string indexid, string from, object param)
		{
			DataTable dt = DataTableHelp.NewValueTextDt();

			//主表指标弹出页面
			if (tbid == "_main")
			{

			}
			else//子表指标弹出页面
			{

			}

			//DataTable转换成IEnumerable
			return DataTableHelp.ToEnumerable<valueTextEntity>(dt);
		}

		/// <summary>
		/// 弹出页面获得数据列表
		/// </summary>
		/// <param name="total">数据count</param>
		/// <param name="indexid">主表触发时为指标编码，子表触发时为子表编码+指标编码</param>
		/// <param name="from">list或edit list时会解除条件参数中包含的系统指标，如用户登录名等</param>
		/// <param name="list">封装查询条件</param>
		public IEnumerable<dynamic> GetPopUpTableList(ref long total, PageInfoEntity pageInfo, string indexid, string from, IEnumerable<valueTextEntity> list)
        {
			string tbname;
			string some;
			string where;
			string order;
			PopUpTableListService tblist = new PopUpTableListService();

			IEnumerable<dynamic> _list = null;


			return _list;
		}
		#endregion 查询

		/// <summary>
		/// 数据删除
		/// </summary>
		/// <param name="id">主表数据id</param>
		/// <param name="fwid">流程编码</param>
		/// <returns></returns>
		public bool DeleteByID(string id, string fwid)
		{
			AttachmentService.DelAttachment(int.Parse(id), fwid);//删除附件

			//删除flowrunlist、flowrunprcslist中关联数据
			if (fwid.StartsWith("fw_"))
			{
				ComService.ExecuteSql("delete from flowrunlist where ListId = " + id + "");
				ComService.ExecuteSql("delete from flowrunprcslist where ListId = " + id + "");
			}

			AutoNo.DelAutoNo(id, fwid);//删除自动编号

			return base.DeleteByWhere("where ListId=" + id + "");//删除主表数据
		}

		/// <summary>
		///返回可修改字段 自由流程排除了隐藏指标 固定流程排除了隐藏指标及不可写字段
		/// </summary>
		/// <param name="prcno">流程步骤编码</param>
		/// <returns></returns>
		public string GetUpdateFields(string prcno)
		{
			//自由流程或无流程排除了隐藏指标
			 return "inCode,inName,inRemarks";
		}



		#region extra 发布模式下将会被调用
		public Data02Model NewModel( )
		{
			Data02Model model = new Data02Model();

			return model;
		}

		public Data02Model ToModel(string json)
		{
			Data02Model model = new Data02Model();
			if (!string.IsNullOrEmpty(json))
            {
               model = (Data02Model)JsonConvert.DeserializeObject<Data02Model>(json);
            }

			return model;
		}

		//主表指标赋初始值
		public Data02Model GetModel(string type, int listid, CurrentUserEntity CurrentUser)
        {
            Data02Model model = new Data02Model();

            if (type == "add")
            {
                model.ListId = listid;
                if (listid != 0)
                {
                    model = GetByWhereFirst("where listid=" + listid + "");
                }
                //赋初始值--系统指标或默认值

            }
            else
            {
                model = GetByWhereFirst("where listid=" + listid + "");
            }

            return model;
        }

		//同时处理复选框和图片指标
		public Data02Model GetModel(string json)
        {
            Data02Model model = new Data02Model();

			if (!string.IsNullOrEmpty(json))
            {
               model = (Data02Model)JsonConvert.DeserializeObject<Data02Model>(json);
			   //处理主表复选框和图片
 
            }

            return model;
        }

		public DataTable ModelToDataTable(Data02Model model)
		{
			return DataTableHelp.ModelToDataTable <Data02Model> (model);
		}

		public int InsertModel(Data02Model model, DataTable dtMain, CurrentUserEntity user, string flowid, string tbname)
		{
			model.createTime = DateTime.Now.ToString();
			model.isLock = "0";
			model.createUser = user == null ? "un_defined" : user.Account;
			model.createDept = user == null ? "un_defined" : user.DeptNo;
			model.createDeptStr = user == null ? "un_defined" : user.DeptNoStr;
			model.runName = BaseUtil.GetRunName(model.createUser, flowid, tbname, dtMain);
			
			return base.InsertReturnID(model);
		}
		#endregion extra 发布版中使用
	}
}/*******************************************************************************
 * Creator:admin 2022-08-29 17:28:01
 * Description: YoursLC有源低代码
*********************************************************************************/

//
//
//
//
//
//
//
//
//
//
//
//
//

namespace Think9.Controllers.CodeBuild
{
    public class Data02Controller : BaseController
    {
        private Think9.Services.CodeBuild.Data02Service Data02Service = new Think9.Services.CodeBuild.Data02Service();
        private ComService ComService = new ComService();

        private readonly string _maintbid = "tb_Data02";//主表编码
        private readonly string _flowid = "bi_Data02";//流程编码 bi_基础信息 fw_一般录入表
        private readonly string _tbname = "Data02行政区划";//录入表名称

        private readonly string _split = BaseUtil.ComSplit;//字符分割 用于多选项的分割等
        private string str;

        #region list列表
        public override ActionResult Index(int? id)
        {
            string err = CheckCom.CheckedBeforeBegin(_flowid);//检查数据库是否建表等
            if (string.IsNullOrEmpty(err))
            {
                object param = BasicHelp.GetParamObject(CurrentUser);//系统参数可作为数据规范的条件参数
                ViewData["SelectList"] = Data02Service.GetSelectList("list", param);//为查询条件(下拉选择)准备动态数据
                ViewData["SearchMode"] = BasicHelp.GetSearchMode(_flowid);//查看编辑模式-录入表管理/权限设置可设置

                base.Index(id);
                return View();
            }
            else
            {
                return Json(err);
            }
        }

        /// <summary>
        /// 操作前判断及处理 列表页面点击新增数据按钮触发
        /// </summary>
        /// <param name="type">类型add</param>
        /// <param name="listid">自增长id,默认0</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BeforeAdd(string type, string listid)
        {
            string err = "";
            int _listid = 0;//自动编号及子表数据初始化等情况需插入一条空数据并返回自增长id
            try
            {
                //得到步骤第一步，基本信息录入表返回空
                CurrentPrcsEntity mPrcs = FlowCom.GetFistFlowStept(_flowid);
                err = mPrcs == null ? "" : mPrcs.ERR;

                if (string.IsNullOrEmpty(err))
                {
                    //添加前检测，如权限校验、录入表是否被禁用等
                    err = CheckCom.CheckedBeforeAdd(_flowid, mPrcs, CurrentUser);
                    if (string.IsNullOrEmpty(err))
                    {
                        //添加前处理，如自动编号及子表数据初始化等
                        _listid = Data02Service.BeforeAdd(ref err, _flowid, _maintbid, _tbname, mPrcs, CurrentUser);

                    }
                }

                if (string.IsNullOrEmpty(err))
                {
                    string pid = mPrcs == null ? "-1" : mPrcs.PrcsId;//当前流程步骤id
                    return Json(SuccessTip("", _listid.ToString(), pid));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 操作前判断及处理 列表页面点击编辑按钮触发
        /// </summary>
        /// <param name="type">类型edit</param>
        /// <param name="listid">自增长id：基础信息表对应本表字段listid，一般录入表对应表flowrunlist中的listid</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BeforeEdit(string type, string listid)
        {
            string err = "";
            //得到当前流程步骤，基本信息录入表返回空
            CurrentPrcsEntity mPrcs = FlowCom.GetCurrentFlowStept(_flowid, listid);
            err = mPrcs == null ? "" : mPrcs.ERR;

            if (string.IsNullOrEmpty(err))
            {
                //编辑前检测-如权限校验、录入表是否被禁用、是否锁定等
                err = CheckCom.CheckedBeforeEdit(_flowid, listid, mPrcs, CurrentUser);
                if (string.IsNullOrEmpty(err))
                {
                    //编辑前数据处理，可手动精简
                    err = Data02Service.BeforeEdit(_flowid, listid, mPrcs, CurrentUser);
                }
            }

            if (string.IsNullOrEmpty(err))
            {
                string pid = mPrcs == null ? "-1" : mPrcs.PrcsId;//当前流程步骤id
                return Json(SuccessTip("", listid, pid));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        /// <summary>
        /// 数据查看 列表页面点击查看按钮触发
        /// wwwroot/Reports文件夹中保存样式模板文件 下载安装MicrosoftRDLC报表设计器可编辑设计其样式
        /// </summary>
        /// <param name="listid">基础信息表对应本表字段listid，一般录入表对应表flowrunlist中的listid</param>
        /// <returns></returns>
        [HttpGet]
        public  ActionResult Detail(string listid)
        {
            TbBasicEntity model = PageCom.GetAttBut(listid, _flowid);//附件按钮设置，可在录入表管理/页面按钮中设置

            ViewData["ListId"] = listid;
            ViewBag.ButPdf = model.ButPDFDetails;//Pdf是否显示 1显示
            ViewBag.ButDoc = model.ButDOCDetails;//DOC是否显示 1显示
            ViewBag.ButExcel = model.ButExcelDetails;//Excel是否显示 1显示
            ViewData["ButAtt"] = model.ButAtt;//附件按钮是否显示 1显示
            ViewData["ButAttTxt"] = model.ButAttTxt;//附件按钮标题
            ViewData["UserId"] = CurrentUser == null ? "un_defined" : CurrentUser.Account;

            return View();
        }

        /// <summary>
        /// 数据删除 列表页面点击删除按钮触发
        /// </summary>
        /// <param name="listid">基础信息表对应本表字段listid，一般录入表对应表flowrunlist中的listid</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult Delete(string listid)
        {
            string err = "";

            //得到当前流程步骤 基本信息录入表返回空
            CurrentPrcsEntity mPrcs = FlowCom.GetCurrentFlowStept(_flowid, listid);
            err = mPrcs == null ? "" : mPrcs.ERR;

            if (string.IsNullOrEmpty(err))
            {
                //删除前检测 包括权限校验、录入表是否被禁用、数据是否锁定等
                err = CheckCom.CheckedBeforeDel(_flowid, listid, mPrcs, CurrentUser);
            }

            try
            {
                if (string.IsNullOrEmpty(err))
                {
                    err = Data02Service.DeleteByID(listid, _flowid) ? "" : "操作失败";
                    //删除后数据处理,默认什么也不做，可自定义可手动删除
                    if (string.IsNullOrEmpty(err))
                    {
                        err = Data02Service.AfterDelete(_flowid, listid, mPrcs, CurrentUser);
                    }
                }

                if (string.IsNullOrEmpty(err))
                {
                    //Record.Add(CurrentUser == null ? "un_defined" : CurrentUser.Account, listid, _flowid, "数据删除");
                    return Json(SuccessTip("删除成功"));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 数据查询 列表页面点击查询按钮触发
        /// </summary>
        /// <param name="model">主表数据model</param>
        /// <param name="pageInfo">页面信息，包括行数、排序等</param>
        /// <param name="isAll">为all则显示所有</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetPageListBySearch(Data02Model model, PageInfoEntity pageInfo, string isAll)
        {
            pageInfo.field = "listid";//排序field
            pageInfo.order = "desc";

            long total = 0;
            IEnumerable<dynamic> list = Data02Service.GetSearchList(ref total, model, pageInfo, CurrentUser, _flowid, _maintbid, isAll);

            if (list == null)
            {
                return Json(ErrorTip("参数错误"));
            }
            else
            {
                var result = new { code = 0, msg = "", count = total, data = list };
                return Json(result);
            }
        }
        #endregion list列表

        #region Form编辑
        /// <summary>
        /// 录入页面显示
        /// </summary>
        /// <param name="type">add或edit</param>
        /// <param name="listid">基础信息表对应本表字段listid，一般录入表对应表flowrunlist中的listid</param>
        /// <param name="pid">当前流程步骤id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form(string type, string listid, string pid)
        {
            int _listid = listid == null ? 0 : int.Parse(listid);
            object param = BasicHelp.GetParamObject(CurrentUser);//数据规范中的筛选条件可使用系统参数
            ViewData["UserId"] = CurrentUser == null ? "un_defined" : CurrentUser.Account;//用户id
            ViewData["Type"] = type;//add或edit
            ViewData["ListId"] = _listid;//基础信息表listid自增长，一般录入表listid来源于flowrunlist
            ViewData["Split"] = _split;//字符分割 checkbox多选时使用
            ViewData["FId"] = _flowid;//流程编码
            ViewData["PrcId"] = pid;//流程步骤id
            ViewData["PrcNo"] = FlowCom.GetFlowNoByID(_flowid, pid);//流程步骤编码
            ViewData["SelectList"] = Data02Service.GetSelectList(type, param);//为下拉选择、多选、单选动态数据源赋值

            //包括为指标赋初始值--系统指标或默认值
            Data02Model model = Data02Service.GetModel(type, _listid, CurrentUser);

            if (model != null)
            {
                return View(model);
            }
            else
            {
                return Json("数据不存在");
            }
        }

        /// <summary>
        /// 下拉选择或者弹出选择后调用，完成数据读取功能--可在录入表管理/数据读取中自定义
        /// </summary>
        /// <param name="controlslist">页面控件id与text</param>
        /// <param name="gridlist">子表数据列表 无子表时为空</param>
        /// <param name="id"></param>
        /// <param name="tbid">_main或子表编码</param>
        /// <param name="indexid">下拉或者弹出选择(触发控件)对应指标编码</param>
        /// <param name="value">下拉或者弹出选择(触发控件)对应控件Value</param>
        /// <returns>返回list交由前台解析</returns>
        [HttpPost]
        public ActionResult AfterControlSelect(IEnumerable<ControlEntity> controlslist, IEnumerable<GridListEntity> gridlist, string id, string tbid, string indexid, string value)
        {
            try
            {
                //未设置数据读取则什么也不做
                List<ControlEntity> list = new List<ControlEntity>();

                return Json(SuccessTip("", list, ""));
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 数据保存 Form页面点击保存按钮触发
        /// </summary>
        /// <param name="model">主表数据model</param>
        /// <param name="gridlist">子表数据 无子表时为空</param>
        /// <param name="listid">listid=0表示增加</param>
        /// <param name="prcno">流程步骤编码</param>
        /// <param name="type">add或edit</param>
        /// <param name="att">附件id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveData(Data02Model model, IEnumerable<GridListEntity> gridlist, int listid, string prcno, string type, string att)
        {
            string err = "";

            try
            {
                //编辑
                if (listid != 0)
                {
                    err = this.Edit(model, gridlist, listid, prcno);
                }
                else
                {
                    listid = this.Add(ref err, prcno, model);

                }

                if (string.IsNullOrEmpty(err))
                {
                    err = Data02Service.AfterEdit(listid.ToString(), _flowid, FlowCom.GetFlowIDByNo(_flowid, prcno), CurrentUser);//编辑后数据处理，可自定义可手动删除

                    if (string.IsNullOrEmpty(err))
                    {
                        return Json(SuccessTip("操作成功"));
                    }
                    else
                    {
                        return Json(ErrorTip("AfterEdit函数出现错误：" + err));
                    }
                }
                else
                {
                    return Json(ErrorTip("操作失败！" + err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 转交下一步--保存数据并流程转交  Form页面点击转交按钮触发
        /// </summary>
        /// <param name="model">主表数据model</param>
        /// <param name="gridlist">子表数据列表 无子表时为空</param>
        /// <param name="listid">listid=0表示增加</param>
        /// <param name="prcno">流程步骤编码</param>
        /// <param name="type">add或edit</param>
        /// <param name="att">附件id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult NextStep(Data02Model model, IEnumerable<GridListEntity> gridlist, int listid, string prcno, string type, string att)
        {
            string err = "";

            try
            {
                //编辑
                if (listid != 0)
                {
                    err = this.Edit(model, gridlist, listid, prcno);
                }
                else
                {
                    listid = this.Add(ref err, prcno, model);

                }

                if (string.IsNullOrEmpty(err))
                {
                    err = Data02Service.AfterEdit(listid.ToString(), _flowid, FlowCom.GetFlowIDByNo(_flowid, prcno), CurrentUser);//编辑后数据处理，可自定义可手动删除

                    if (string.IsNullOrEmpty(err))
                    {
                        if (_flowid.StartsWith("bi_"))
                        {
                          return Json(ErrorTip("数据保存成功，基本信息表不能被转交"));
                        }
                        else
                        {
                          return Json(SuccessTip("", listid.ToString()));
                        }
                    }
                    else
                    {
                        return Json(ErrorTip("AfterEdit函数出现错误：" + err));
                    }
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 结束--保存数据并结束流程 Form页面点击结束按钮触发
        /// </summary>
        /// <param name="model">主表数据model</param>
        /// <param name="gridlist">子表数据 无子表时为空</param>
        /// <param name="listid">listid=0表示增加</param>
        /// <param name="prcno">流程步骤编码</param>
        /// <param name="type">add或edit</param>
        /// <param name="att">附件id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Finish(Data02Model model, IEnumerable<GridListEntity> gridlist, int listid, string prcno, string type, string att)
        {
            string err = "";

            try
            {
                //编辑
                if (listid != 0)
                {
                    err = this.Edit(model, gridlist, listid, prcno);
                }
                else
                {
                    listid = this.Add(ref err, prcno, model);

                }

                if (string.IsNullOrEmpty(err))
                {
                    err = Data02Service.AfterFinish(listid.ToString(), _flowid, FlowCom.GetFlowIDByNo(_flowid, prcno), CurrentUser);//完成后数据处理，可手动删除

                    if (string.IsNullOrEmpty(err))
                    {
                        if (_flowid.StartsWith("bi_"))
                        {
                            return Json(ErrorTip("操作成功，数据已被锁定"));
                        }
                        else
                        {
                            return Json(SuccessTip("操作成功"));
                        }
                    }
                    else
                    {
                        return Json(ErrorTip("AfterFinish函数出现错误：" + err));
                    }
                }
                else
                {
                    return Json(ErrorTip("操作失败！" + err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 添加主表数据
        /// </summary>
        /// <param name="err">错误信息</param>
        /// <param name="prcno">当前流程步骤编码</param>
        /// <param name="model">主表数据</param>
        /// <returns></returns>
        public int Add(ref string err, string prcno, Data02Model model)
        {
            int listid = 0;

            DataTable dtMain = DataTableHelp.ModelToDataTable<Data02Model>(model);//model转换DataTable
            //进行主键和唯一键检测，主键和唯一键可在录入表指标属性中设置
            err = CheckCom.CheckMainTbValueBKAndUnique(listid, _maintbid, model, dtMain);
            if (string.IsNullOrEmpty(err))
            {
                //进行主表自定义校验，自定义校验可在录入表管理/录入校验中设置
                err = CheckCom.CheckMainTbValidate(dtMain, _flowid, prcno);
                if (string.IsNullOrEmpty(err))
                {
					model.createTime = DateTime.Now.ToString();
					model.isLock = "0";
					//model.createUser = CurrentUser.Account;
					//model.createDept = CurrentUser.DeptNo;
					//model.createDeptStr = CurrentUser.DeptNoStr;
					//model.runName = BaseUtil.GetRunName(CurrentUser.Account, _flowid, _tbname, dtMain);
					
					listid = Data02Service.InsertReturnID(model);
					
					err = listid > 0 ? "" : "添加失败";
					if (string.IsNullOrEmpty(err))
					{
						//Record.Add(CurrentUser == null ? "un_defined" : CurrentUser.Account, listid.ToString(), _flowid, "数据添加");
					}
                }
            }

            return listid;
        }

        /// <summary>
        /// 编辑主表数据
        /// </summary>
        /// <param name="model">主表数据</param>
        /// <param name="gridlist">子表数据列表 无子表时为空</param>
        /// <param name="listid">主表自增长id：基础信息表对应主表listid，一般录入表对应表flowrunlist中的listid</param>
        /// <param name="prcno">当前流程步骤编码</param>
        /// <returns></returns>
        public string Edit(Data02Model model, IEnumerable<GridListEntity> gridlist, int listid, string prcno)
        {
            string err = "";

            //有子表时会遍历数据并判断取数


            if (string.IsNullOrEmpty(err))
            {
                model.ListId = listid;

                DataTable dtMain = DataTableHelp.ModelToDataTable<Data02Model>(model);
                //主键和唯一检测，主键和唯一可在录入表指标属性中设置
                err = CheckCom.CheckMainTbValueBKAndUnique(listid, _maintbid, model, dtMain);
                if (string.IsNullOrEmpty(err))
                {
                    //自定义校验，自定义校验可在录入表管理录入校验中设置
                    err = CheckCom.CheckTbValidate(dtMain, gridlist, _flowid, prcno);
                    if (string.IsNullOrEmpty(err))
                    {
                        //子表数据编辑
		

                        string updateFields = Data02Service.GetUpdateFields(prcno);//可编辑列
                        if(!string.IsNullOrEmpty(updateFields))
                        {

                            //编辑主表
                            err = Data02Service.UpdateByWhere("where listid=" + listid + "", updateFields, model) > 0 ? "" : "编辑失败";
                        }

                        //Record.Add(CurrentUser == null ? "un_defined" : CurrentUser.Account, listid.ToString(), _flowid, "数据编辑");
                    }
                }
            }

            return err;
        }


        #endregion Form编辑




    }
}
