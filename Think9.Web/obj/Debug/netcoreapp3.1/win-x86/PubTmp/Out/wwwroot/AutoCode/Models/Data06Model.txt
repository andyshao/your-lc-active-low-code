﻿/*******************************************************************************
 * Create:admin 2022-08-29 17:28:04
 * Description: YoursLC有源低代码 实体类
*********************************************************************************/

using System;
using DapperExtensions;
namespace Think9.Models 
{
	/// <summary>
	/// 实体类  生成时间：2022-08-29 17:28:04
	/// </summary>
	 [Table("tb_Data06")]
	public class Data06Model : MainTBEntity
	{
		#region Model
		/// <summary>
		/// 编码
		/// </summary>
		public string inCode { get; set; }
		/// <summary>
		/// 名称
		/// </summary>
		public string inName { get; set; }
		#endregion Model
	}
}
