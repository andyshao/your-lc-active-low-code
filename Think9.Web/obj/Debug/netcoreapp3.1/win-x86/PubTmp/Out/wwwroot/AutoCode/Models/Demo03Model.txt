﻿/*******************************************************************************
 * Create:admin 2022-08-29 12:33:14
 * Description: YoursLC有源低代码 实体类
*********************************************************************************/

using System;
using DapperExtensions;
namespace Think9.Models 
{
	/// <summary>
	/// 实体类  生成时间：2022-08-29 12:33:14
	/// </summary>
	 [Table("tb_Demo03")]
	public class Demo03Model : MainTB2Entity
	{
		#region Model
		/// <summary>
		/// 调薪时间
		/// </summary>
		public DateTime? inSalaryAdjustmentTime { get; set; }
		/// <summary>
		/// 调薪时间 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public DateTime? inSalaryAdjustmentTime_Exa { get; set; }
		/// <summary>
		/// 编码
		/// </summary>
		public string inCode { get; set; }
		/// <summary>
		/// 名称
		/// </summary>
		public string inName { get; set; }
		/// <summary>
		/// 类别
		/// </summary>
		public string inCategory { get; set; }
		/// <summary>
		/// 计量单位
		/// </summary>
		public string inUnitOfMeasurement { get; set; }
		/// <summary>
		/// 备注2
		/// </summary>
		public string inNote2 { get; set; }
		/// <summary>
		/// 员工编码
		/// </summary>
		public string inEmployeeCode { get; set; }
		/// <summary>
		/// 员工姓名
		/// </summary>
		public string inEmployeeName { get; set; }
		/// <summary>
		/// 员工部门
		/// </summary>
		public string inEmployeeDepartment { get; set; }
		/// <summary>
		/// 员工岗位
		/// </summary>
		public string inEmployeePosition { get; set; }
		/// <summary>
		/// 图片
		/// </summary>
		public string inPicture { get; set; }
		/// <summary>
		/// 图片 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public string inPicture_Exa { get; set; }
		/// <summary>
		/// 整数
		/// </summary>
		public int? Integer01 { get; set; }
		/// <summary>
		/// 整数 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public int? Integer01_Exa { get; set; }
		/// <summary>
		/// 数字
		/// </summary>
		public decimal? inNumber { get; set; }
		/// <summary>
		/// 数字 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public decimal? inNumber_Exa { get; set; }
		/// <summary>
		/// 数量
		/// </summary>
		public decimal? inSL { get; set; }
		/// <summary>
		/// 数量 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public decimal? inSL_Exa { get; set; }
		/// <summary>
		/// 备注9
		/// </summary>
		public string inNote9 { get; set; }
		/// <summary>
		/// 测试0001
		/// </summary>
		public string inCS0001 { get; set; }
		#endregion Model
	}
}
