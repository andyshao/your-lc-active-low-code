﻿/*******************************************************************************
 * Create:admin 2022-08-29 12:33:15
 * Description: YoursLC有源低代码 实体类
*********************************************************************************/

using System;
using DapperExtensions;
namespace Think9.Models 
{
	/// <summary>
	/// 实体类  生成时间：2022-08-29 12:33:15
	/// </summary>
	 [Table("tb_Demo04")]
	public class Demo04Model : MainTB2Entity
	{
		#region Model
		/// <summary>
		/// 测试0001
		/// </summary>
		public string inCS0001 { get; set; }
		/// <summary>
		/// 测试0001 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public string inCS0001_Exa { get; set; }
		/// <summary>
		/// 整数
		/// </summary>
		public int? Integer01 { get; set; }
		/// <summary>
		/// 整数 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public int? Integer01_Exa { get; set; }
		/// <summary>
		/// 图片3
		/// </summary>
		public string inPicture3 { get; set; }
		/// <summary>
		/// 图片3 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public string inPicture3_Exa { get; set; }
		/// <summary>
		/// 备注2
		/// </summary>
		public string inNote2 { get; set; }
		/// <summary>
		/// 图片
		/// </summary>
		public string inPicture { get; set; }
		/// <summary>
		/// 图片 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public string inPicture_Exa { get; set; }
		/// <summary>
		/// 数字
		/// </summary>
		public decimal? inNumber { get; set; }
		/// <summary>
		/// 数字 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public decimal? inNumber_Exa { get; set; }
		/// <summary>
		/// 编码
		/// </summary>
		public string inCode { get; set; }
		/// <summary>
		/// 类别
		/// </summary>
		public string inCategory { get; set; }
		/// <summary>
		/// 名称
		/// </summary>
		public string inName { get; set; }
		/// <summary>
		/// 备注9
		/// </summary>
		public string inNote9 { get; set; }
		/// <summary>
		/// 入职日期
		/// </summary>
		public DateTime? inEntryDate { get; set; }
		/// <summary>
		/// 入职日期 【额外--用于数据转化或查询条件】
		/// </summary>
		[Computed]
		public DateTime? inEntryDate_Exa { get; set; }
		#endregion Model
	}
}
