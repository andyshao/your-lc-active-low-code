﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using Think9.CreatCode;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Table;

namespace Think9.Services.Basic
{
    public class CodeBuildServices
    {
        private TbBasicService TbBasicService = new TbBasicService();
        private ComService ComService = new ComService();
        private string _dbtype = DBType.mysql.ToString();//数据库类型

        public CodeBuildEntity LookCodeBuild(string siteRoot, string maintbid)
        {
            Think9.Models.CodeBuildEntity model = new CodeBuildEntity();
            model.TbId = maintbid;

            string str = "";
            string sontbid = "";

            string sql = "select * from TbIndex order by TbId,IndexOrderNo";
            DataTable _dtindex = ComService.GetDataTable(sql);

            sql = "select TbId,TbName,ParentId from tbbasic where ParentId='" + maintbid + "'";
            DataTable sonlist = ComService.GetDataTable(sql);

            sql = "select * from tbbasic ";
            DataTable allTB = ComService.GetDataTable(sql);

            sql = "select * from tbbasic2 ";
            DataTable allTB2 = ComService.GetDataTable(sql);

            sql = "select * from rulesingle";
            DataTable dtrulesingle = ComService.GetDataTable(sql);

            sql = "select * from RuleMultiple";
            DataTable dtrulemultiple = ComService.GetDataTable(sql);

            sql = "select * from RuleMultiplefiled order by RuleId,FiledOrder";
            DataTable dtrulemultiplefiled = ComService.GetDataTable(sql);

            sql = "select * from ruletree";
            DataTable dtruletree = ComService.GetDataTable(sql);

            sql = "select * from flow";
            DataTable dtflow = ComService.GetDataTable(sql);

            sql = "select * from flowprcs order by FlowId,PrcsNo";
            DataTable dtflowprcs = ComService.GetDataTable(sql);

            sql = "select * from tbbut order by TbId";
            DataTable dtTbBut = ComService.GetDataTable(sql);

            sql = "SELECT TbRelation.*,RelationList.* FROM  TbRelation INNER JOIN RelationList ON TbRelation.RelationId = RelationList.RelationId  ";
            DataTable dtVIEW_TbRelationRD = ComService.GetDataTable(sql);

            sql = "select * from RelationRD order by RelationId";
            DataTable dtRelationRD = ComService.GetDataTable(sql);

            sql = "select * from RelationRDField order by FillIndexId";
            DataTable dtRelationRDField = ComService.GetDataTable(sql);

            DataTable dtindex = DataTableCom.GetIndexList(allTB, _dtindex);

            List<valueTextEntity> list = new List<valueTextEntity>();

            string noprefix = maintbid.Replace("tb_", "");
            string FilePathViews = siteRoot + "\\AppCreatCode\\Views\\" + noprefix + "\\";//
            string FilePathCode = siteRoot + "\\AppCreatCode\\";//

            CreatModel CreatModel = new CreatModel(maintbid, allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree);

            CreatController CreatController = new CreatController(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtVIEW_TbRelationRD, dtRelationRD, dtRelationRDField, dtTbBut, dtflow, dtflowprcs);

            CreatCshtml CreatCshtml = new CreatCshtml(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtVIEW_TbRelationRD, dtRelationRD, dtRelationRDField, dtTbBut, dtflow, dtflowprcs);

            CreatJS CreatJS = new CreatJS(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtflow, dtflowprcs);

            CreatService CreatService = new CreatService(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtVIEW_TbRelationRD, dtRelationRD, dtRelationRDField, dtflow, dtflowprcs);

            CreatTbXml CreatTbXml = new CreatTbXml(maintbid, allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree);

            CreatRdlc CreatRdlc = new CreatRdlc(maintbid, allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree);

            //主表model
            StringPlus sModel = new StringPlus();
            str = CreatModel.CreatModelCode(maintbid);
            sModel.AppendLine(str);
            //子表model
            foreach (DataRow row in sonlist.Rows)
            {
                sontbid = row["tbid"].ToString();

                sModel.AppendLine("");
                sModel.AppendLine("");
                str = CreatModel.CreatSonModelCode(sontbid);
                sModel.AppendLine(str);
            }
            model.Models = sModel.Value;

            StringPlus sService = new StringPlus();
            str = CreatService.CreatMainServiceCode(siteRoot, maintbid);
            sService.AppendLine(str);
            //子表Service
            foreach (DataRow row in sonlist.Rows)
            {
                sontbid = row["tbid"].ToString();
                string sontbname = row["TbName"].ToString();

                sService.AppendLine("");
                sService.AppendLine("");
                str = CreatService.CreatSonServiceCode(siteRoot, maintbid, sontbid, sontbname);
                sService.AppendLine(str);
            }
            model.Services = sService.Value;

            StringPlus sController = new StringPlus();
            str = CreatController.CreatControllerCode(siteRoot, maintbid);
            sController.AppendLine(str);
            model.Controllers = sController.Value;

            StringPlus sListCshtml = new StringPlus();
            str = CreatCshtml.CreatListCshtmlCode(siteRoot, maintbid);
            sListCshtml.AppendLine(str);
            model.Views_Index = sListCshtml.Value;

            StringPlus sFormCshtml = new StringPlus();
            str = CreatCshtml.CreatFormCshtmlCode(siteRoot, maintbid);
            sFormCshtml.AppendLine(str);
            model.Views_Form = sFormCshtml.Value;

            StringPlus sDetailCshtml = new StringPlus();
            str = CreatCshtml.CreatDetailCode(siteRoot, maintbid);
            sDetailCshtml.AppendLine(str);
            model.Views_Detail = sDetailCshtml.Value;

            StringPlus sJS = new StringPlus();
            str = CreatJS.CreatJSCode(siteRoot, maintbid);
            sJS.AppendLine(str);
            model.Self_JS = sJS.Value;

            str = CreatTbXml.CreatTbXmlAuto(siteRoot, maintbid, "2");
            Think9.Util.Helper.FileHelper.CreateFile(FilePathCode + "TbXml\\" + maintbid.Replace("tb_", "") + ".xml", str);

            DataSet dsFormXml = new DataSet();
            dsFormXml.ReadXml(FilePathCode + "TbXml\\" + maintbid.Replace("tb_", "") + ".xml");

            StringPlus sRdlc = new StringPlus();
            str = CreatRdlc.CreatTbRdlc(dsFormXml, maintbid, siteRoot);
            sRdlc.AppendLine(str);
            model.Reports_Rdlc = sRdlc.Value;

            StringPlus sCreatDataTable = new StringPlus();
            TbBasicEntity mTb = TbBasicService.GetByWhereFirst("where TbId=@TbId", new { TbId = maintbid });
            if (mTb != null)
            {
                string type = mTb.ParentId == "" ? "1" : "2";
                str = ComService.GetCreatDBStr(maintbid, type, mTb.FlowId);
                sCreatDataTable.AppendLine(str);

                foreach (DataRow dr in ComService.GetDataTable("Select TbId AS id  from tbbasic where ParentId='" + maintbid + "' ").Rows)
                {
                    str = ComService.GetCreatDBStr(dr["id"].ToString(), "2", mTb.FlowId);
                    sCreatDataTable.AppendLine(str);
                }
            }
            model.CreatDataTable = sCreatDataTable.Value;

            //弹出页面生成
            //主表
            str = "";
            StringPlus sMain_Pop = new StringPlus();
            List<RuleModel> mainList = CreatCom.GetRuleModelListByTbID(maintbid, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, _dbtype);
            foreach (RuleModel obj in mainList)
            {
                if (obj.ControlType == "1")
                {
                    if (obj.RuleType == "6" || obj.RuleType == "7" || obj.RuleType == "8")
                    {
                        string FrmTB = obj.FrmTB;
                        string SearchFiled = obj.SearchFiled;
                        string flag = obj.IndexId;

                        string tbIdnow = obj.TbId;

                        str = CreatCshtml.CreatPopUpCshtmlCode(siteRoot, obj.list, maintbid, obj.RuleType, FrmTB, SearchFiled, flag, obj.isSelMuch);
                        sMain_Pop.AppendLine(str);
                        sMain_Pop.AppendLine("");
                        sMain_Pop.AppendLine("");
                    }
                }
            }
            model.Views_Main_Pop = sMain_Pop.Value;

            //子表
            str = "";
            StringPlus sGrid_Pop = new StringPlus();
            foreach (DataRow row in sonlist.Rows)
            {
                sontbid = row["tbid"].ToString();
                List<RuleModel> sonList = CreatCom.GetRuleModelListByTbID(sontbid, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, _dbtype);
                foreach (RuleModel obj in sonList)
                {
                    if (obj.ControlType == "1")
                    {
                        if (obj.RuleType == "6" || obj.RuleType == "7" || obj.RuleType == "8")
                        {
                            string FrmTB = obj.FrmTB;
                            string SearchFiled = obj.SearchFiled;
                            string flag = sontbid.Replace("tb_", "") + obj.IndexId;

                            string tbIdnow = obj.TbId;

                            str = CreatCshtml.CreatPopUpCshtmlCode(siteRoot, obj.list, maintbid, obj.RuleType, FrmTB, SearchFiled, flag, obj.isSelMuch);
                            sGrid_Pop.AppendLine(str);
                            sGrid_Pop.AppendLine("");
                            sGrid_Pop.AppendLine("");
                        }
                    }
                }
            }
            model.Views_Grid_Pop = sGrid_Pop.Value;

            return model;
        }

        public void CreatCode(string idsStr, string siteRoot, string prefix, CurrentUserEntity user, string type)
        {
            string tbid = "";
            BaseConfigModel.Creator = user == null ? "un_defined" : user.Account;

            string sql = "select * from TbIndex order by TbId,IndexOrderNo";
            DataTable _dtindex = ComService.GetDataTable(sql);

            sql = "select * from tbbasic ";
            DataTable allTB = ComService.GetDataTable(sql);

            sql = "select * from tbbasic2 ";
            DataTable allTB2 = ComService.GetDataTable(sql);

            sql = "select * from rulesingle";
            DataTable dtrulesingle = ComService.GetDataTable(sql);

            sql = "select * from RuleMultiple";
            DataTable dtrulemultiple = ComService.GetDataTable(sql);

            sql = "select * from RuleMultiplefiled order by RuleId,FiledOrder";
            DataTable dtrulemultiplefiled = ComService.GetDataTable(sql);

            sql = "select * from ruletree";
            DataTable dtruletree = ComService.GetDataTable(sql);

            sql = "select * from flow";
            DataTable dtflow = ComService.GetDataTable(sql);

            sql = "select * from flowprcs order by FlowId,PrcsNo";
            DataTable dtflowprcs = ComService.GetDataTable(sql);

            sql = "select * from tbbut order by TbId";
            DataTable dtTbBut = ComService.GetDataTable(sql);

            sql = "SELECT TbRelation.*,RelationList.* FROM  TbRelation INNER JOIN RelationList ON TbRelation.RelationId = RelationList.RelationId  ";
            DataTable dtVIEW_TbRelationRD = ComService.GetDataTable(sql);

            sql = "select * from RelationRD order by RelationId";
            DataTable dtRelationRD = ComService.GetDataTable(sql);

            sql = "select * from RelationRDField order by FillIndexId";
            DataTable dtRelationRDField = ComService.GetDataTable(sql);

            DataTable dtindex = DataTableCom.GetIndexList(allTB, _dtindex);

            var idsArray = idsStr.Substring(0, idsStr.Length - 1).Split(',');
            string[] arr = BaseUtil.GetStrArray(idsStr, ",");// 以;分割
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                if (arr[i] != null && arr[i].ToString().Trim() != "")
                {
                    tbid = arr[i].ToString().Trim();

                    sql = "select TbId,TbName,ParentId from tbbasic where ParentId='" + tbid + "'";
                    DataTable sonlist = ComService.GetDataTable(sql);

                    if (type == "release")
                    {
                        this.CreatCodeForRelease(siteRoot, tbid, allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtVIEW_TbRelationRD, dtRelationRD, dtRelationRDField, dtTbBut, dtflow, dtflowprcs);
                    }
                    else
                    {
                        this.CreatCode(siteRoot, prefix, tbid, allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtVIEW_TbRelationRD, dtRelationRD, dtRelationRDField, dtTbBut, dtflow, dtflowprcs);
                    }
                }
            }
        }

        public string CompressFile(string prefix)
        {
            string siteRoot = Path.Combine(Directory.GetCurrentDirectory(), "");
            string FilePathCode = siteRoot + "\\AppCreatCode\\" + prefix + "\\";//
            string zipPath = siteRoot + "\\wwwroot\\CreatCode\\" + prefix + ".zip";

            ZipFile.CreateFromDirectory(FilePathCode, zipPath);

            Think9.Util.Helper.FileHelper.DeleteDirectory(Path.Combine(Directory.GetCurrentDirectory(), "AppCreatCode\\" + prefix + "\\"));

            string url = "<div class=\"layui-form-item\" style=\"text-align: center;\"><a href='../CreatCode/" + prefix + ".zip' target ='_blank'  class='layui-btn'>点击下载</a></div>";

            return url;
        }

        private string CreatCodeForRelease(string siteRoot, string maintbid, DataTable allTB, DataTable allTB2, DataTable sonlist, DataTable dtindex, DataTable dtrulesingle, DataTable dtrulemultiple, DataTable dtrulemultiplefiled, DataTable dtruletree, DataTable dtVIEW_TbRelationRD, DataTable dtRelationRD, DataTable dtRelationRDField, DataTable dtTbBut, DataTable dtflow, DataTable dtflowprcs)
        {
            string Err = "";
            string str = "";
            string sontbid = "";

            string noprefixTbID = maintbid.Replace("tb_", "");

            string filePathViews = siteRoot + "\\Views\\" + noprefixTbID + "\\";//
            string filePathCode = siteRoot + "\\wwwroot\\AutoCode\\";//
            string filePathJS = siteRoot + "\\wwwroot\\self_js\\";//
            string filePathXml = siteRoot + "\\wwwroot\\TbXml\\";//Reports
            string filePathReports = siteRoot + "\\wwwroot\\Reports\\";//

            CreatModel CreatModel = new CreatModel(maintbid, allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree);

            CreatController CreatController = new CreatController(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtVIEW_TbRelationRD, dtRelationRD, dtRelationRDField, dtTbBut, dtflow, dtflowprcs);

            CreatCshtml CreatCshtml = new CreatCshtml(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtVIEW_TbRelationRD, dtRelationRD, dtRelationRDField, dtTbBut, dtflow, dtflowprcs);

            CreatJS CreatJS = new CreatJS(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtflow, dtflowprcs);

            CreatService CreatService = new CreatService(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtVIEW_TbRelationRD, dtRelationRD, dtRelationRDField, dtflow, dtflowprcs);

            CreatTbXml CreatTbXml = new CreatTbXml(maintbid, allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree);

            CreatRdlc CreatRdlc = new CreatRdlc(maintbid, allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree);

            //主表model
            str = CreatModel.CreatModelCode(maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Models\\" + noprefixTbID + "Model.txt", str);

            //子表model
            foreach (DataRow row in sonlist.Rows)
            {
                sontbid = row["tbid"].ToString();

                str = CreatModel.CreatSonModelCode(sontbid);
                Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Models\\Grid_" + sontbid.Replace("tb_", "") + "Model.txt", str);
            }

            str = "Model类 Grid_前缀的文件属于子表类 可将该文件夹中的所有文件放在AppCreatCode文件夹中";
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Models\\说明.txt", str);

            str = CreatService.CreatMainServiceCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Services\\" + noprefixTbID + "Service.txt", str);
            //子表Service
            foreach (DataRow row in sonlist.Rows)
            {
                sontbid = row["tbid"].ToString();
                string sontbname = row["TbName"].ToString();

                str = CreatService.CreatSonServiceCode(siteRoot, maintbid, sontbid, sontbname);
                Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Services\\Grid_" + sontbid.Replace("tb_", "") + "Service.txt", str);
            }

            str = "Service类 Grid_前缀的文件属于子表类  可将该文件夹中的所有文件放在AppCreatCode文件夹中";
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Services\\说明.txt", str);

            str = CreatController.CreatControllerCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Controllers\\" + noprefixTbID + "Controller.txt", str);

            str = "Controller类 为防止代码生成时覆盖，可将该文件夹中的所有文件放在AppCreatCode文件夹中";
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Controllers\\说明.txt", str);

            str = CreatCshtml.CreatListCshtmlCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "Index.cshtml", str);

            str = CreatCshtml.CreatFormCshtmlCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "Form.cshtml", str);

            str = CreatCshtml.CreatDetailCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "Detail.cshtml", str);

            str = "Grid_前缀的文件属于弹出页面 请将该文件夹中的所有文件迁移至Views文件夹中";
            Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "说明.txt", str);

            str = CreatJS.CreatJSCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathJS + maintbid.Replace("tb_", "") + ".js", str);

            str = "请将该文件夹中的所有文件迁移至wwwroot/self_js文件夹中";
            Think9.Util.Helper.FileHelper.CreateFile(filePathJS + "说明.txt", str);

            str = CreatTbXml.CreatTbXmlAuto(siteRoot, maintbid, "2");
            Think9.Util.Helper.FileHelper.CreateFile(filePathXml + maintbid.Replace("tb_", "") + ".xml", str);

            DataSet dsFormXml = new DataSet();
            dsFormXml.ReadXml(filePathXml + maintbid.Replace("tb_", "") + ".xml");

            str = CreatRdlc.CreatTbRdlc(dsFormXml, maintbid, siteRoot);
            Think9.Util.Helper.FileHelper.CreateFile(filePathReports + maintbid.Replace("tb_", "") + ".rdlc", str);

            str = "请将该文件夹中的所有文件迁移至wwwroot/Reports文件夹中 下载安装MicrosoftRDLC报表设计器可编辑设计其样式";
            Think9.Util.Helper.FileHelper.CreateFile(filePathReports + "说明.txt", str);

            //弹出页面生成
            //主表
            List<RuleModel> mainList = CreatCom.GetRuleModelListByTbID(maintbid, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, _dbtype);
            foreach (RuleModel obj in mainList)
            {
                if (obj.ControlType == "1")
                {
                    if (obj.RuleType == "6" || obj.RuleType == "7" || obj.RuleType == "8")
                    {
                        string FrmTB = obj.FrmTB;
                        string SearchFiled = obj.SearchFiled;
                        string flag = obj.IndexId;

                        string tbIdnow = obj.TbId;

                        str = CreatCshtml.CreatPopUpCshtmlCode(siteRoot, obj.list, maintbid, obj.RuleType, FrmTB, SearchFiled, flag, obj.isSelMuch);

                        Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "Grid_" + flag + ".cshtml", str);
                    }
                }
            }
            //子表
            foreach (DataRow row in sonlist.Rows)
            {
                sontbid = row["tbid"].ToString();
                List<RuleModel> sonList = CreatCom.GetRuleModelListByTbID(sontbid, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, _dbtype);
                foreach (RuleModel obj in sonList)
                {
                    if (obj.ControlType == "1")
                    {
                        if (obj.RuleType == "6" || obj.RuleType == "7" || obj.RuleType == "8")
                        {
                            string FrmTB = obj.FrmTB;
                            string SearchFiled = obj.SearchFiled;
                            string flag = sontbid.Replace("tb_", "") + obj.IndexId;

                            string tbIdnow = obj.TbId;

                            str = CreatCshtml.CreatPopUpCshtmlCode(siteRoot, obj.list, maintbid, obj.RuleType, FrmTB, SearchFiled, flag, obj.isSelMuch);

                            Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "Grid_" + flag + ".cshtml", str);
                        }
                    }
                }
            }

            return Err;
        }

        private string CreatCode(string siteRoot, string prefix, string maintbid, DataTable allTB, DataTable allTB2, DataTable sonlist, DataTable dtindex, DataTable dtrulesingle, DataTable dtrulemultiple, DataTable dtrulemultiplefiled, DataTable dtruletree, DataTable dtVIEW_TbRelationRD, DataTable dtRelationRD, DataTable dtRelationRDField, DataTable dtTbBut, DataTable dtflow, DataTable dtflowprcs)
        {
            string Err = "";
            string str = "";
            string sontbid = "";

            string noprefix = maintbid.Replace("tb_", "");
            string filePathViews = siteRoot + "\\AppCreatCode\\Views\\" + noprefix + "\\";//
            string filePathCode = siteRoot + "\\AppCreatCode\\";//
            string rootFilePathCode = siteRoot + "\\wwwroot\\AutoCode\\";//
            if (!string.IsNullOrEmpty(prefix))
            {
                filePathViews = siteRoot + "\\AppCreatCode\\" + prefix + "\\Views\\" + noprefix + "\\";//
                //rootFilePathCode = siteRoot + "\\wwwroot\\AutoCode\\" + prefix + "\\";//
                filePathCode = siteRoot + "\\AppCreatCode\\" + prefix + "\\";//
            }

            CreatModel CreatModel = new CreatModel(maintbid, allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree);

            CreatController CreatController = new CreatController(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtVIEW_TbRelationRD, dtRelationRD, dtRelationRDField, dtTbBut, dtflow, dtflowprcs);

            CreatCshtml CreatCshtml = new CreatCshtml(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtVIEW_TbRelationRD, dtRelationRD, dtRelationRDField, dtTbBut, dtflow, dtflowprcs);

            CreatJS CreatJS = new CreatJS(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtflow, dtflowprcs);

            CreatService CreatService = new CreatService(allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, dtVIEW_TbRelationRD, dtRelationRD, dtRelationRDField, dtflow, dtflowprcs);

            CreatTbXml CreatTbXml = new CreatTbXml(maintbid, allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree);

            CreatRdlc CreatRdlc = new CreatRdlc(maintbid, allTB, allTB2, sonlist, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree);

            //主表model
            str = CreatModel.CreatModelCode(maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Models\\" + noprefix + "Model.cs", str);
            Think9.Util.Helper.FileHelper.CreateFile(rootFilePathCode + "Models\\" + noprefix + "Model.txt", str);

            //子表model
            foreach (DataRow row in sonlist.Rows)
            {
                sontbid = row["tbid"].ToString();

                str = CreatModel.CreatSonModelCode(sontbid);
                Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Models\\Grid_" + sontbid.Replace("tb_", "") + "Model.cs", str);
                Think9.Util.Helper.FileHelper.CreateFile(rootFilePathCode + "Models\\Grid_" + sontbid.Replace("tb_", "") + "Model.txt", str);
            }

            str = "Model类 Grid_前缀的文件属于子表类 可将该文件夹中的所有文件放在AppCreatCode文件夹中";
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Models\\说明.txt", str);

            str = CreatService.CreatMainServiceCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Services\\" + noprefix + "Service.cs", str);
            Think9.Util.Helper.FileHelper.CreateFile(rootFilePathCode + "Services\\" + noprefix + "Service.txt", str);
            //子表Service
            foreach (DataRow row in sonlist.Rows)
            {
                sontbid = row["tbid"].ToString();
                string sontbname = row["TbName"].ToString();

                str = CreatService.CreatSonServiceCode(siteRoot, maintbid, sontbid, sontbname);
                Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Services\\Grid_" + sontbid.Replace("tb_", "") + "Service.cs", str);
                Think9.Util.Helper.FileHelper.CreateFile(rootFilePathCode + "Services\\Grid_" + sontbid.Replace("tb_", "") + "Service.txt", str);
            }

            str = "Service类 Grid_前缀的文件属于子表类  可将该文件夹中的所有文件放在AppCreatCode文件夹中";
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Services\\说明.txt", str);

            str = CreatController.CreatControllerCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Controllers\\" + noprefix + "Controller.cs", str);
            Think9.Util.Helper.FileHelper.CreateFile(rootFilePathCode + "Controllers\\" + noprefix + "Controller.txt", str);

            str = "Controller类 可将该文件夹中的所有文件放在AppCreatCode文件夹中";
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Controllers\\说明.txt", str);

            str = CreatCshtml.CreatListCshtmlCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "Index.cshtml", str);

            str = CreatCshtml.CreatFormCshtmlCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "Form.cshtml", str);

            str = CreatCshtml.CreatDetailCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "Detail.cshtml", str);

            str = "Grid_前缀的文件属于弹出页面 请将该文件夹中的所有文件迁移至Views文件夹中";
            Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "说明.txt", str);

            str = CreatJS.CreatJSCode(siteRoot, maintbid);
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "self_js\\" + maintbid.Replace("tb_", "") + ".js", str);

            str = "请将该文件夹中的所有文件迁移至wwwroot/self_js文件夹中";
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "self_js\\说明.txt", str);

            str = CreatTbXml.CreatTbXmlAuto(siteRoot, maintbid, "2");
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "TbXml\\" + maintbid.Replace("tb_", "") + ".xml", str);

            DataSet dsFormXml = new DataSet();
            dsFormXml.ReadXml(filePathCode + "TbXml\\" + maintbid.Replace("tb_", "") + ".xml");

            str = CreatRdlc.CreatTbRdlc(dsFormXml, maintbid, siteRoot);
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Reports\\" + maintbid.Replace("tb_", "") + ".rdlc", str);

            str = "请将该文件夹中的所有文件迁移至wwwroot/Reports文件夹中 下载安装MicrosoftRDLC报表设计器可编辑设计其样式";
            Think9.Util.Helper.FileHelper.CreateFile(filePathCode + "Reports\\说明.txt", str);

            //弹出页面生成
            //主表
            List<RuleModel> mainList = CreatCom.GetRuleModelListByTbID(maintbid, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, _dbtype);
            foreach (RuleModel obj in mainList)
            {
                if (obj.ControlType == "1")
                {
                    if (obj.RuleType == "6" || obj.RuleType == "7" || obj.RuleType == "8")
                    {
                        string FrmTB = obj.FrmTB;
                        string SearchFiled = obj.SearchFiled;
                        string flag = obj.IndexId;

                        string tbIdnow = obj.TbId;

                        str = CreatCshtml.CreatPopUpCshtmlCode(siteRoot, obj.list, maintbid, obj.RuleType, FrmTB, SearchFiled, flag, obj.isSelMuch);

                        Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "Grid_" + flag + ".cshtml", str);
                    }
                }
            }
            //子表
            foreach (DataRow row in sonlist.Rows)
            {
                sontbid = row["tbid"].ToString();
                List<RuleModel> sonList = CreatCom.GetRuleModelListByTbID(sontbid, dtindex, dtrulesingle, dtrulemultiple, dtrulemultiplefiled, dtruletree, _dbtype);
                foreach (RuleModel obj in sonList)
                {
                    if (obj.ControlType == "1")
                    {
                        if (obj.RuleType == "6" || obj.RuleType == "7" || obj.RuleType == "8")
                        {
                            string FrmTB = obj.FrmTB;
                            string SearchFiled = obj.SearchFiled;
                            string flag = sontbid.Replace("tb_", "") + obj.IndexId;

                            string tbIdnow = obj.TbId;

                            str = CreatCshtml.CreatPopUpCshtmlCode(siteRoot, obj.list, maintbid, obj.RuleType, FrmTB, SearchFiled, flag, obj.isSelMuch);

                            Think9.Util.Helper.FileHelper.CreateFile(filePathViews + "Grid_" + flag + ".cshtml", str);
                        }
                    }
                }
            }

            return Err;
        }
    }
}