using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using NLog.Web;
using System;
using System.Data;
using System.Text.Encodings.Web;
using Think9.Services.Base;
using Think9.CreatCode;

namespace Think9.Controllers.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //ComService comService = new ComService();
            //DataTable allTB = comService.GetDataTable("select * from tbbasic ");

            //RoslynCompile compilation = new RoslynCompile();
            //compilation.BuildAssembly2(allTB);

            var logger = NLog.Web.NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                logger.Debug("init main");
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception exception)
            {
                //NLog: catch setup errors
                logger.Error(exception, "Stopped program because of exception");
                logger.Error(exception, exception.Message);
                throw;
            }
            finally
            {
                NLog.LogManager.Shutdown();
            }
            Console.WriteLine(HtmlEncoder.Default.Encode("yourslc"));
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    string port = Think9.Services.Base.Configs.GetValue("port");//�˿ں�
                    webBuilder.UseUrls("http://*:" + port);//�˿�
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureLogging(logging =>
                {
                    //logging.ClearProviders();
                    //logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                })
                .UseNLog()  // NLog: Setup NLog for Dependency injection
                 //autofac
                .UseServiceProviderFactory(new AutofacServiceProviderFactory());
    }
}