﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using Think9.Services.Basic;

namespace Think9.Controllers.Basic
{
    public class TestController : Controller
    {
        //public ITestService TestService2 { get; set; }
        public IMemoryCache MemoryCache { get; set; }

        public TestService TestService = new TestService();

        public ActionResult Index()
        {
            //TestService.Run("111");
            MemoryCache.Set("1221", "sdasda");
            Console.WriteLine(MemoryCache.Get("1221"));
            return View();
        }
    }
}