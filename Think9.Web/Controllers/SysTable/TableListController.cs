﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using Think9.CreatCode;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Basic;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysTable")]
    public class TableListController : BaseController
    {
        private ChangeModel changeModel = new ChangeModel();
        private CodeBuildServices codeBuild = new CodeBuildServices();
        private TbBasicService TbBasicService = new TbBasicService();
        private ComService ComService = new ComService();

        //private string dbtype = DBType.mysql.ToString();//数据库类型
        private static string directoryPath = Path.Combine(Directory.GetCurrentDirectory(), "");

        public override ActionResult Index(int? id)
        {
            base.Index(id);

            return View();
        }

        [HttpPost]
        public ActionResult GetTableTreeSelect()
        {
            List<TreeGridEntity> list = TbBasicService.GetTableTreeSelect();

            var menuJson = Newtonsoft.Json.JsonConvert.SerializeObject(list);

            var result = SuccessTip("操作成功", menuJson);

            return Json(result);
        }

        [HttpGet]
        public JsonResult GetMainTb(PageInfoEntity pageInfo, string sortid, string key)
        {
            TbBasicEntity model = new TbBasicEntity();
            pageInfo.field = "OrderNo";
            string where = "where TbType='1' ";
            if (!string.IsNullOrEmpty(sortid))
            {
                where = "where TbType='1' and TbSortId='" + sortid + "'";
            }
            string _keywords = key == null ? "" : key;
            if (_keywords != "")
            {
                where += " and (TbId like @TbName OR TbName like @TbName) ";
                model.TbName = string.Format("%{0}%", _keywords);
            }

            try
            {
                var list = TbBasicService.GetMainTbList(model, pageInfo, where);
                var result = new { code = 0, msg = "", count = 999999, data = list };
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        public ActionResult TbGrid(string tbid)
        {
            ViewBag.tbid = tbid;
            return View("~/Areas/SysTable/TableList/TbGrid.cshtml");
        }

        public ActionResult TbDCI(string tbid)
        {
            ViewBag.tbid = tbid;
            return View("~/Areas/SysTable/TableList/TbDCI.cshtml");
        }

        [HttpGet]
        public JsonResult GetGridTbList(PageInfoEntity pageInfo, string tbid)
        {
            pageInfo.field = "OrderNo";
            string where = "where ParentId='" + tbid + "'";

            var list = TbBasicService.GetByWhere(where, null, null);
            foreach (TbBasicEntity obj in list)
            {
                obj.indexCount = "{" + ComService.GetTotal("tbindex", "where TbId='" + obj.TbId + "'").ToString() + "}";
                obj.TbRelationCount = "{" + ComService.GetTotal("TbRelation", "where TbID='" + obj.TbId + "'").ToString() + "}";
                //obj.TbRelationCount11 = "{" + ComService.GetTotal("TbRelation INNER JOIN RelationList ON TbRelation.RelationId = RelationList.RelationId ", "where TbRelation.TbID='" + obj.TbId + "'  and RelationList.RelationType = '11' ").ToString() + "}";
                //obj.TbRelationCount21 = "{" + ComService.GetTotal("TbRelation INNER JOIN RelationList ON TbRelation.RelationId = RelationList.RelationId ", "where TbRelation.TbID='" + obj.TbId + "'  and RelationList.RelationType = '21' ").ToString() + "}";
                //obj.TbRelationCount31 = "{" + ComService.GetTotal("TbRelation INNER JOIN RelationList ON TbRelation.RelationId = RelationList.RelationId ", "where TbRelation.TbID='" + obj.TbId + "' and RelationList.RelationType = '31' ").ToString() + "}";
                obj.TbRelationCount11 = "{" + ComService.GetSingle("SELECT COUNT(1) FROM TbRelation INNER JOIN RelationList ON TbRelation.RelationId = RelationList.RelationId where TbRelation.TbID='" + obj.TbId + "'  and RelationList.RelationType = '11' ") + "}";
                obj.TbRelationCount21 = "{" + ComService.GetSingle("SELECT COUNT(1) FROM TbRelation INNER JOIN RelationList ON TbRelation.RelationId = RelationList.RelationId where TbRelation.TbID='" + obj.TbId + "'  and RelationList.RelationType = '21' ") + "}";
                obj.TbRelationCount31 = "{" + ComService.GetSingle("SELECT COUNT(1) FROM TbRelation INNER JOIN RelationList ON TbRelation.RelationId = RelationList.RelationId where TbRelation.TbID='" + obj.TbId + "'  and RelationList.RelationType = '31' ") + "}";
                obj.ValueCheckCount = "{" + ComService.GetTotal("TbValueCheck", "where TbId='" + obj.TbId + "'").ToString() + "}";
            }

            return Json(new { code = 0, msg = "", count = list.Count(), data = list });
        }

        public ActionResult Warn(string idsStr, string from)
        {
            ViewBag.tbid = idsStr;
            ViewBag.from = string.IsNullOrEmpty(from) ? "" : from;
            return View();
        }

        [HttpPost]
        public ActionResult GetWarnList(string idsStr, string from)
        {
            return GetWarn(idsStr);
        }

        [HttpPost]
        public ActionResult GetDCIList(string tbid)
        {
            var list = TbBasicService.GetTbDCIList(tbid);

            var result = new { code = 0, msg = "", count = 999999, data = list };
            return Json(result);
        }

        [HttpPost]
        public ActionResult GetDCIList2(string tbid)
        {
            var list = TbBasicService.GetTbDCIList2(tbid);

            var result = new { code = 0, msg = "", count = 999999, data = list };
            return Json(result);
        }

        public ActionResult WarnDetails(string tbid)
        {
            string err = "";
            string file = directoryPath + "\\wwwroot\\AutoCode\\" + tbid + "_code.txt";
            string details = "";
            if (System.IO.File.Exists(file))
            {
                details = FileHelper.FileToString(file).Trim();
            }

            //DataTable allTB = ComService.GetDataTable("select * from tbbasic ");
            //details = RoslynCompile.CheckBuildAssembly(allTB, tbid, ref err);

            ViewBag.Details = details;

            return View();
        }

        private ActionResult GetWarn(string tbid)
        {
            tbid = string.IsNullOrEmpty(tbid) ? "" : tbid;

            string _tbid = "";
            string err = "";
            string str = "";
            string siteRoot = directoryPath;
            List<WarnEntity> list = new List<WarnEntity>();

            DataTable dtIndex = ComService.GetDataTable("select * from TbIndex order by TbId,IndexOrderNo");
            DataTable allTB = ComService.GetDataTable("select * from tbbasic ");
            DataTable dtFlow = ComService.GetDataTable("select * from flow ");
            DataTable dtPrcs = ComService.GetDataTable("select * from flowprcs ");

            var idsArray = tbid.Substring(0, tbid.Length - 1).Split(',');
            string[] arr = BaseUtil.GetStrArray(tbid, ",");// 以;分割
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                if (arr[i] != null && arr[i].ToString().Trim() != "")
                {
                    _tbid = arr[i].ToString().Trim();

                    foreach (DataRow dr in allTB.Rows)
                    {
                        if (dr["TbId"].ToString() == _tbid || dr["ParentId"].ToString() == _tbid)
                        {
                            if (!ComService.TabExists(dr["TbId"].ToString()))
                            {
                                list.Add(new WarnEntity { tbid = dr["tbid"].ToString(), indexid = "", Content = "未建数据表", level = "err" });
                            }
                        }
                    }

                    str = "_TB?tbid=" + _tbid;
                    if (ComService.GetTotal("sys_module", "where UrlAddress='" + _tbid.Replace("tb_", "") + "' OR UrlAddress='/" + _tbid.Replace("tb_", "") + "' OR UrlAddress='" + str + "' OR UrlAddress='/" + str + "'") == 0)
                    {
                        list.Add(new WarnEntity { tbid = _tbid, indexid = "", Content = "未建菜单", level = "err" });
                    }

                    err = "";
                    codeBuild.CreatCode(_tbid, siteRoot, "", CurrentUser, "release");
                    RoslynCompile.CheckBuildAssembly(allTB, _tbid, ref err);
                    if (!string.IsNullOrEmpty(err))
                    {
                        list.Add(new WarnEntity { tbid = _tbid, indexid = "", Content = "预编译错误：" + err, level = "err", Detial = _tbid });
                    }

                    changeModel.ReplaceCshtml(_tbid);

                    Check check = new Check(allTB, dtIndex, dtFlow, dtPrcs);
                    DataTable dt = check.GetWarn(siteRoot, _tbid);

                    foreach (DataRow dr in dt.Rows)
                    {
                        list.Add(new WarnEntity { tbid = dr["tbid"].ToString(), indexid = dr["indexid"].ToString(), Content = dr["Content"].ToString(), level = dr["level"].ToString() });
                    }
                }
            }

            var result = new { code = 0, msg = "", count = list.Count, data = list };
            return Json(result);
        }
    }
}