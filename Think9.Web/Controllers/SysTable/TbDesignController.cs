﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Think9.CreatCode;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Basic;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysTable")]
    public class TbDesignController : BaseController
    {
        private TbJson TbJson = new TbJson();
        private TbIndexService TbIndexService = new TbIndexService();
        private ComService ComService = new ComService();
        private TbBasicService TbBasicService = new TbBasicService();
        private IndexDateType IndexDtaeTypeService = new IndexDateType();

        private readonly IWebHostEnvironment _webHostEnvironment;

        public TbDesignController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
        }

        [HttpPost]
        public JsonResult GetIndexListByTbIDAndTag(string tbid, string tag)
        {
            string _tbid = tbid == null ? "" : tbid;

            IEnumerable<dynamic> list = TbIndexService.GetIndexByTbIDAndTag(tbid, tag);

            return Json(list);
        }

        [HttpPost]
        public JsonResult GetGridList(string tbid)
        {
            tbid = tbid == null ? "" : tbid;

            IEnumerable<TbBasicEntity> list = TbBasicService.GetByWhere("where ParentId=@tbid", new { tbid = tbid }, "TbId,TbName");

            return Json(list);
        }

        [HttpPost]
        public JsonResult GetIndexByTbIDAndIndexID(string tbid, string indexid)
        {
            string _tbid = tbid == null ? "" : tbid;

            IEnumerable<dynamic> list = TbIndexService.GetIndexByTbIDAndIndexID(tbid, indexid);

            return Json(list);
        }

        [HttpPost]
        public JsonResult GetJsonStr(string tbid, string type)
        {
            DataTable _dtindex = ComService.GetDataTable("select * from TbIndex order by TbId,IndexOrderNo");
            DataTable allTB = ComService.GetDataTable("select * from tbbasic ");
            TbStyle TBStyle = new TbStyle(allTB, _dtindex);
            string str = "";

            try
            {
                str = TBStyle.GetJsonStrByTbID(tbid, type);
                return Json(str);
            }
            catch (Exception ex)
            {
                str = ex.ToString();
                return Json(ErrorTip(str));
            }
        }

        [HttpPost]
        public JsonResult GetGridTableHtml(string tbid)
        {
            return Json(TbBasicService.GetGridTableHtmlByTbID(tbid));
        }

        [HttpPost]
        public JsonResult CreatJsonFile(string tbid, string json)
        {
            FormControlEntity model = new FormControlEntity();

            try
            {
                string strPath = $"{this._webHostEnvironment.WebRootPath}\\TbJson\\";

                Think9.Util.Helper.FileHelper.CreateFile(strPath + "" + tbid + ".json", json);

                DataTable _dtindex = ComService.GetDataTable("select * from TbIndex order by TbId,IndexOrderNo");
                DataTable allTB = ComService.GetDataTable("select * from tbbasic ");
                TbStyle TBStyle = new TbStyle(allTB, _dtindex);
                List<FormControlEntity> list = Conversion(TBStyle.GetFormControlList(tbid, json));

                string err = TbJson.CheckControlFromObj(list, tbid);

                if (string.IsNullOrEmpty(err))
                {
                    err = TbJson.UpTbIndexByControlFromObj(list, tbid);
                }

                if (string.IsNullOrEmpty(err))
                {
                    return Json(SuccessTip("操作成功"));
                }
                else
                {
                    return Json(ErrorTip("验证失败！" + err));
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpPost]
        public JsonResult GetJsonFromApi(string tbid)
        {
            string json = "load";
            try
            {
                string strPath = $"{this._webHostEnvironment.WebRootPath}\\TbJson\\" + tbid + ".json";

                if (Directory.Exists(Path.GetDirectoryName(strPath)))
                {
                    json = Think9.CreatCode.FileHelper.FileToString(strPath);
                }

                return Json(json);
            }
            catch (Exception ex)
            {
                string str = ex.Message;
                return Json("err");
            }
        }

        private List<FormControlEntity> Conversion(List<FormControlModel> list)
        {
            List<FormControlEntity> _list = new List<FormControlEntity>();
            foreach (FormControlModel obj in list)
            {
                FormControlEntity _obj = new FormControlEntity();

                _obj._default = obj._default;
                _obj._readonly = obj._readonly;
                _obj.index = obj.index;
                _obj.tag = obj.tag;
                _obj.label = obj.label;
                _obj.labelhide = obj.labelhide;
                _obj.name = obj.name;
                _obj.type = obj.type;
                _obj.placeholder = obj.placeholder;
                _obj.min = obj.min;
                _obj.max = obj.max;
                _obj.maxlength = obj.maxlength;
                _obj.verify = obj.verify;
                _obj.width = obj.width;
                _obj.height = obj.height;
                _obj.lay_skin = obj.lay_skin;
                _obj.labelwidth = obj.labelwidth;
                _obj.uploadtype = obj.uploadtype;
                _obj.disabled = obj.disabled;
                _obj.required = obj.required;
                _obj.lay_search = obj.lay_search;
                _obj.data_datetype = obj.data_datetype;
                _obj.data_maxvalue = obj.data_maxvalue;
                _obj.data_minvalue = obj.data_minvalue;
                _obj.data_dateformat = obj.data_dateformat;
                _obj.data_half = obj.data_half;
                _obj.theme = obj.theme;
                _obj.data_theme = obj.data_theme;
                _obj.data_color = obj.data_color;
                _obj.data_default = obj.data_default;
                _obj.data_value = obj.data_value;
                _obj.msg = obj.msg;
                _obj.textarea = obj.textarea;
                _obj.column = obj.column;

                _list.Add(_obj);
            }

            return _list;
        }
    }
}