﻿using Microsoft.AspNetCore.Mvc;
using System;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Flow;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysTable")]
    public class TbValueCheckController : BaseController
    {
        private TbValueCheck TbValueCheckService = new TbValueCheck();
        private TbBasicService TbBasicService = new TbBasicService();
        private FlowService FlowService = new FlowService();

        public ActionResult List(string tbid)
        {
            ViewBag.tbid = tbid;
            return View();
        }

        //选择适用范围
        [HttpGet]
        public ActionResult PUflow(string tbid)
        {
            ViewBag.tbid = tbid;
            return View();
        }

        //打开校验页面
        [HttpGet]
        public ActionResult PUFormula(string tbid, string fid, string some)
        {
            ViewBag.tbid = tbid;
            ViewBag.formid = fid;
            ViewBag.some = some;
            return View();
        }

        [HttpGet]
        public JsonResult GetList(TbValueCheckEntity model, PageInfoEntity pageInfo, string tbid)
        {
            pageInfo.field = "IOrder";
            var result = TbValueCheckService.GetPageByFilter(model, pageInfo, "where TbId='" + tbid + "'");
            return Json(result);
        }

        [HttpGet]
        public ActionResult GetSelectFlowList(string tbid)
        {
            var result = new { code = 0, msg = "", count = 999999, data = FlowService.GetUseableFlowListForTbValueCheck(tbid) };

            return Json(result);
        }

        [HttpGet]
        public ActionResult GetSelectIndexList(string tbid, string some)
        {
            some = some == null ? "" : some;
            var result = new { code = 0, msg = "", count = 999999, data = TbValueCheckService.GetSelectIndexList(tbid, some) };

            return Json(result);
        }

        [HttpGet]
        public ActionResult Add(string tbid)
        {
            ViewBag.tbid = tbid;
            TbBasicEntity model = TbBasicService.GetByWhereFirst("where TbId='" + tbid + "'");
            if (model != null)
            {
                ViewBag.tbname = model.TbName;
                return View();
            }
            else
            {
                return Json("错误：当前录入表对象为空");
            }
        }

        [HttpPost]
        public ActionResult Add(TbValueCheckEntity model, string tbid)
        {
            model.isUse = "1";
            var result = TbValueCheckService.Insert(model) ? SuccessTip("操作成功") : ErrorTip("操作失败");
            return Json(result);
        }

        [HttpGet]
        public ActionResult Edit(string tbid, string id)
        {
            ViewBag.tbid = tbid;
            ViewBag.id = id;

            string err = "";
            TbValueCheckEntity model = TbValueCheckService.GetByWhereFirst("where Id=" + id + "");
            TbBasicEntity mTb = TbBasicService.GetByWhereFirst("where TbId='" + tbid + "'");
            if (mTb != null)
            {
                if (model != null)
                {
                    model.TbNmae = mTb.TbName;
                    model.FlowStr_Exa = FlowService.GetFlowStrForTbValueCheck(tbid, model.FlowStr);
                }
                else
                {
                    err = "错误：当前对象为空";
                }
            }
            else
            {
                err = "错误：当前录入表对象为空";
            }

            if (string.IsNullOrEmpty(err))
            {
                return View(model);
            }
            else
            {
                return Json(err);
            }
        }

        [HttpPost]
        public ActionResult Edit(TbValueCheckEntity model, string id)
        {
            try
            {
                string where = "where id=" + id + "";
                string filed = "LeftValue,Compare,RightValue,Explain,NullCase,IOrder,isUse,FlowStr";
                var result = TbValueCheckService.UpdateByWhere(where, filed, model) > 0 ? SuccessTip("操作成功") : ErrorTip("编辑失败");
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        public ActionResult Delete(TbValueCheckEntity model, string id)
        {
            string where = "where id=" + id + "";
            var result = TbValueCheckService.DeleteByWhere(where) ? SuccessTip("删除成功") : ErrorTip("操作失败");
            return Json(result);
        }
    }
}