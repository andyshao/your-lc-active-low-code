﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Basic;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysTable")]
    public class TbSetUpController : BaseController
    {
        private TbIndexService TbIndexService = new TbIndexService();
        private ComService ComService = new ComService();
        private SortService SortService = new SortService();
        private TbBasicService TbBasicService = new TbBasicService();
        private IndexDateType IndexDtaeTypeService = new IndexDateType();
        //private readonly IWebHostEnvironment _webHostEnvironment;

        //指标类型
        private SelectList IndexDtaeTypeList
        { get { return new SelectList(IndexDtaeTypeService.GetIndexDtaeType(), "TypeId", "TypeName"); } }

        public ActionResult TbIndexList(string tbid)
        {
            ViewBag.tbid = tbid;
            return View();
        }

        public ActionResult TbJson(string tbid)
        {
            ViewBag.tbid = tbid;
            return View();
        }

        //
        public ActionResult ColumnList(string tbid)
        {
            ViewBag.tbid = tbid;
            return View();
        }

        //
        public ActionResult SelectRuleList(string tbid, string indexid, string ismuch)
        {
            ViewBag.ismuch = ismuch;

            String show = "";
            String dataType = "";
            IndexBasicService IndexBasicService = new IndexBasicService();

            if (string.IsNullOrEmpty(tbid) && string.IsNullOrEmpty(indexid))
            {
                var modelTb = TbBasicService.GetByWhereFirst("where TbId=@TbId", new { TbId = tbid });

                string tbName = modelTb == null ? "" : modelTb.TbName;
                if (tbName.Length > 20)
                {
                    tbName = tbName.Substring(0, 20) + ".. ";
                }

                var modelIndex = IndexBasicService.GetByWhereFirst("where indexid=@indexid", new { indexid = indexid });
                string IndexName = modelIndex == null ? "" : modelIndex.IndexName;
                dataType = modelIndex == null ? "" : modelIndex.IndexDataType;

                show = tbName + "" + IndexName + " - 选择数据规范";
            }

            ViewBag.tbid = tbid;
            ViewBag.indexid = indexid;
            ViewBag.show = show;
            ViewBag.datatype = dataType;

            return View();
        }

        public ActionResult EditMainIndex(string tbid, string indexid)
        {
            ViewBag.tbid = tbid;
            ViewBag.ControlType = new SelectList(ControlType.GetMainTbControlType(), "Value", "Text");
            ViewBag.ValidateList = new SelectList(ValidateList.GetValidateList(), "Value", "Text");

            string where = "where TbId=@TbId and indexid=@indexid ";
            object param = new { TbId = tbid, indexid = indexid };

            TbIndexEntity model = TbIndexService.GetByWhereFirst(where, param);
            if (model != null)
            {
                foreach (IndexDtaeTypeEntity obj in IndexDtaeTypeService.GetIndexDtaeType())
                {
                    if (model.DataType == obj.TypeId)
                    {
                        model.DataType = obj.TypeName;
                        ViewBag.DataType = obj.TypeId.Substring(0, 1);//指标类型首字符
                        break;
                    }
                }

                model.isShow = model.isShow == null ? "1" : model.isShow;

                RuleListService rulelist = new RuleListService();
                model.RuleName = rulelist.GetRuleNameByID(model.RuleId, model.isSelMuch);
                return View(model);
            }
            else
            {
                return Json("数据不存在！");
            }
        }

        public ActionResult EditColumn(string tbid, string indexid)
        {
            ViewBag.tbid = tbid;
            ViewBag.ControlType = new SelectList(ControlType.GetGridTbControlType(), "Value", "Text");
            ViewBag.ValidateList = new SelectList(ValidateList.GetValidateList(), "Value", "Text");

            string where = "where TbId=@TbId and indexid=@indexid ";
            object param = new { TbId = tbid, indexid = indexid };

            TbIndexEntity model = TbIndexService.GetByWhereFirst(where, param);
            if (model != null)
            {
                foreach (IndexDtaeTypeEntity obj in IndexDtaeTypeService.GetIndexDtaeType())
                {
                    if (model.DataType == obj.TypeId)
                    {
                        model.DataType = obj.TypeName;
                        ViewBag.DataType = obj.TypeId.Substring(0, 1);//指标类型首字符
                        break;
                    }
                }

                RuleListService rulelist = new RuleListService();
                model.RuleName = rulelist.GetRuleNameByID(model.RuleId, model.isSelMuch);
                return View(model);
            }
            else
            {
                return Json("数据不存在！");
            }
        }

        public ActionResult RemoveMainIndex(string tbid)
        {
            ViewBag.TbSortList = new SelectList(SortService.GetAll("ClassID,SortID,SortName", "ORDER BY SortOrder").Where(x => x.ClassID == "CAT_table"), "SortID", "SortName");
            ViewBag.DtaeTypeList = IndexDtaeTypeService.GetIndexDtaeTypeList();
            ViewBag.IndexSortList = new SelectList(SortService.GetAll("ClassID,SortID,SortName", "ORDER BY SortOrder").Where(x => x.ClassID == "CAT_index"), "SortID", "SortName");
            ViewBag.tbid = HttpContext.Request.Query["tbid"].ToString();
            return View();
        }

        public ActionResult DBTable(string tbid)
        {
            ViewBag.tbid = tbid;
            return View();
        }

        public ActionResult GetDbFieldist(string tbid)
        {
            DataTable dt = ComService.GetTbFieldist(tbid);

            IEnumerable<TbIndexEntity> _index = TbIndexService.GetByWhere("where TbId = @TbId", new { TbId = tbid }, "IndexId,IndexName", "ORDER BY IndexOrderNo");

            List<DbFieldEntity> list = new List<DbFieldEntity>();

            foreach (DataRow dr in dt.Rows)
            {
                DbFieldEntity obj = new DbFieldEntity();
                obj.ColumnName = dr["COLUMN_NAME"].ToString();
                obj.DataType = dr["DATA_type"].ToString();
                foreach (TbIndexEntity model in _index)
                {
                    if (obj.ColumnName == model.IndexId)
                    {
                        obj.IndexName = model.IndexName;
                    }
                }
                list.Add(obj);
            }

            var result = new { code = 0, msg = "", count = dt.Rows.Count, data = list };

            return Json(result);
        }

        public ActionResult AddColumn(string tbid)
        {
            ViewBag.tbid = tbid;
            ViewBag.DtaeTypeList = IndexDtaeTypeList;
            return View();
        }

        public ActionResult EditColumnType(string tbid, string indexid)
        {
            ViewBag.tbid = tbid;
            ViewBag.indexid = indexid;
            ViewBag.DtaeTypeList = IndexDtaeTypeList;
            return View();
        }

        [HttpPost]
        public ActionResult EditColumnType(string tbid, string indexid, string type)
        {
            string err = "";

            try
            {
                if (ComService.GetSingle("select DataType  FROM tbindex  WHERE TbId= '" + tbid + "' and IndexId= '" + indexid + "'") == type)
                {
                    return Json(ErrorTip("所选类型和原有类型相同"));
                }
                else
                {
                    string sql = ComService.GetAlterTbFieldSql(tbid, indexid, type);
                    int icount = ComService.ExecuteSql(sql);
                    sql = "update tbindex set DataType='" + type + "' WHERE TbId= '" + tbid + "' and IndexId= '" + indexid + "'";
                    ComService.ExecuteSql(sql);
                }

                return Json(SuccessTip("修改成功！"));
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpPost]
        public ActionResult AddColumn(string tbid, string name, string type)
        {
            try
            {
                string err = TbIndexService.AddGridColumn(tbid, name, type);

                if (string.IsNullOrEmpty(err))
                {
                    return Json(SuccessTip("新建成功！"));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpGet]
        public ActionResult DelColumn(string tbid)
        {
            try
            {
                string err = TbIndexService.DelGridColumn(tbid);

                if (string.IsNullOrEmpty(err))
                {
                    return Json(SuccessTip("删除成功！"));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        //打开校验页面
        [HttpGet]
        public ActionResult PUFormula(string tbid, string fid, string some)
        {
            ViewBag.tbid = tbid;
            ViewBag.formid = fid;
            ViewBag.some = some;
            return View();
        }

        [HttpPost]
        public ActionResult GetTbIndexList(TbIndexEntity model, PageInfoEntity pageInfo, string tbid)
        {
            string _tbid = tbid == null ? "" : tbid;
            if (!_tbid.StartsWith("tb_"))
            {
                _tbid = "tb_" + _tbid;
            }

            long total = 0;
            IEnumerable<dynamic> list = TbIndexService.GetTbIndexList(model, pageInfo, _tbid, ref total);

            var result = new { code = 0, msg = "", count = total, data = list };

            return Json(result);
        }

        [HttpPost]
        public ActionResult GetMainTbIndexList(TbIndexEntity model, PageInfoEntity pageInfo, string tbid)
        {
            string _tbid = tbid == null ? "" : tbid;
            if (!_tbid.StartsWith("tb_"))
            {
                _tbid = "tb_" + _tbid;
            }

            long total = 0;
            IEnumerable<dynamic> list = TbIndexService.GetTbIndexList02(model, pageInfo, _tbid, ref total);

            var result = new { code = 0, msg = "", count = total, data = list };

            return Json(result);
        }

        [HttpPost]
        public ActionResult GetGridTbIndexList(TbIndexEntity model, PageInfoEntity pageInfo, string tbid)
        {
            string _tbid = tbid == null ? "" : tbid;
            if (!_tbid.StartsWith("tb_"))
            {
                _tbid = "tb_" + _tbid;
            }

            long total = 0;
            IEnumerable<dynamic> list = TbIndexService.GetTbIndexList03(model, pageInfo, _tbid, ref total);

            var result = new { code = 0, msg = "", count = total, data = list };

            return Json(result);
        }

        [HttpGet]
        public ActionResult GetIndexListByTbID(string tbid, string fid)
        {
            tbid = tbid == null ? "" : tbid;
            List<valueTextEntity> list = TbIndexService.GetIndexByTbID(tbid, fid);
            var result = new { code = 0, msg = "", count = list.Count, data = list };

            return Json(result);
        }

        [HttpPost]
        public ActionResult AddTbIndexByStr(string tbid, string si)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(tbid))
                {
                    return Json(ErrorTip("操作失败tbid为空"));
                }
                else
                {
                    if (!tbid.StartsWith("tb_"))
                    {
                        tbid = "tb_" + tbid;
                    }
                    string sindex = si == null ? "" : si;
                    int icount = 0;
                    string[] arr = BaseUtil.GetStrArray(sindex, ",");// 以;分割
                    for (int i = 0; i < arr.GetLength(0); i++)
                    {
                        if (arr[i] != null)
                        {
                            string index = arr[i].ToString().Trim();
                            TbIndexService.AddTbIndex(ref icount, tbid, index);
                        }
                    }

                    string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;
                    Record.AddInfo(userid, tbid, "添加指标" + sindex);

                    return Json(SuccessTip("共添加" + icount.ToString() + "指标"));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpPost]
        public ActionResult RemoveTbIndexByStr(string tbid, string si)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(tbid))
                {
                    return Json(ErrorTip("操作失败tbid为空"));
                }
                else
                {
                    if (!tbid.StartsWith("tb_"))
                    {
                        tbid = "tb_" + tbid;
                    }
                    string sindex = si == null ? "" : si;
                    int icount = 0;
                    string[] arr = BaseUtil.GetStrArray(sindex, ",");// 以;分割
                    for (int i = 0; i < arr.GetLength(0); i++)
                    {
                        if (arr[i] != null)
                        {
                            string index = arr[i].ToString().Trim();

                            TbIndexService.DeleteByWhere("where TbId='" + tbid + "' and IndexId='" + index + "'");

                            //删除表javascript关系
                            TbBasicService.DeletTbIndexJs(tbid, index);
                            //删除表事件
                            TbBasicService.DeletTbEvent(tbid, index);
                            //删除自动编号
                            TbBasicService.DeleteRuleAutoNoByTbIndex(tbid, index);

                            //数据库删除字段
                            if (ComService.TabExists(tbid))
                            {
                                if (ComService.ColumnExists(tbid, index))
                                {
                                    string sql = "ALTER TABLE " + tbid + "  DROP  COLUMN  " + index + "";
                                    ComService.ExecuteSql(sql);
                                }
                            }

                            icount++;
                        }
                    }

                    string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;
                    Record.AddInfo(userid, tbid, "移除指标" + sindex);

                    return Json(SuccessTip("共移除" + icount.ToString() + "指标"));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpPost]
        public ActionResult UpTbIndexWidthAndNo(IEnumerable<TbIndexEntity> list)
        {
            var _indexid = "";
            var _tbid = "";
            var _no = "";
            var _width = "";
            var err = "";
            string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;

            foreach (TbIndexEntity obj in list)
            {
                _indexid = ((TbIndexEntity)obj).IndexId;
                _tbid = ((TbIndexEntity)obj).TbId;
                _no = ((TbIndexEntity)obj).IndexOrderNo.ToString();
                _width = ((TbIndexEntity)obj).ColumnWith.ToString();

                if (string.IsNullOrEmpty(_width))
                {
                    err += _indexid + "列宽不能为空 ";
                }
                else
                {
                    if (!ValidatorHelper.IsInteger(_width))
                    {
                        err += _indexid + "列宽不是整数 ";
                    }
                    else
                    {
                        if (int.Parse(_width) < 20)
                        {
                            err += _indexid + "列宽要大于20 ";
                        }
                    }
                }

                if (string.IsNullOrEmpty(_width))
                {
                    err += _indexid + "排序不能为空 ";
                }
                else
                {
                    if (!ValidatorHelper.IsInteger(_width))
                    {
                        err += _indexid + "排序不是整数 ";
                    }
                    else
                    {
                        if (int.Parse(_width) < 0)
                        {
                            err += _indexid + "排序要大于0 ";
                        }
                    }
                }
            }

            try
            {
                if (string.IsNullOrEmpty(err))
                {
                    foreach (TbIndexEntity obj in list)
                    {
                        _indexid = ((TbIndexEntity)obj).IndexId;
                        _tbid = ((TbIndexEntity)obj).TbId;
                        _no = ((TbIndexEntity)obj).IndexOrderNo.ToString();
                        _width = ((TbIndexEntity)obj).ColumnWith.ToString();

                        TbIndexService.UpdateByWhere("where IndexId='" + _indexid + "' and TbId='" + _tbid + "' ", "IndexOrderNo,ColumnWith", (TbIndexEntity)obj);
                        Record.AddInfo(userid, _tbid, "编辑指标属性" + _indexid + "[IndexOrderNo=" + _no + "][width=" + _width + "]");
                    }

                    return Json(SuccessTip("保存成功"));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpPost]
        public ActionResult UpTbIndexWidth(IEnumerable<TbIndexEntity> list)
        {
            var _indexid = "";
            var _tbid = "";
            var _no = "";
            var _width = "";
            var err = "";
            string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;

            DataTable dt = DataTableHelp.IEnumerableToDataTable<TbIndexEntity>(list);//转换DataTable

            foreach (TbIndexEntity obj in list)
            {
                _indexid = ((TbIndexEntity)obj).IndexId;
                _tbid = ((TbIndexEntity)obj).TbId;
                _no = ((TbIndexEntity)obj).IndexOrderNo.ToString();
                _width = ((TbIndexEntity)obj).ColumnWith.ToString();

                if (_indexid.StartsWith("v"))
                {
                    if (string.IsNullOrEmpty(_width))
                    {
                        err += _indexid + "列宽不能为空 ";
                    }
                    else
                    {
                        if (!ValidatorHelper.IsInteger(_width))
                        {
                            err += _indexid + "列宽不是整数 ";
                        }
                        else
                        {
                            if (int.Parse(_width) < 20)
                            {
                                err += _indexid + "列宽要大于20 ";
                            }
                        }
                    }
                }
            }

            try
            {
                if (string.IsNullOrEmpty(err))
                {
                    foreach (TbIndexEntity obj in list)
                    {
                        _indexid = ((TbIndexEntity)obj).IndexId;
                        _tbid = ((TbIndexEntity)obj).TbId;
                        _no = ((TbIndexEntity)obj).IndexOrderNo.ToString();
                        _width = ((TbIndexEntity)obj).ColumnWith.ToString();

                        if (_indexid.StartsWith("v"))
                        {
                            TbIndexService.UpdateByWhere("where IndexId='" + _indexid + "' and TbId='" + _tbid + "' ", "ColumnWith", (TbIndexEntity)obj);
                            Record.AddInfo(userid, _tbid, "编辑指标属性" + _indexid + "[width=" + _width + "]");
                        }
                    }

                    return Json(SuccessTip("保存成功"));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpPost]
        public ActionResult UpTbIndexSome(string tbid, string indexid, string name, bool isState)
        {
            string err = "";
            string where = "where IndexId='" + indexid + "' and TbId='" + tbid + "' ";
            TbIndexEntity model = new TbIndexEntity();

            if (name == "isEmpty")
            {
                if (isState)
                {
                    model.isEmpty = "2";
                }
                else
                {
                    model = TbIndexService.GetByWhereFirst(where);
                    if (model.isUnique == "1")
                    {
                        err += " 已设置为唯一属性，不能修改必填属性";
                    }
                    if (model.isPK == "1")
                    {
                        err += " 已设置为主键属性，不能修改必填属性";
                    }

                    model.isEmpty = "1";
                }
            }

            if (name == "isOrder")
            {
                model.isOrder = "2";
                if (isState)
                {
                    model.isOrder = "1";
                }
            }

            if (name == "isLock")
            {
                model.isLock = "2";
                if (isState)
                {
                    model.isLock = "1";
                }
            }

            if (name == "isColumnShow2")
            {
                model.isColumnShow2 = "2";
                if (isState)
                {
                    model.isColumnShow2 = "1";
                }
            }

            if (name == "isColumnShow")
            {
                model.isColumnShow = "2";
                if (isState)
                {
                    model.isColumnShow = "1";
                }
            }

            if (name == "isSearch2")
            {
                model.isSearch2 = "2";
                if (isState)
                {
                    model.isSearch2 = "1";
                }
            }

            if (name == "isSearch")
            {
                model.isSearch = "2";
                if (isState)
                {
                    model.isSearch = "1";
                }
            }

            if (name == "isShow")
            {
                model.isShow = "1";
                if (isState)
                {
                    model.isShow = "2";
                }
            }

            if (name == "isSelMuch")
            {
                model.isSelMuch = "2";
                if (isState)
                {
                    model.isSelMuch = "1";
                }
            }

            try
            {
                if (string.IsNullOrEmpty(err))
                {
                    err = TbIndexService.UpdateByWhere(where, name, model) > 0 ? "" : "操作失败";
                }

                if (string.IsNullOrEmpty(err))
                {
                    string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;
                    Record.AddInfo(userid, tbid, "编辑指标属性：" + indexid + "[" + name + "]");

                    return Json(SuccessTip("保存成功"));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpPost]
        public ActionResult UpTbIndexByTbIndexID(TbIndexEntity model, string tbid, string indexid)
        {
            try
            {
                string err = TbIndexService.UpMainTbIndex(model, tbid, indexid);
                if (err == "")
                {
                    string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;
                    string str = "[IndexName=" + model.IndexName + "]" + "[ListStat=" + model.ListStat + "]" + "[ListHeadName=" + model.ListHeadName + "]" + "[ColumnWith=" + model.ColumnWith + "]" + "[isPK=" + model.isPK + "]" + "[isUnique=" + model.isUnique + "]" + "[isEmpty=" + model.isEmpty + "]" + "[isColumnShow=" + model.isColumnShow + "]" + "[isColumnShow2=" + model.isColumnShow2 + "]" + "[isSearch=" + model.isSearch + "]" + "[isOrder=" + model.isOrder + "]" + "[isLock=" + model.isLock + "]" + "[isLock2=" + model.isLock2 + "]" + "[isTime=" + model.isTime + "]" + "[IndexOrderNo=" + model.IndexOrderNo + "]" + "[DefaultV=" + model.DefaultV + "]" + "[RuleName=" + model.RuleName + "]" + "[RuleType=" + model.RuleType + "]" + "[RuleId=" + model.RuleId + "]" + "[isSelMuch=" + model.isSelMuch + "]" + "[ControlType=" + model.ControlType + "]...";
                    Record.AddInfo(userid, tbid, "编辑指标属性:" + indexid + str);

                    return Json(SuccessTip("保存成功"));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpPost]
        public ActionResult UpGridTbIndex(TbIndexEntity model, string tbid, string indexid)
        {
            try
            {
                string err = TbIndexService.UpGridTbIndex(model, tbid, indexid);
                if (err == "")
                {
                    string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;
                    string str = "[IndexName=" + model.IndexName + "]" + "[ListStat=" + model.ListStat + "]" + "[ListHeadName=" + model.ListHeadName + "]" + "[ColumnWith=" + model.ColumnWith + "]" + "[isPK=" + model.isPK + "]" + "[isUnique=" + model.isUnique + "]" + "[isEmpty=" + model.isEmpty + "]" + "[isColumnShow=" + model.isColumnShow + "]" + "[isColumnShow2=" + model.isColumnShow2 + "]" + "[isSearch=" + model.isSearch + "]" + "[isOrder=" + model.isOrder + "]" + "[isLock=" + model.isLock + "]" + "[isLock2=" + model.isLock2 + "]" + "[isTime=" + model.isTime + "]" + "[IndexOrderNo=" + model.IndexOrderNo + "]" + "[DefaultV=" + model.DefaultV + "]" + "[RuleName=" + model.RuleName + "]" + "[RuleType=" + model.RuleType + "]" + "[RuleId=" + model.RuleId + "]" + "[isSelMuch=" + model.isSelMuch + "]" + "[ControlType=" + model.ControlType + "]...";
                    Record.AddInfo(userid, tbid, "编辑指标属性:" + indexid + str);

                    return Json(SuccessTip("保存成功"));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpPost]
        public ActionResult UpTbIndexRule(string ruleid, string tbid, string indexid)
        {
            string name = "";
            string err = "";

            string _ruleid = ruleid == null ? "" : ruleid;

            try
            {
                if (_ruleid.Trim() == "")
                {
                    return this.RemoveTbIndexRule(tbid, indexid);
                }
                else
                {
                    err = TbBasicService.UpTbIndexByTbIndexIDAndRuleID(ref name, _ruleid, tbid, indexid);
                }

                if (err == "")
                {
                    string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;
                    Record.AddInfo(userid, tbid, "编辑指标数据规范:" + indexid + "[" + _ruleid + "]");

                    return Json(SuccessTip("保存成功", name));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.ToString()));
            }
        }

        [HttpPost]
        public ActionResult RemoveTbIndexRule(string tbid, string indexid)
        {
            try
            {
                string Err = TbBasicService.RemoveTbIndexRule(tbid, indexid);

                if (Err == "")
                {
                    return Json(SuccessTip("保存成功", ""));
                }
                else
                {
                    return Json(ErrorTip(Err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpPost]
        public ActionResult UpTbIndexControlType(string typeid, string tbid, string indexid)
        {
            try
            {
                string err = TbBasicService.UpTbIndexControlType(typeid, tbid, indexid);

                if (err == "")
                {
                    string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;
                    Record.AddInfo(userid, tbid, "编辑指标控件类型:" + indexid + "[" + typeid + "]");

                    return Json(SuccessTip("保存成功"));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.ToString()));
            }
        }
    }
}