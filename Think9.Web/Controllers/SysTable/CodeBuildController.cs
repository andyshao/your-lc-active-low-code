﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Think9.CreatCode;
using Think9.Services.Base;
using Think9.Services.Basic;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysTable")]
    public class CodeBuildController : BaseController
    {
        private CodeBuildServices codeBuild = new CodeBuildServices();
        private ChangeModel changeModel = new ChangeModel();
        private TbBasicService TbBasicService = new TbBasicService();
        private SortService SortService = new SortService();
        private ComService ComService = new ComService();
        //private string _dbtype = DBType.mysql.ToString();//数据库类型

        private static string directoryPath = Path.Combine(Directory.GetCurrentDirectory(), "");

        public CodeBuildController()
        {
            //_dbtype = Think9.Services.Base.Configs.GetDBProvider("DBProvider");
        }

        public override ActionResult Index(int? id)
        {
            ViewBag.SortList = new SelectList(SortService.GetAll("ClassID,SortID,SortName", "ORDER BY SortOrder").Where(x => x.ClassID == "CAT_table"), "SortID", "SortName");
            base.Index(id);
            return View();
        }

        public ActionResult CodeBuild()
        {
            return View("~/Areas/SysTable/TableList/CodeBuild.cshtml");
        }

        public ActionResult LookCode(string tbid)
        {
            string siteRoot = directoryPath;
            Think9.Models.CodeBuildEntity model = codeBuild.LookCodeBuild(siteRoot, tbid);
            return View(model);
        }

        [HttpPost]
        public ActionResult ChangeModel(string idsStr, bool isState)
        {
            string isRelease = "release";
            if (isState)
            {
                isRelease = "";
            }

            try
            {
                changeModel.SetModel(isRelease, idsStr);

                return Json(SuccessTip("保存成功"));
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpPost]
        public ActionResult CodeBuild(string idsStr)
        {
            if (Debugger.IsAttached)
            {
                try
                {
                    string siteRoot = directoryPath;
                    string prefix = "";

                    CreatCom.CreatDESEncryptFile(directoryPath);//要删除的

                    //this.CodeBuild(idsStr, siteRoot, prefix);
                    codeBuild.CreatCode(idsStr, siteRoot, prefix, CurrentUser, "");

                    var result = SuccessTip("操作成功,生成的代码在AppCreatCode文件夹中");
                    return Json(result);
                }
                catch (Exception ex)
                {
                    return Json(ErrorTip(ex.Message));
                }
            }
            else
            {
                return Json(ErrorTip("请在本地开发模式时使用代码生成，请选择代码下载方式！"));
            }
        }

        [HttpPost]
        public ActionResult DownloadCode(string idsStr)
        {
            try
            {
                string siteRoot = directoryPath;

                string prefix = System.Guid.NewGuid().ToString("N");

                CreatCom.CreatDESEncryptFile(directoryPath);//要删除的

                //this.CodeBuild(idsStr, siteRoot, prefix);
                codeBuild.CreatCode(idsStr, siteRoot, prefix, CurrentUser, "");

                string url = codeBuild.CompressFile(prefix);

                var result = SuccessTip("操作成功", url);
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }
    }
}