﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Data;
using System.IO;
using System.Linq;
using Think9.CreatCode;
using Think9.Services.Base;
using Think9.Services.Basic;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysTable")]
    public class ReleaseController : BaseController
    {
        private CodeBuildServices codeBuild = new CodeBuildServices();
        //private TbBasicService TbBasicService = new TbBasicService();
        private SortService SortService = new SortService();
        private ComService ComService = new ComService();
        private ChangeModel changeModel = new ChangeModel();
        //private string _dbtype = DBType.mysql.ToString();//数据库类型

        private static string directoryPath = Path.Combine(Directory.GetCurrentDirectory(), "");

        public ReleaseController()
        {
            //_dbtype = Think9.Services.Base.Configs.GetDBProvider("DBProvider");
        }

        public override ActionResult Index(int? id)
        {
            ViewBag.SortList = new SelectList(SortService.GetAll("ClassID,SortID,SortName", "ORDER BY SortOrder").Where(x => x.ClassID == "CAT_table"), "SortID", "SortName");
            base.Index(id);
            return View();
        }

        [HttpPost]
        public ActionResult ChangeModel(string idsStr, bool isState)
        {
            string isRelease = "";
            if (isState)
            {
                isRelease = "release";
            }

            try
            {
                changeModel.SetModel(isRelease, idsStr);

                return Json(SuccessTip("保存成功"));
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        //重新生成
        [HttpPost]
        public ActionResult Release(string idsStr)
        {
            try
            {
                string siteRoot = directoryPath;

                string prefix = System.Guid.NewGuid().ToString("N");

                CreatCom.CreatDESEncryptFile(directoryPath);//要删除的

                codeBuild.CreatCode(idsStr, siteRoot, "", CurrentUser, "release");

                //this.CodeBuild(idsStr, siteRoot, "");

                changeModel.SetModel("release", idsStr);

                var result = SuccessTip("生成成功！将显示可能存在的问题");
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }
    }
}