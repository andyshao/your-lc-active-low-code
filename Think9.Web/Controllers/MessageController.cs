﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Data;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Basic;

namespace Think9.Controllers.Basic
{
    public class MessageController : BaseController
    {
        private NotifyService NotifyService = new NotifyService();
        private ComService ComService = new ComService();

        /// <summary>
        /// 获取公告信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetSmsCount()
        {
            string userId = CurrentUser == null ? "un_defined" : CurrentUser.Account;

            SmsEntity model = new SmsEntity();
            model.MsgCout = 0;
            string strcount1 = ComService.GetSingle("SELECT COUNT(1) FROM sms where ToId = '" + userId + "' and isRead = '2'");
            if (!string.IsNullOrEmpty(strcount1))
            {
                model.MsgCout += int.Parse(strcount1);
            }
            string strcount2 = ComService.GetSingle("SELECT COUNT(1) FROM  notify");
            if (!string.IsNullOrEmpty(strcount2))
            {
                model.MsgCout += int.Parse(strcount2);
            }

            return Json(model);
        }

        /// <summary>
        /// 获取公告信息
        /// </summary>
        /// <returns></returns>
        public JsonResult GetUnReadListJson()
        {
            DataTable dt = DataTableHelp.NewSmsDt();

            string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;
            userid = ";" + userid + ";";

            NotifyEntity model = new NotifyEntity();
            model.Readers = string.Format("%{0}%", userid);

            IEnumerable<dynamic> list = NotifyService.GetByWhere("where Readers not like @Readers", model, null, "order by publishTime desc ");

            string sql = "select * from notify";
            foreach (NotifyEntity obj in list)
            {
                DataRow row = dt.NewRow();
                row["SmsId"] = obj.NotifyId.ToString();
                row["Type"] = 0;
                row["FromId"] = obj.FromId;
                row["Subject"] = obj.Subject;
                row["Content"] = obj.Content;
                row["createTime"] = obj.publishTime.ToString();
                dt.Rows.Add(row);
            }

            string userId = CurrentUser == null ? "un_defined" : CurrentUser.Account;
            sql = "select * from sms where ToId = '" + userId + "' and isRead = '2' order by SmsId desc";
            foreach (DataRow dr in ComService.GetDataTable(sql).Rows)
            {
                DataRow row = dt.NewRow();
                row["SmsId"] = dr["SmsId"].ToString();
                row["Type"] = 99;
                row["FromId"] = dr["FromId"].ToString();
                row["Subject"] = "";
                row["Content"] = dr["Content"].ToString();
                row["createTime"] = dr["createTime"].ToString();
                dt.Rows.Add(row);
            }

            return Json(DataTableHelp.ToEnumerable<SmsEntity>(dt));
        }
    }
}