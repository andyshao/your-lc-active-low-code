﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Reporting.NETCore;
using System;
using System.Data;
using System.IO;
using Think9.Services.Base;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("Com")]
    public class RDLCReportController : BaseController
    {
        private ComService ComService = new ComService();
        private readonly IWebHostEnvironment _webHostEnvironment;

        public RDLCReportController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
        }

        public ActionResult ExportPdf(string listid, string fwid)
        {
            string err = "";
            string tbid = fwid.Replace("bi_", "tb_").Replace("fw_", "tb_");
            var report = new LocalReport();

            string strPath = $"{this._webHostEnvironment.WebRootPath}\\UserImg\\";
            string strPath2 = $"{this._webHostEnvironment.WebRootPath}\\images\\";
            var path = $"{this._webHostEnvironment.WebRootPath}\\Reports\\" + tbid.Replace("tb_", "") + ".rdlc";

            try
            {
                DataTable dt99 = PageCom.GetMainTbDt(ref err, fwid, listid, strPath, strPath2);
                DataTable dt25 = PageCom.GetGridTbDt(ref err, fwid, listid, strPath, strPath2);

                if (string.IsNullOrEmpty(err))
                {
                    using var fs = new FileStream(path, FileMode.Open);
                    report.LoadReportDefinition(fs);
                    report.EnableExternalImages = true;
                    report.DataSources.Add(new ReportDataSource("db99", dt99));
                    report.DataSources.Add(new ReportDataSource("db25", dt25));

                    //可添加报表参数
                    //var parameters = new[] { new ReportParameter("prm", "test") };
                    //report.SetParameters(parameters);

                    //pdf尺寸
                    string width = "22cm";
                    string heigh = "29.7cm";
                    string deviceInfo =
                                     "<DeviceInfo>" +
                                     "  <OutputFormat>PDF</OutputFormat>" +
                                     "  <PageWidth>" + width + "</PageWidth>" +
                                     "  <PageHeight>" + heigh + "</PageHeight>" +
                                     "  <MarginTop>1.5cm</MarginTop>" +
                                     "  <MarginLeft>0.5cm</MarginLeft>" +
                                     "  <MarginRight>0.5cm</MarginRight>" +
                                     "  <MarginBottom>1.5cm</MarginBottom>" +
                                     "</DeviceInfo>";
                    byte[] pdf = report.Render("PDF", deviceInfo);

                    return File(pdf, "application/pdf");
                }
                else
                {
                    return Json(err);
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        public ActionResult ExportDocx(string listid, string fwid)
        {
            string err = "";
            string tbid = fwid.Replace("bi_", "tb_").Replace("fw_", "tb_");
            var report = new LocalReport();

            string strPath = $"{this._webHostEnvironment.WebRootPath}\\UserImg\\";
            string strPath2 = $"{this._webHostEnvironment.WebRootPath}\\images\\";
            var path = $"{this._webHostEnvironment.WebRootPath}\\Reports\\" + tbid.Replace("tb_", "") + ".rdlc";

            try
            {
                DataTable dt99 = PageCom.GetMainTbDt(ref err, fwid, listid, strPath, strPath2);
                DataTable dt25 = PageCom.GetGridTbDt(ref err, fwid, listid, strPath, strPath2);

                if (string.IsNullOrEmpty(err))
                {
                    using var fs = new FileStream(path, FileMode.Open);
                    report.LoadReportDefinition(fs);
                    report.EnableExternalImages = true;
                    report.DataSources.Add(new ReportDataSource("db99", dt99));
                    report.DataSources.Add(new ReportDataSource("db25", dt25));

                    //可添加报表参数
                    //var parameters = new[] { new ReportParameter("prm", "test") };
                    //report.SetParameters(parameters);

                    byte[] docx = report.Render("WORDOPENXML");
                    return File(docx, "application/msword", "Export.docx");
                }
                else
                {
                    return Json(err);
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        public ActionResult ExportXlsx(string listid, string fwid)
        {
            string err = "";
            string tbid = fwid.Replace("bi_", "tb_").Replace("fw_", "tb_");
            var report = new LocalReport();

            string strPath = $"{this._webHostEnvironment.WebRootPath}\\UserImg\\";
            string strPath2 = $"{this._webHostEnvironment.WebRootPath}\\images\\";
            var path = $"{this._webHostEnvironment.WebRootPath}\\Reports\\" + tbid.Replace("tb_", "") + ".rdlc";

            try
            {
                DataTable dt99 = PageCom.GetMainTbDt(ref err, fwid, listid, strPath, strPath2);
                DataTable dt25 = PageCom.GetGridTbDt(ref err, fwid, listid, strPath, strPath2);

                if (string.IsNullOrEmpty(err))
                {
                    using var fs = new FileStream(path, FileMode.Open);
                    report.LoadReportDefinition(fs);
                    report.EnableExternalImages = true;
                    report.DataSources.Add(new ReportDataSource("db99", dt99));
                    report.DataSources.Add(new ReportDataSource("db25", dt25));

                    //可添加报表参数
                    //var parameters = new[] { new ReportParameter("prm", "test") };
                    //report.SetParameters(parameters);
                    byte[] xlsx = report.Render("EXCELOPENXML");
                    return File(xlsx, "application/msexcel", "Export.xlsx");
                }
                else
                {
                    return Json(err);
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }
    }
}