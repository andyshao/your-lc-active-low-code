﻿using Microsoft.AspNetCore.Mvc;
using System;
using Think9.Services.Basic;

namespace Think9.Controllers.Basic
{
    public class LoginOutController : Controller
    {
        private LogonLogService LogonLogService = new LogonLogService();

        // GET: Login
        public ActionResult Index()
        {
            try
            {
                //var OperatorProvider = new OperatorProvider(HttpContext);

                //OperatorProvider.WebHelper.ClearSession();
                //OperatorProvider.RemoveCurrent();
                ////return RedirectToAction("Index", "LoginOut");
                return View("~/Views/Login/Index.cshtml");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}