﻿using Microsoft.AspNetCore.Mvc;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Flow;

namespace Think9.Controllers.Basic
{
    [Area("SysFlow")]
    public class FlowController : BaseController
    {
        private FlowService FlowService = new FlowService();
        private ComService ComService = new ComService();

        public override ActionResult Index(int? id)
        {
            base.Index(id);
            return View();
        }

        [HttpGet]
        public JsonResult GetList(FlowEntity model, PageInfoEntity pageInfo, string key)
        {
            pageInfo.field = "flowid";
            string where = "where  LEFT(flowid,3) = 'fw_' and flowType='1'";

            string _keywords = key == null ? "" : key;
            if (_keywords != "")
            {
                where += " and (FlowName like @FlowName OR FlowId like @FlowName) ";
                model.FlowName = string.Format("%{0}%", _keywords);
            }

            var result = FlowService.GetPageByFilter(model, pageInfo, where);
            return Json(result);
        }

        public ActionResult Edit(string id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Edit(FlowEntity model)
        {
            return Json(SuccessTip("操作成功"));
        }
    }
}