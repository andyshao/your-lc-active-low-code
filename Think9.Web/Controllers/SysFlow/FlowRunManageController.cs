﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Data;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Basic;
using Think9.Services.Flow;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysFlow")]
    public class FlowRunManageController : BaseController
    {
        private FlowRunListService RunListService = new FlowRunListService();
        private FlowService FlowService = new FlowService();
        private ComService ComService = new ComService();
        private TbBasicService TbBasicService = new TbBasicService();

        public override ActionResult Index(int? id)
        {
            base.Index(id);

            string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;
            //用户可管理的录入表
            ViewBag.SelectFW = FlowService.GetManageFlowListByUserId(userid);

            return View("List01");
        }

        [HttpGet]
        public ActionResult Next(string listid, string flid, string type)
        {
            string msg = "";
            string sql = "select * FROM flowrunlist where listid=" + listid + " ";
            DataTable dt = ComService.GetDataTable(sql);
            if (dt.Rows.Count > 0)
            {
                msg = "编号为--" + dt.Rows[0]["ruNumber"].ToString() + "名称为--" + dt.Rows[0]["runName"].ToString() + "已交由你办理";
            }

            ViewBag.Msg = msg;
            ViewBag.ListId = listid;
            ViewBag.FwId = flid;

            if (type == "1")
            {
                return View("Next1");//固定
            }
            else
            {
                return View("Next2");//自由
            }
        }

        /// <summary>
        /// 返回数据列表
        /// </summary>
        /// <param name="model"></param>
        /// <param name="pageInfo"></param>
        /// <param name="fwid"></param>
        /// <param name="userid"></param>
        /// <param name="islock"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetManageList(FlowRunListEntity model, PageInfoEntity pageInfo, string fwid, string userid, string islock, string key)
        {
            fwid = fwid == null ? "" : fwid;
            userid = userid == null ? "" : userid;
            key = key == null ? "" : key;
            if (fwid.StartsWith("bi_"))
            {
                long total = 0;
                List<FlowRunListEntity> list = RunListService.GetManageDataList(ref total, pageInfo, CurrentUser, fwid, userid, islock, key);

                var result = new { code = 0, msg = "", count = total, data = list };
                return Json(result);
            }
            else
            {
                return Json(RunListService.GetManageDataList(model, pageInfo, CurrentUser, fwid, userid, islock, key));
            }
        }

        public ActionResult GetManageFlowList(string listid, string fwid)
        {
            string userid = CurrentUser == null ? "un_defined" : CurrentUser.Account;
            var result = new { code = 0, msg = "", count = 999999, data = FlowService.GetManageFlowListByUserId(userid) };
            return Json(result);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="idsStr"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult BatchDel(string idsStr)
        {
            string str = "";
            string id = "";
            int icount = 0;
            int ierr = 0;
            string tbid = "";
            string fwid = "";
            string islock = ""; //0未锁定1已锁定
            string[] arr = BaseUtil.GetStrArray(idsStr, "#");// 以;分割
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                if (arr[i] != null)
                {
                    string[] arr2 = BaseUtil.GetStrArray(arr[i].ToString().Trim(), ";");// 以;分割
                    if (arr2[0] != null && arr2[1] != null)
                    {
                        id = arr2[0].ToString().Trim();
                        fwid = arr2[1].ToString().Trim();
                        tbid = fwid.Replace("fw_", "tb_").Replace("bi_", "tb_");
                        if (fwid.StartsWith("bi_"))
                        {
                            islock = ComService.GetSingle("select isLock  FROM " + fwid.Replace("bi_", "tb_") + " WHERE listid= " + id);
                        }
                        else
                        {
                            islock = ComService.GetSingle("select isLock  FROM flowrunlist WHERE listid= " + id);
                        }

                        if (islock == "0")
                        {
                            AttachmentService.DelAttachment(int.Parse(id), fwid);//删除附件

                            //删除flowrunlist、flowrunprcslist中关联数据
                            if (fwid.StartsWith("fw_"))
                            {
                                ComService.ExecuteSql("delete from flowrunlist where ListId = " + id + "");
                                ComService.ExecuteSql("delete from flowrunprcslist where ListId = " + id + "");
                            }

                            //删除自动编号
                            AutoNo.DelAutoNo(id, fwid);

                            ComService.ExecuteSql("delete from " + tbid + " where ListId = " + id + "");

                            foreach (TbBasicEntity obj in TbBasicService.GetByWhere("where ParentId='" + arr2[1].ToString().Trim() + "'", null, "TbId"))
                            {
                                ComService.ExecuteSql("delete from " + obj.TbId + " where ListId = " + id + "");
                            }

                            Record.Add(CurrentUser == null ? "un_defined" : CurrentUser.Account, id, fwid, "数据删除");

                            icount++;
                        }
                        else
                        {
                            ierr++;
                        }
                    }
                }
            }

            if (ierr == 0)
            {
                return Json(SuccessTip("删除成功"));
            }
            else
            {
                string show = "删除" + icount.ToString() + "数据 " + " 失败" + ierr.ToString() + "数据-已锁定数据请解锁后再删除 ";
                return Json(ErrorTip(show));
            }
        }

        [HttpPost]
        public JsonResult BatchLock(string idsStr, string flag)
        {
            string str = "";
            string id = "";
            int icount = 0;
            int ierr = 0;
            string tbid = "";
            string fwid = "";
            string[] arr = BaseUtil.GetStrArray(idsStr, "#");// 以;分割
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                if (arr[i] != null)
                {
                    string[] arr2 = BaseUtil.GetStrArray(arr[i].ToString().Trim(), ";");// 以;分割
                    if (arr2[0] != null && arr2[1] != null)
                    {
                        id = arr2[0].ToString().Trim();
                        fwid = arr2[1].ToString().Trim();
                        tbid = fwid.Replace("fw_", "tb_").Replace("bi_", "tb_");

                        //删除flowrunlist、flowrunprcslist中关联数据
                        if (fwid.StartsWith("fw_"))
                        {
                            ComService.ExecuteSql("update flowrunlist set isLock='" + flag + "' where ListId = " + id + "");
                        }
                        else
                        {
                            ComService.ExecuteSql("update " + tbid + " set isLock='" + flag + "' where ListId = " + id + "");
                        }

                        str = "锁定数据";
                        if (flag == "0")
                        {
                            str = "解锁数据";
                        }
                        Record.Add(CurrentUser == null ? "un_defined" : CurrentUser.Account, id, fwid, str);

                        icount++;
                    }
                }
            }

            if (ierr == 0)
            {
                return Json(SuccessTip("操作成功"));
            }
            else
            {
                string show = "操作失败" + ierr.ToString() + "数据 ";
                return Json(ErrorTip(show));
            }
        }
    }
}