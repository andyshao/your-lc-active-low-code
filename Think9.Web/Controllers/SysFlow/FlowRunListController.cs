﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Flow;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysFlow")]
    public class FlowRunListController : BaseController
    {
        private FlowRunListService RunListService = new FlowRunListService();
        private FlowService FlowService = new FlowService();
        private ComService ComService = new ComService();
        private FlowRunPrcsListService PrcsListService = new Services.Flow.FlowRunPrcsListService();

        public override ActionResult Index(int? id)
        {
            base.Index(id);

            ViewBag.SelectList = FlowService.GetUseableFlowList();

            return View("List01");
        }

        //记录查看
        public ActionResult RecordList(string listid, string fwid)
        {
            ViewBag.ListId = listid;
            ViewBag.FwId = fwid;
            return View();
        }

        //工作办理
        public ActionResult BeforeWorkHand(string fwid, string listid)
        {
            string err;
            string pid = "";
            CurrentUserEntity SelfUser = CurrentUser;
            if (SelfUser != null)
            {
                CurrentPrcsEntity mPrcs = FlowCom.GetCurrentFlowStept(fwid, listid);
                err = mPrcs.ERR;
                if (string.IsNullOrEmpty(err))
                {
                    err = CheckCom.CheckedBeforeEdit(fwid, listid, mPrcs, SelfUser);
                    if (string.IsNullOrEmpty(err))
                    {
                        err = FlowCom.TakeOverPrcs(SelfUser, mPrcs);
                    }
                }
                else
                {
                    if (mPrcs.runFlag != "1")
                    {
                        err = "当前流程非待接收状态";
                    }
                }
            }
            else
            {
                err = "当前用户对象为空";
            }

            if (err == "")
            {
                return Json(SuccessTip("", pid));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        /// <summary>
        /// 返回待接收
        /// </summary>
        /// <param name="model"></param>
        /// <param name="pageInfo"></param>
        /// <param name="flid"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetToBeReceivedList(FlowRunListEntity model, PageInfoEntity pageInfo, string flid, string key)
        {
            pageInfo.returnFields = "listid, ruNumber,FlowId,isLock , runName, createUser, isFinish,relatedId,flowType,currentPrcsId,runFlag,currentPrcsName,createTime";
            pageInfo.field = "listid";
            pageInfo.order = "desc";
            string where = FlowCom.GetToBeReceivedWhere(CurrentUser, flid, key);
            var result = RunListService.GetPageByFilter(model, pageInfo, where);
            return Json(result);
        }

        /// <summary>
        /// 返回办理中
        /// </summary>
        /// <param name="model"></param>
        /// <param name="pageInfo"></param>
        /// <param name="flid"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetInProcessList(FlowRunListEntity model, PageInfoEntity pageInfo, string flid, string key)
        {
            pageInfo.returnFields = "listid, ruNumber,FlowId,isLock, runName, createUser, isFinish,relatedId,flowType,currentPrcsId,runFlag,currentPrcsName,createTime";
            pageInfo.field = "listid";
            pageInfo.order = "desc";
            string where = FlowCom.GetWorkNowWhere(CurrentUser, key, flid);
            var result = RunListService.GetPageByFilter(model, pageInfo, where);
            return Json(result);
        }

        //流程记录
        public ActionResult GetFlowRunPrcsList(string listid, string fwid)
        {
            IEnumerable<FlowRunPrcsListEntity> list = PrcsListService.GetByWhere(" where listid=" + listid + " and FlowId='" + fwid + "' ", null, null, "order by id");
            var result = new { code = 0, msg = "", count = list.Count(), data = list };
            return Json(result);
        }

        //操作记录
        public ActionResult GetFlowRunRecord(string listid, string fwid)
        {
            var result = new { code = 0, msg = "", count = 999999, data = DataTableHelp.ToEnumerable<RecordRunEntity>(ComService.GetDataTable("select * from recordrun where FlowId='" + fwid + "' and listid = '" + listid + "' order by OperateTime desc")) };
            return Json(result);
        }
    }
}