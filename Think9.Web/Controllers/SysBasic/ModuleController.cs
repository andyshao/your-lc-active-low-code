﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Linq;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Basic;

namespace Think9.Controllers.Basic
{
    [Area("SysBasic")]
    public class ModuleController : BaseController
    {
        private ModuleService ModuleService = new ModuleService();
        private ComService ComService = new ComService();
        private string split = BaseUtil.ComSplit;//字符分割 用于多选项的分割等

        [HttpGet]
        public override ActionResult Index(int? id)
        {
            base.Index(id);
            return View();
        }

        //左侧菜单 启动时调用
        [HttpGet]
        public JsonResult GetLeftTree()
        {
            object result = ModuleService.GetModuleList(CurrentUser.RoleId);
            return Json(result);
        }

        [HttpGet]
        public JsonResult GetList()
        {
            string str = "";
            var list = ModuleService.GetAll();

            DataTable dtRoleModule = ComService.GetDataTable("select * from sys_roleauthorize");
            DataTable dtRole = ComService.GetDataTable("select * from sys_role");
            //处理select、checkbox、radio等Value与Text转化
            foreach (ModuleEntity obj in list)
            {
                str = "";
                foreach (DataRow dr in dtRole.Rows)
                {
                    if (ComService.GetTotal("sys_roleauthorize", "where RoleId=" + dr["id"].ToString().Trim() + " and ModuleId=" + obj.Id + "") > 0)
                    {
                        str += dr["FullName"].ToString().Trim() + " ";
                    }
                }

                obj.RoleStr = str;
            }

            var result = new { code = 0, count = list.Count(), data = list };
            return Json(result);
        }

        [HttpGet]
        public ActionResult Add()
        {
            DataTable dt = DataTableHelp.NewValueTextDt();

            string sql = "select * from sys_role";
            foreach (DataRow dr in ComService.GetDataTable(sql).Rows)
            {
                DataRow row = dt.NewRow();
                row["ClassID"] = "sys_role";
                row["Value"] = dr["Id"].ToString();
                row["Text"] = dr["FullName"].ToString();
                dt.Rows.Add(row);
            }
            ViewBag.Split = split;//字符分割 checkbox多选时使用
            ViewBag.RoleList = DataTableHelp.ToEnumerable<valueTextEntity>(dt);
            ViewBag.SelectList = ModuleService.GetSelectTreeList();
            return View();
        }

        [HttpPost]
        public ActionResult Add(ModuleEntity model)
        {
            int id = 0;
            model.FontFamily = "";
            model.UpdateTime = DateTime.Now;
            model.Icon = model.Icon == null ? "" : model.Icon;
            model.UrlAddress = model.UrlAddress == null ? "" : model.UrlAddress;

            id = ModuleService.InsertReturnID(model);
            if (id > 0)
            {
                RoleAuthorizeService Service = new RoleAuthorizeService();
                if (!string.IsNullOrEmpty(model.RoleStr))
                {
                    string[] arr = BaseUtil.GetStrArray(model.RoleStr, split);// 以;分割
                    for (int i = 0; i < arr.GetLength(0); i++)
                    {
                        if (arr[i] != null && arr[i].ToString().Trim() != "")
                        {
                            RoleAuthorizeEntity obj = new RoleAuthorizeEntity { ButtonId = 0, ModuleId = id, RoleId = int.Parse(arr[i].ToString().Trim()) };

                            Service.Insert(obj);
                        }
                    }
                }
                return Json(SuccessTip("操作成功"));
            }
            else
            {
                return Json(ErrorTip("操作失败"));
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (Think9.Services.Base.Configs.GetValue("IsDemo") == "true")
            {
                return Json("演示模式下不能编辑菜单！");
            }
            else
            {
                DataTable dt = DataTableHelp.NewValueTextDt();

                DataTable dtRole = ComService.GetDataTable("select * from sys_role");
                foreach (DataRow dr in dtRole.Rows)
                {
                    DataRow row = dt.NewRow();
                    row["ClassID"] = "sys_role";
                    row["Value"] = dr["Id"].ToString();
                    row["Text"] = dr["FullName"].ToString();
                    dt.Rows.Add(row);
                }
                ViewBag.Split = split;//字符分割 checkbox多选时使用
                ViewBag.RoleList = DataTableHelp.ToEnumerable<valueTextEntity>(dt);
                ViewBag.SelectList = ModuleService.GetSelectTreeList();

                var model = ModuleService.GetById(id);
                if (model != null)
                {
                    ViewBag.PId = model.ParentId;

                    string str = split;
                    foreach (DataRow dr in dtRole.Rows)
                    {
                        if (ComService.GetTotal("sys_roleauthorize", "where RoleId=" + dr["id"].ToString().Trim() + " and ModuleId=" + id + "") > 0)
                        {
                            str += dr["id"].ToString().Trim() + split;
                        }
                    }
                    model.RoleStr = str;
                    return View(model);
                }
                else
                {
                    return Json("数据不存在！");
                }
            }
        }

        [HttpPost]
        public ActionResult Edit(ModuleEntity model)
        {
            model.UpdateTime = DateTime.Now;
            model.Icon = model.Icon == null ? "" : model.Icon;
            model.UrlAddress = model.UrlAddress == null ? "" : model.UrlAddress;

            string err = this.CheckParent(model.ParentId.ToString(), model.Id.ToString());
            if (err == "")
            {
                err = ModuleService.UpdateById(model) ? "" : "编辑失败";
                if (err == "")
                {
                    RoleAuthorizeService Service = new RoleAuthorizeService();
                    ComService.ExecuteSql("delete from sys_roleauthorize where ModuleId=" + model.Id + "");

                    if (!string.IsNullOrEmpty(model.RoleStr))
                    {
                        string[] arr = BaseUtil.GetStrArray(model.RoleStr, split);// 以;分割
                        for (int i = 0; i < arr.GetLength(0); i++)
                        {
                            if (arr[i] != null && arr[i].ToString().Trim() != "")
                            {
                                RoleAuthorizeEntity obj = new RoleAuthorizeEntity { ButtonId = 0, ModuleId = model.Id, RoleId = int.Parse(arr[i].ToString().Trim()) };

                                Service.Insert(obj);
                            }
                        }
                    }
                }
            }

            if (err == "")
            {
                return Json(SuccessTip("操作成功"));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        [HttpGet]
        public JsonResult Delete(int id)
        {
            if (Think9.Services.Base.Configs.GetValue("IsDemo") == "true")
            {
                return Json(ErrorTip("演示模式下不能删除菜单！"));
            }
            else
            {
                string err = "";
                if (ComService.GetTotal("sys_module", "where ParentId=" + id + " ") > 0)
                {
                    err = "不能删除，还有下级菜单";
                }
                else
                {
                    err = ModuleService.DeleteById(id) ? "" : "操作失败";
                    if (err == "")
                    {
                        ComService.ExecuteSql("delete from sys_roleauthorize where ModuleId=" + id + "");
                    }
                }

                if (err == "")
                {
                    return Json(SuccessTip("删除成功"));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
        }

        [HttpGet]
        public JsonResult BatchDel(string idsStr)
        {
            string id = "";
            if (Think9.Services.Base.Configs.GetValue("IsDemo") == "true")
            {
                return Json(ErrorTip("演示模式下不能删除菜单！"));
            }
            else
            {
                var idsArray = idsStr.Substring(0, idsStr.Length - 1).Split(',');
                string[] arr = BaseUtil.GetStrArray(idsStr, ",");// 以;分割
                for (int i = 0; i < arr.GetLength(0); i++)
                {
                    if (arr[i] != null)
                    {
                        if (arr[i].ToString().Trim() != "")
                        {
                            id = arr[i].ToString().Trim();
                        }
                    }
                }
                return Json(SuccessTip("删除成功"));
            }
        }

        [HttpGet]
        public JsonResult ModuleButtonList(int roleId)
        {
            var list = ModuleService.GetModuleButtonList(roleId);
            var result = new { code = 0, count = list.Count(), data = list };
            return Json(result);
        }

        public ActionResult Icon()
        {
            return View();
        }

        private string CheckParent(string selectParentID, string id)
        {
            string strErr = "";

            if (selectParentID == id)
            {
                strErr = "不能选择自己作为上级！";
            }
            else
            {
                DataTable dtAll = ComService.GetDataTable("select id,FullName as name,ParentId from sys_module order by OrderNo");

                string strAllUpDep = ModuleService.GetUpIDStr(dtAll, selectParentID);

                if (strAllUpDep.Contains("." + id + "."))
                {
                    strErr = "上级菜单选择错误！";
                }
            }

            return strErr;
        }
    }
}