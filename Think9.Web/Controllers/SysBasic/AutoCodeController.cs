﻿using Microsoft.AspNetCore.Mvc;
using Think9.Services.Base;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysBasic")]
    public class AutoCodeController : BaseController
    {
        private RuleAutoService RuleAutoService = new RuleAutoService();
        private RuleListService RuleListService = new RuleListService();

        [HttpPost]
        public JsonResult GetCode(string name, string type)
        {
            string appId = Think9.Services.Base.Configs.GetValue("BaiduTransAppId");//百度翻译appId
            string password = Think9.Services.Base.Configs.GetValue("BaiduTransPassWord");//百度翻译appId

            string str = Think9.Services.Basic.CreatCode.Creat(appId, password, type, name);

            var result = SuccessTip("操作成功", str);

            return Json(result);
        }

        [HttpPost]
        public JsonResult NewGuid()
        {
            var result = SuccessTip("操作成功", Think9.Services.Basic.CreatCode.NewGuid());

            return Json(result);
        }
    }
}