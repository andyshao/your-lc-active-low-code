﻿using Microsoft.AspNetCore.Mvc;
using System;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Basic;

namespace Think9.Controllers.Basic
{
    [Area("SysBasic")]
    public class NotifyController : BaseController
    {
        private NotifyService NotifyService = new NotifyService();
        private ComService ComService = new ComService();

        // GET: Permissions/Role
        public override ActionResult Index(int? id)
        {
            base.Index(id);
            return View();
        }

        [HttpGet]
        public JsonResult GetList(NotifyEntity model, PageInfoEntity pageInfo, string key)
        {
            pageInfo.field = "publishTime desc";
            var result = NotifyService.GetPageByFilter(model, pageInfo, "where 1=1 ");
            return Json(result);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(NotifyEntity model)
        {
            try
            {
                model.FromId = CurrentUser == null ? "" : CurrentUser.Account;
                model.BeginDate = DateTime.Now;
                model.EndDate = null;
                model.Important = 0;
                model.publishTime = DateTime.Now;
                model.ToUser = "#all#";
                model.Type = 0;
                model.Readers = "";

                var result = NotifyService.Insert(model) ? SuccessTip("操作成功") : ErrorTip("操作失败");
                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            string where = "where NotifyId=@NotifyId";
            object param = new { NotifyId = id };

            var model = NotifyService.GetByWhereFirst(where, param);

            if (model != null)
            {
                return View(model);
            }
            else
            {
                return Json("数据不存在！");
            }
        }

        [HttpPost]
        public ActionResult Edit(NotifyEntity model)
        {
            string where = "where NotifyId=@NotifyId";
            string updateFields = "Content,Subject";

            var result = NotifyService.UpdateByWhere(where, updateFields, model) > 0 ? SuccessTip("操作成功") : ErrorTip("编辑失败");

            return Json(result);
        }

        [HttpGet]
        public JsonResult Delete(string id)
        {
            string where = "where NotifyId=" + id + " ";
            var result = NotifyService.DeleteByWhere(where) ? SuccessTip("删除成功") : ErrorTip("操作失败");
            return Json(result);
        }
    }
}