﻿using Microsoft.AspNetCore.Mvc;
using System;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysTable")]
    public class RuleTreeController : BaseController
    {
        private RuleTreeService RuleTreeService = new RuleTreeService();
        private RuleServiceBasic RuleService = new RuleServiceBasic();
        private RuleListService RuleListService = new RuleListService();

        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.SelectSourceList = RuleService.GetSelectSource();
            return View();
        }

        [HttpPost]
        public ActionResult Add(RuleTreeEntity model)
        {
            string err = "";

            try
            {
                err = RuleTreeService.Add(model);
            }
            catch (Exception ex)
            {
                err = ex.Message;
            }

            if (err == "")
            {
                return Json(SuccessTip("操作成功"));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        [HttpGet]
        public ActionResult Edit(string rid)
        {
            ViewBag.id = rid;
            string where = "where RuleId=@RuleId";
            object param = new { RuleId = rid };
            var model = RuleTreeService.GetByWhereFirst(where, param);

            var modelList = RuleListService.GetByWhereFirst(where, param);

            if (model != null && modelList != null)
            {
                ViewBag.SelectSourceList = RuleService.GetSelectValueByTbID(model.TbId);
                RuleTreeService.SetDefault(ref model);
                model.Name = modelList.RuleName;
                return View(model);
            }
            else
            {
                return Json("数据不存在！");
            }
        }

        [HttpPost]
        public ActionResult Edit(RuleTreeEntity model, string id)
        {
            string err = "";

            try
            {
                err = RuleTreeService.Edit(model);
            }
            catch (Exception ex)
            {
                err = ex.Message;
            }

            if (err == "")
            {
                return Json(SuccessTip("操作成功"));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        [HttpGet]
        public JsonResult GetSelectValueFieldByTb(string id)
        {
            string _id = id == null ? "" : id;
            return Json(RuleService.GetSelectValueFieldByTb(_id));
        }

        [HttpGet]
        public JsonResult GetSelectTxtFieldByTb(string id)
        {
            string _id = id == null ? "" : id;
            return Json(RuleService.GetSelectTxtFieldByTb(_id));
        }

        [HttpGet]
        public JsonResult GetSelectOrderFieldByTb(string id)
        {
            string _id = id == null ? "" : id;
            return Json(RuleService.GetSelectOrderFieldByTb(_id));
        }

        [HttpGet]
        public JsonResult GetConditionFieldList(string id)
        {
            string _id = id == null ? "" : id;
            string prefix = "";

            //id=#dict#+数据字典编码或#table#+录入表编码组成

            string str = _id.ToLower().Replace("#table#", "");

            return Json(RuleService.GetConditionFieldList02(_id, prefix));
        }
    }
}