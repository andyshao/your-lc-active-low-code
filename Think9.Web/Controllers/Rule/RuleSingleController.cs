﻿using Microsoft.AspNetCore.Mvc;
using System;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysTable")]
    public class RuleSingleController : BaseController
    {
        private RuleSingleService RuleSingleService = new RuleSingleService();
        private RuleServiceBasic RuleService = new RuleServiceBasic();
        private RuleListService RuleListService = new RuleListService();

        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.SelectSourceList = RuleService.GetSelectSource();

            return View();
        }

        [HttpPost]
        public ActionResult Add(RuleSingleEntity model)
        {
            string err = "";

            try
            {
                err = RuleSingleService.Add(model);
            }
            catch (Exception ex)
            {
                err = ex.Message;
            }

            if (err == "")
            {
                return Json(SuccessTip("新建成功"));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        [HttpGet]
        public ActionResult Edit(string rid)
        {
            ViewBag.id = rid;
            string where = "where RuleId=@RuleId";
            object param = new { RuleId = rid };

            var model = RuleSingleService.GetByWhereFirst(where, param);
            var modelList = RuleListService.GetByWhereFirst(where, param);

            if (model != null && modelList != null)
            {
                ViewBag.SelectSourceList = RuleService.GetSelectValueByTbID(model.TbId);
                RuleSingleService.SetDefault(ref model);
                model.Name = modelList.RuleName;
                return View(model);
            }
            else
            {
                return Json("数据不存在！");
            }
        }

        [HttpPost]
        public ActionResult Edit(RuleSingleEntity model, string id)
        {
            string err = "";

            try
            {
                err = RuleSingleService.Edit(model);
            }
            catch (Exception ex)
            {
                err = ex.Message;
            }

            if (err == "")
            {
                return Json(SuccessTip("编辑成功"));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        [HttpGet]
        public JsonResult GetSelectValueFieldByTb(string id)
        {
            string _id = id == null ? "" : id;
            return Json(RuleService.GetSelectValueFieldByTb(_id));
        }

        [HttpGet]
        public JsonResult GetSelectTxtFieldByTb(string id)
        {
            string _id = id == null ? "" : id;
            return Json(RuleService.GetSelectTxtFieldByTb(_id));
        }

        [HttpGet]
        public JsonResult GetSelectOrderFieldByTb(string id)
        {
            string _id = id == null ? "" : id;
            return Json(RuleService.GetSelectOrderFieldByTb(_id));
        }

        [HttpGet]
        public JsonResult GetConditionFieldList(string id)
        {
            string _id = id == null ? "" : id;
            string prefix = "";

            //id=#dict#+数据字典编码或#table#+录入表编码组成

            string str = _id.ToLower().Replace("#table#", "");

            return Json(RuleService.GetConditionFieldList02(_id, prefix));
        }
    }
}