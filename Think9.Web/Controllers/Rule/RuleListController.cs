﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Basic;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysTable")]
    public class RuleListController : BaseController
    {
        private TbIndexService TbIndexService = new TbIndexService();
        private RuleListService RuleListService = new RuleListService();
        //private ComService ComService = new ComService();

        public override ActionResult Index(int? id)
        {
            ViewBag.ClassID = "1";

            base.Index(id);
            return View();
        }

        [HttpGet]
        public JsonResult GetList(RuleListEntity model, PageInfoEntity pageInfo, string classid, string key)
        {
            pageInfo.field = "UpdateTime";
            pageInfo.order = "desc";

            string _key = key == null ? "" : key;
            string where = "where RuleType='" + classid + "'";
            if (_key != "")
            {
                where += " and RuleName like @RuleName ";
                model.RuleName = string.Format("%{0}%", _key);
            }

            long total = 0;
            IEnumerable<dynamic> list = RuleListService.GetPageListBySearch(ref total, model, pageInfo, where);
            var result = new { code = 0, msg = "", count = total, data = list };

            return Json(result);
        }

        [HttpGet]
        public ActionResult PUParameter()
        {
            return View();
        }

        [HttpGet]
        public ActionResult PUSearchIndex(string id)
        {
            string _key = id == null ? "" : id;
            ViewBag.tbid = _key.Replace("#table#", "");
            return View();
        }

        [HttpGet]
        public ActionResult GetIndexByTbID(string tbid)
        {
            var result = new { code = 0, msg = "", count = 999999, data = TbIndexService.GetIndexByTbID(tbid) };
            return Json(result);
        }

        [HttpGet]
        public JsonResult GetSysParameter()
        {
            var result = new { code = 0, msg = "", count = 999999, data = SysParameter.GetSysParameterList() };
            return Json(result);
        }

        [HttpGet]
        public JsonResult Delete(string id)
        {
            RuleListService.DeleteRule(id);
            return Json(SuccessTip("删除成功，已关联该数据规范的录入表指标需重新指定数据规范！"));
        }

        [HttpGet]
        public JsonResult BatchDel(string idsStr)
        {
            string id = "";
            var idsArray = idsStr.Substring(0, idsStr.Length - 1).Split(',');
            string[] arr = BaseUtil.GetStrArray(idsStr, ",");// 以;分割
            for (int i = 0; i < arr.GetLength(0); i++)
            {
                if (arr[i] != null)
                {
                    if (arr[i].ToString().Trim() != "")
                    {
                        id = arr[i].ToString().Trim();
                        RuleListService.DeleteRule(id);
                    }
                }
            }
            return Json(SuccessTip("删除成功，已关联该数据规范的录入表指标需重新指定数据规范！"));
        }

        [HttpPost]
        public ActionResult RuleListTreeSelect()
        {
            List<TreeGridEntity> list = RuleListService.GetRuleTreeSelect();

            var menuJson = Newtonsoft.Json.JsonConvert.SerializeObject(list);

            var result = SuccessTip("操作成功", menuJson);

            return Json(result);
        }

        [HttpPost]
        public ActionResult GetRuleName(string tbid, string indexid)
        {
            return Json(SuccessTip("", RuleListService.GetRuleName(tbid, indexid)));
        }

        [HttpPost]
        public JsonResult UpNameByID(string id, string name)
        {
            RuleListEntity model = new RuleListEntity();
            model.RuleName = name;
            model.RuleId = id;

            string where = "where RuleId='" + model.RuleId + "'";
            string updateFields = "RuleName";

            var result = RuleListService.UpdateByWhere(where, updateFields, model) > 0 ? SuccessTip("操作成功") : ErrorTip("编辑失败");
            return Json(result);
        }
    }
}