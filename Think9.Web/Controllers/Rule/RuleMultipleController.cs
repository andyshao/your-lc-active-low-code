﻿using Microsoft.AspNetCore.Mvc;
using System;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Table;

namespace Think9.Controllers.Basic
{
    [Area("SysTable")]
    public class RuleMultipleController : BaseController
    {
        private RuleMultipleService RuleMultipleService = new RuleMultipleService();
        private RuleServiceBasic RuleService = new RuleServiceBasic();
        private RuleListService RuleListService = new RuleListService();

        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.Guid = Think9.Services.Basic.CreatCode.NewGuid();
            ViewBag.SelectSourceList = RuleService.GetSelectSource();

            return View();
        }

        [HttpPost]
        public ActionResult Add(RuleMultipleEntity model, string id)
        {
            string err = "";

            try
            {
                err = RuleMultipleService.Add(model, id);
            }
            catch (Exception ex)
            {
                err = ex.Message;
            }

            if (err == "")
            {
                return Json(SuccessTip("新建成功"));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        [HttpGet]
        public ActionResult Edit(string rid)
        {
            string where = "where RuleId=@RuleId";
            object param = new { RuleId = rid };

            var model = RuleMultipleService.GetByWhereFirst(where, param);
            var modelList = RuleListService.GetByWhereFirst(where, param);

            if (model != null && modelList != null)
            {
                ViewBag.id = rid;
                ViewBag.SelectSourceList = RuleService.GetSelectValueByTbID(model.TbId);
                RuleMultipleService.SetDefault(ref model);
                model.Name = modelList.RuleName;
                return View(model);
            }
            else
            {
                return Json(ErrorTip("数据不存在！！！"));
            }
        }

        [HttpPost]
        public ActionResult Edit(RuleMultipleEntity model, string id)
        {
            string err = "";

            try
            {
                err = RuleMultipleService.Edit(model);
            }
            catch (Exception ex)
            {
                err = ex.Message;
            }

            if (err == "")
            {
                return Json(SuccessTip("编辑成功"));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        [HttpPost]
        public ActionResult AddListFiledTemp(string id, string value, string text)
        {
            string err = "";
            try
            {
                err = RuleMultipleService.AddListFiledTemp(id, value, text);
            }
            catch (Exception ex)
            {
                err = ex.Message;
            }

            if (err == "")
            {
                return Json(SuccessTip("操作成功"));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        [HttpPost]
        public ActionResult AddListFiled(string id, string value, string text, string tbid)
        {
            string err = "";
            try
            {
                err = RuleMultipleService.AddListFiled(id, value, text, tbid);
            }
            catch (Exception ex)
            {
                err = ex.Message;
            }

            if (err == "")
            {
                return Json(SuccessTip("操作成功"));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        [HttpGet]
        public ActionResult DeleteFiledTemp(string id)
        {
            string err = RuleMultipleService.DelListFiledTemp(id);

            if (err == "")
            {
                return Json(SuccessTip(""));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        [HttpGet]
        public ActionResult DeleteFiled(string rid, string indexid)
        {
            string err = RuleMultipleService.DelListFiled(rid, indexid);

            if (err == "")
            {
                return Json(SuccessTip(""));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        [HttpGet]
        public ActionResult GetListFiledTemp(string id)
        {
            var result = new { code = 0, msg = "", count = 999999, data = RuleMultipleService.GetListFiledTemp(id) };
            return Json(result);
        }

        [HttpGet]
        public ActionResult GetListFiled(string id)
        {
            var result = new { code = 0, msg = "", count = 999999, data = RuleMultipleService.GetListFiled(id) };
            return Json(result);
        }

        [HttpGet]
        public JsonResult GetSelectValueFieldByTb(string id)
        {
            string _id = id == null ? "" : id;
            return Json(RuleService.GetSelectValueFieldByTb(_id));
        }

        [HttpGet]
        public JsonResult GetSelectTxtFieldByTb(string id)
        {
            string _id = id == null ? "" : id;
            return Json(RuleService.GetSelectTxtFieldByTb(_id));
        }

        [HttpGet]
        public JsonResult GetSelectOrderFieldByTb(string id)
        {
            string _id = id == null ? "" : id;
            return Json(RuleService.GetSelectOrderFieldByTb(_id));
        }

        [HttpGet]
        public JsonResult GetConditionFieldList(string id)
        {
            string _id = id == null ? "" : id;
            string prefix = "";

            //id=#dict#+数据字典编码或#table#+录入表编码组成
            string str = _id.ToLower().Replace("#table#", "");

            return Json(RuleService.GetConditionFieldList02(_id, prefix));
        }
    }
}