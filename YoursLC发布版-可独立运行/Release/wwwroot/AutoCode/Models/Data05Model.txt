﻿/*******************************************************************************
 * Create:admin 2022-08-29 17:28:03
 * Description: YoursLC有源低代码 实体类
*********************************************************************************/

using System;
using DapperExtensions;
namespace Think9.Models 
{
	/// <summary>
	/// 实体类  生成时间：2022-08-29 17:28:03
	/// </summary>
	 [Table("tb_Data05")]
	public class Data05Model : MainTBEntity
	{
		#region Model
		/// <summary>
		/// 编码
		/// </summary>
		public string inCode { get; set; }
		/// <summary>
		/// 名称
		/// </summary>
		public string inName { get; set; }
		#endregion Model
	}
}
