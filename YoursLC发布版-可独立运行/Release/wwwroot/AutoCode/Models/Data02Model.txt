﻿/*******************************************************************************
 * Create:admin 2022-08-29 17:28:01
 * Description: YoursLC有源低代码 实体类
*********************************************************************************/

using System;
using DapperExtensions;
namespace Think9.Models 
{
	/// <summary>
	/// 实体类  生成时间：2022-08-29 17:28:01
	/// </summary>
	 [Table("tb_Data02")]
	public class Data02Model : MainTBEntity
	{
		#region Model
		/// <summary>
		/// 编码
		/// </summary>
		public string inCode { get; set; }
		/// <summary>
		/// 名称
		/// </summary>
		public string inName { get; set; }
		/// <summary>
		/// 备注
		/// </summary>
		public string inRemarks { get; set; }
		#endregion Model
	}
}
