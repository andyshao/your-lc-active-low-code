﻿/*******************************************************************************
 * Creator:admin 2022-08-29 12:33:15
 * Description: YoursLC有源低代码
*********************************************************************************/

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Think9.Controllers.Basic;
using Think9.Models;
using Think9.Services.Base;
using Think9.Services.Basic;
using Think9.Services.CodeBuild;
using Think9.Services.Flow;
using Think9.Services.Table;

namespace Think9.Controllers.CodeBuild
{
    public class Demo04Controller : BaseController
    {
        private Demo04Service Demo04Service = new Demo04Service();
        private ComService ComService = new ComService();

        private readonly string _maintbid = "tb_Demo04";//主表编码
        private readonly string _flowid = "fw_Demo04";//流程编码 bi_基础信息 fw_一般录入表
        private readonly string _tbname = "自由流程";//录入表名称

        private readonly string _split = BaseUtil.ComSplit;//字符分割 用于多选项的分割等
        private string str;

        #region list列表
        public override ActionResult Index(int? id)
        {
            string err = CheckCom.CheckedBeforeBegin(_flowid);//检查数据库是否建表等
            if (string.IsNullOrEmpty(err))
            {
                object param = BasicHelp.GetParamObject(CurrentUser);//系统参数可作为数据规范的条件参数
                ViewBag.SelectList = Demo04Service.GetSelectList("list", param);//为查询条件(下拉选择)准备动态数据
                ViewBag.SearchMode = BasicHelp.GetSearchMode(_flowid);//查看编辑模式-录入表管理/权限设置可设置

                base.Index(id);
                return View();
            }
            else
            {
                return Json(err);
            }
        }

        /// <summary>
        /// 操作前判断及处理 列表页面点击新增数据按钮触发
        /// </summary>
        /// <param name="type">类型add</param>
        /// <param name="listid">自增长id,默认0</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BeforeAdd(string type, string listid)
        {
            string err = "";
            int _listid = 0;//自动编号及子表数据初始化等情况需插入一条空数据并返回自增长id
            try
            {
                //得到步骤第一步，基本信息录入表返回空
                CurrentPrcsEntity mPrcs = FlowCom.GetFistFlowStept(_flowid);
                err = mPrcs == null ? "" : mPrcs.ERR;

                if (string.IsNullOrEmpty(err))
                {
                    //添加前检测，如权限校验、录入表是否被禁用等
                    err = CheckCom.CheckedBeforeAdd(_flowid, mPrcs, CurrentUser);
                    if (string.IsNullOrEmpty(err))
                    {
                        //添加前处理，如自动编号及子表数据初始化等
                        _listid = Demo04Service.BeforeAdd(ref err, _flowid, _maintbid, _tbname, mPrcs, CurrentUser);

                    }
                }

                if (string.IsNullOrEmpty(err))
                {
                    string pid = mPrcs == null ? "-1" : mPrcs.PrcsId;//当前流程步骤id
                    return Json(SuccessTip("", _listid.ToString(), pid));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 操作前判断及处理 列表页面点击编辑按钮触发
        /// </summary>
        /// <param name="type">类型edit</param>
        /// <param name="listid">自增长id：基础信息表对应本表字段listid，一般录入表对应表flowrunlist中的listid</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BeforeEdit(string type, string listid)
        {
            string err = "";
            //得到当前流程步骤，基本信息录入表返回空
            CurrentPrcsEntity mPrcs = FlowCom.GetCurrentFlowStept(_flowid, listid);
            err = mPrcs == null ? "" : mPrcs.ERR;

            if (string.IsNullOrEmpty(err))
            {
                //编辑前检测-如权限校验、录入表是否被禁用、是否锁定等
                err = CheckCom.CheckedBeforeEdit(_flowid, listid, mPrcs, CurrentUser);
                if (string.IsNullOrEmpty(err))
                {
                    //编辑前数据处理，可手动精简
                    err = Demo04Service.BeforeEdit(_flowid, listid, mPrcs, CurrentUser);
                }
            }

            if (string.IsNullOrEmpty(err))
            {
                string pid = mPrcs == null ? "-1" : mPrcs.PrcsId;//当前流程步骤id
                return Json(SuccessTip("", listid, pid));
            }
            else
            {
                return Json(ErrorTip(err));
            }
        }

        /// <summary>
        /// 数据查看 列表页面点击查看按钮触发
        /// wwwroot/Reports文件夹中保存样式模板文件 下载安装MicrosoftRDLC报表设计器可编辑设计其样式
        /// </summary>
        /// <param name="listid">基础信息表对应本表字段listid，一般录入表对应表flowrunlist中的listid</param>
        /// <returns></returns>
        [HttpGet]
        public  ActionResult Detail(string listid)
        {
            TbBasicEntity model = PageCom.GetAttBut(listid, _flowid);//附件按钮设置，可在录入表管理/页面按钮中设置

            ViewBag.ListId = listid;
            ViewBag.ButPdf = model.ButPDFDetails;//Pdf是否显示 1显示
            ViewBag.ButDoc = model.ButDOCDetails;//DOC是否显示 1显示
            ViewBag.ButExcel = model.ButExcelDetails;//Excel是否显示 1显示
            ViewBag.ButAtt = model.ButAtt;//附件按钮是否显示 1显示
            ViewBag.ButAttTxt = model.ButAttTxt;//附件按钮标题
            ViewBag.UserId = CurrentUser == null ? "un_defined" : CurrentUser.Account;

            return View();
        }

        /// <summary>
        /// 数据删除 列表页面点击删除按钮触发
        /// </summary>
        /// <param name="listid">基础信息表对应本表字段listid，一般录入表对应表flowrunlist中的listid</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult Delete(string listid)
        {
            string err = "";

            //得到当前流程步骤 基本信息录入表返回空
            CurrentPrcsEntity mPrcs = FlowCom.GetCurrentFlowStept(_flowid, listid);
            err = mPrcs == null ? "" : mPrcs.ERR;

            if (string.IsNullOrEmpty(err))
            {
                //删除前检测 包括权限校验、录入表是否被禁用、数据是否锁定等
                err = CheckCom.CheckedBeforeDel(_flowid, listid, mPrcs, CurrentUser);
            }

            try
            {
                if (string.IsNullOrEmpty(err))
                {
                    err = Demo04Service.DeleteByID(listid, _flowid) ? "" : "操作失败";
                    //删除后数据处理,默认什么也不做，可自定义可手动删除
                    if (string.IsNullOrEmpty(err))
                    {
                        err = Demo04Service.AfterDelete(_flowid, listid, mPrcs, CurrentUser);
                    }
                }

                if (string.IsNullOrEmpty(err))
                {
                    Record.Add(CurrentUser == null ? "un_defined" : CurrentUser.Account, listid, _flowid, "数据删除");
                    return Json(SuccessTip("删除成功"));
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 数据查询 列表页面点击查询按钮触发
        /// </summary>
        /// <param name="model">主表数据model</param>
        /// <param name="pageInfo">页面信息，包括行数、排序等</param>
        /// <param name="isAll">为all则显示所有</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetPageListBySearch(Demo04Model model, PageInfoEntity pageInfo, string isAll)
        {
            pageInfo.field = "listid";//排序field
            pageInfo.order = "desc";

            long total = 0;
            IEnumerable<dynamic> list = Demo04Service.GetSearchList(ref total, model, pageInfo, CurrentUser, _flowid, _maintbid, isAll);

            if (list == null)
            {
                return Json(ErrorTip("参数错误"));
            }
            else
            {
                var result = new { code = 0, msg = "", count = total, data = list };
                return Json(result);
            }
        }
        #endregion list列表

        #region Form编辑
        /// <summary>
        /// 录入页面显示
        /// </summary>
        /// <param name="type">add或edit</param>
        /// <param name="listid">基础信息表对应本表字段listid，一般录入表对应表flowrunlist中的listid</param>
        /// <param name="pid">当前流程步骤id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Form(string type, string listid, string pid)
        {
            int _listid = listid == null ? 0 : int.Parse(listid);
            object param = BasicHelp.GetParamObject(CurrentUser);//数据规范中的筛选条件可使用系统参数
            ViewBag.UserId = CurrentUser == null ? "un_defined" : CurrentUser.Account;//用户id
            ViewBag.Type = type;//add或edit
            ViewBag.ListId = _listid;//基础信息表listid自增长，一般录入表listid来源于flowrunlist
            ViewBag.Split = _split;//字符分割 checkbox多选时使用
            ViewBag.FId = _flowid;//流程编码
            ViewBag.PrcId = pid;//流程步骤id
            ViewBag.PrcNo = FlowCom.GetFlowNoByID(_flowid, pid);//流程步骤编码
            ViewBag.SelectList = Demo04Service.GetSelectList(type, param);//为下拉选择、多选、单选动态数据源赋值

            //包括为指标赋初始值--系统指标或默认值
            Demo04Model model = Demo04Service.GetModel(type, _listid, CurrentUser);

            if (model != null)
            {
                return View(model);
            }
            else
            {
                return Json("数据不存在");
            }
        }

        /// <summary>
        /// 下拉选择或者弹出选择后调用，完成数据读取功能--可在录入表管理/数据读取中自定义
        /// </summary>
        /// <param name="controlslist">页面控件id与text</param>
        /// <param name="gridlist">子表数据列表 无子表时为空</param>
        /// <param name="id"></param>
        /// <param name="tbid">_main或子表编码</param>
        /// <param name="indexid">下拉或者弹出选择(触发控件)对应指标编码</param>
        /// <param name="value">下拉或者弹出选择(触发控件)对应控件Value</param>
        /// <returns>返回list交由前台解析</returns>
        [HttpPost]
        public ActionResult AfterControlSelect(IEnumerable<ControlEntity> controlslist, IEnumerable<GridListEntity> gridlist, string id, string tbid, string indexid, string value)
        {
            try
            {
                //未设置数据读取则什么也不做
                List<ControlEntity> list = new List<ControlEntity>();

                return Json(SuccessTip("", list, ""));
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 数据保存 Form页面点击保存按钮触发
        /// </summary>
        /// <param name="model">主表数据model</param>
        /// <param name="gridlist">子表数据 无子表时为空</param>
        /// <param name="listid">listid=0表示增加</param>
        /// <param name="prcno">流程步骤编码</param>
        /// <param name="type">add或edit</param>
        /// <param name="att">附件id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveData(Demo04Model model, IEnumerable<GridListEntity> gridlist, int listid, string prcno, string type, string att)
        {
            string err = "";
			model.inCS0001 = model.inCS0001_Exa;//处理复选框
			model.inPicture3 = model.inPicture3_Exa;//处理图片
			model.inPicture = model.inPicture_Exa;//处理图片
            try
            {
                //编辑
                if (listid != 0)
                {
                    err = this.Edit(model, gridlist, listid, prcno);
                }
                else
                {
                    listid = this.Add(ref err, prcno, model);

                }
				if (string.IsNullOrEmpty(err) && type == "add")
				{
					AttachmentService.UpdateAttachmentId(listid, _flowid, att);//处理listid=0时添加了附件
				}
                if (string.IsNullOrEmpty(err))
                {
                    err = Demo04Service.AfterEdit(listid.ToString(), _flowid, FlowCom.GetFlowIDByNo(_flowid, prcno), CurrentUser);//编辑后数据处理，可自定义可手动删除

                    if (string.IsNullOrEmpty(err))
                    {
                        return Json(SuccessTip("操作成功"));
                    }
                    else
                    {
                        return Json(ErrorTip("AfterEdit函数出现错误：" + err));
                    }
                }
                else
                {
                    return Json(ErrorTip("操作失败！" + err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 转交下一步--保存数据并流程转交  Form页面点击转交按钮触发
        /// </summary>
        /// <param name="model">主表数据model</param>
        /// <param name="gridlist">子表数据列表 无子表时为空</param>
        /// <param name="listid">listid=0表示增加</param>
        /// <param name="prcno">流程步骤编码</param>
        /// <param name="type">add或edit</param>
        /// <param name="att">附件id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult NextStep(Demo04Model model, IEnumerable<GridListEntity> gridlist, int listid, string prcno, string type, string att)
        {
            string err = "";
			model.inCS0001 = model.inCS0001_Exa;//处理复选框
			model.inPicture3 = model.inPicture3_Exa;//处理图片
			model.inPicture = model.inPicture_Exa;//处理图片
            try
            {
                //编辑
                if (listid != 0)
                {
                    err = this.Edit(model, gridlist, listid, prcno);
                }
                else
                {
                    listid = this.Add(ref err, prcno, model);

                }
				if (string.IsNullOrEmpty(err) && type == "add")
				{
					AttachmentService.UpdateAttachmentId(listid, _flowid, att);//处理listid=0时添加了附件
				}
                if (string.IsNullOrEmpty(err))
                {
                    err = Demo04Service.AfterEdit(listid.ToString(), _flowid, FlowCom.GetFlowIDByNo(_flowid, prcno), CurrentUser);//编辑后数据处理，可自定义可手动删除

                    if (string.IsNullOrEmpty(err))
                    {
                        if (_flowid.StartsWith("bi_"))
                        {
                          return Json(ErrorTip("数据保存成功，基本信息表不能被转交"));
                        }
                        else
                        {
                          return Json(SuccessTip("", listid.ToString()));
                        }
                    }
                    else
                    {
                        return Json(ErrorTip("AfterEdit函数出现错误：" + err));
                    }
                }
                else
                {
                    return Json(ErrorTip(err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 结束--保存数据并结束流程 Form页面点击结束按钮触发
        /// </summary>
        /// <param name="model">主表数据model</param>
        /// <param name="gridlist">子表数据 无子表时为空</param>
        /// <param name="listid">listid=0表示增加</param>
        /// <param name="prcno">流程步骤编码</param>
        /// <param name="type">add或edit</param>
        /// <param name="att">附件id</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Finish(Demo04Model model, IEnumerable<GridListEntity> gridlist, int listid, string prcno, string type, string att)
        {
            string err = "";
			model.inCS0001 = model.inCS0001_Exa;//处理复选框
			model.inPicture3 = model.inPicture3_Exa;//处理图片
			model.inPicture = model.inPicture_Exa;//处理图片
            try
            {
                //编辑
                if (listid != 0)
                {
                    err = this.Edit(model, gridlist, listid, prcno);
                }
                else
                {
                    listid = this.Add(ref err, prcno, model);

                }
				if (string.IsNullOrEmpty(err) && type == "add")
				{
					AttachmentService.UpdateAttachmentId(listid, _flowid, att);//处理listid=0时添加了附件
				}
                if (string.IsNullOrEmpty(err))
                {
                    err = Demo04Service.AfterFinish(listid.ToString(), _flowid, FlowCom.GetFlowIDByNo(_flowid, prcno), CurrentUser);//完成后数据处理，可手动删除

                    if (string.IsNullOrEmpty(err))
                    {
                        if (_flowid.StartsWith("bi_"))
                        {
                            return Json(ErrorTip("操作成功，数据已被锁定"));
                        }
                        else
                        {
                            return Json(SuccessTip("操作成功"));
                        }
                    }
                    else
                    {
                        return Json(ErrorTip("AfterFinish函数出现错误：" + err));
                    }
                }
                else
                {
                    return Json(ErrorTip("操作失败！" + err));
                }
            }
            catch (Exception ex)
            {
                return Json(ErrorTip(ex.Message));
            }
        }

        /// <summary>
        /// 添加主表数据
        /// </summary>
        /// <param name="err">错误信息</param>
        /// <param name="prcno">当前流程步骤编码</param>
        /// <param name="model">主表数据</param>
        /// <returns></returns>
        private int Add(ref string err, string prcno, Demo04Model model)
        {
            int listid = 0;

            DataTable dtMain = DataTableHelp.ModelToDataTable<Demo04Model>(model);//model转换DataTable
            //进行主键和唯一键检测，主键和唯一键可在录入表指标属性中设置
            err = CheckCom.CheckMainTbValueBKAndUnique(listid, _maintbid, model, dtMain);
            if (string.IsNullOrEmpty(err))
            {
                //进行主表自定义校验，自定义校验可在录入表管理/录入校验中设置
                err = CheckCom.CheckMainTbValidate(dtMain, _flowid, prcno);
                if (string.IsNullOrEmpty(err))
                {
					CurrentPrcsEntity mPrcs = FlowCom.GetFistFlowStept(_flowid);
					if (string.IsNullOrEmpty(mPrcs.ERR))
					{
						//首先FlowrunList表中添加数据返回自增长的listid FlowrunList用于记录流程信息
						listid = FlowRunListService.InsertFlowrunListReturnID(ref err, _maintbid, _flowid, _tbname, mPrcs, CurrentUser);
						if (string.IsNullOrEmpty(err))
						{
							model.ListId = listid;
							err = Demo04Service.Insert(model) ? "" : "添加失败";
							if (string.IsNullOrEmpty(err))
							{
								Record.Add(CurrentUser == null ? "un_defined" : CurrentUser.Account, listid.ToString(), _flowid, "数据添加");
							}
						}
					}
                }
            }

            return listid;
        }

        /// <summary>
        /// 编辑主表数据
        /// </summary>
        /// <param name="model">主表数据</param>
        /// <param name="gridlist">子表数据列表 无子表时为空</param>
        /// <param name="listid">主表自增长id：基础信息表对应主表listid，一般录入表对应表flowrunlist中的listid</param>
        /// <param name="prcno">当前流程步骤编码</param>
        /// <returns></returns>
        private string Edit(Demo04Model model, IEnumerable<GridListEntity> gridlist, int listid, string prcno)
        {
            string err = "";

            //有子表时会遍历数据并判断取数
			List<Subtable01Model> listSubtable01 = Subtable01Service.GetGridFormList(gridlist, "", listid, ref err);

            if (string.IsNullOrEmpty(err))
            {
                model.ListId = listid;

                DataTable dtMain = DataTableHelp.ModelToDataTable<Demo04Model>(model);
                //主键和唯一检测，主键和唯一可在录入表指标属性中设置
                err = CheckCom.CheckMainTbValueBKAndUnique(listid, _maintbid, model, dtMain);
                if (string.IsNullOrEmpty(err))
                {
                    //自定义校验，自定义校验可在录入表管理录入校验中设置
                    err = CheckCom.CheckTbValidate(dtMain, gridlist, _flowid, prcno);
                    if (string.IsNullOrEmpty(err))
                    {
                        //子表数据编辑
						Subtable01Service Subtable01 = new Subtable01Service();
						foreach (Subtable01Model obj in listSubtable01)
						{
							Subtable01.UpdateById(obj);
						}

                        string updateFields = Demo04Service.GetUpdateFields(prcno);//可编辑列
                        if(!string.IsNullOrEmpty(updateFields))
                        {

                            //编辑主表
                            err = Demo04Service.UpdateByWhere("where listid=" + listid + "", updateFields, model) > 0 ? "" : "编辑失败";
                        }

                        Record.Add(CurrentUser == null ? "un_defined" : CurrentUser.Account, listid.ToString(), _flowid, "数据编辑");
                    }
                }
            }

            return err;
        }


        #endregion Form编辑

        #region 子表处理
        /// <summary>
        /// 从子表获取数据
        /// </summary>
        /// <param name="tbid">子表编码</param>
        /// <param name="listid">主表数据id</param>
        /// <param name="from">add或edit，用于控制指标编辑时锁定</param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetDataFromGrid(string tbid, int listid, string from)
        {
		    List<dynamic> list = null;
			if (tbid == "tb_Subtable01")
			{
				Subtable01Service Subtable01 = new Subtable01Service();
				list = Subtable01.GetGridDataByListId(CurrentUser, listid, from);
			}

            var result = new { code = 0, msg = "", count = 999999, data = list };
            return Json(result);
        }

        /// <summary>
        /// 子表添加一条数据，子表点击添加按钮触发
        /// </summary>
        /// <param name="model">主表model</param>
        /// <param name="list">子表数据列表</param>
        /// <param name="tbid">子表编码</param>
        /// <param name="listid">主表自增长id：基础信息表对应主表listid，一般录入表对应表flowrunlist中的listid</param>
        [HttpPost]
        public ActionResult AddGrid(Demo04Model model, IEnumerable<GridListEntity> list, string tbid, int listid)
        {
            string err = "";
            int newid = 0;
            try
            {
                newid = this.AddItem(ref err, model, CurrentUser, list, tbid, listid);

            }
            catch (Exception ex)
            {
                err += ex.Message;
            }

            if (newid > 0 && string.IsNullOrEmpty(err))
            {
                Record.Add(CurrentUser == null ? "un_defined" : CurrentUser.Account, newid.ToString(), _flowid, "添加子表(" + tbid + ")数据");
                return Json(SuccessTip("操作成功", newid.ToString()));
            }
            else
            {
                return Json(ErrorTip("添加失败：" + err));
            }
        }

        /// <summary>
        ///子表删除一条数据，子表点击删除按钮触发
        /// </summary>
        /// <param name="flag">#子表编码#id#行号#</param>
        /// <param name="listid">主表自增长id：基础信息表对应主表listid，一般录入表对应表flowrunlist中的listid</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DelGrid(string flag, string listid)
        {
            string tbid = "";
            string id = "";
            BasicHelp.GetTbAndIdByFlag(flag, ref tbid, ref id);//flag：#子表编码#id#行号#

            if (!string.IsNullOrEmpty(tbid) && !string.IsNullOrEmpty(id))
            {
                if(ComService.ExecuteSql("delete from " + tbid + " where Id=" + id + "") > 0)
                {
                    Record.Add(CurrentUser == null ? "un_defined" : CurrentUser.Account, listid, _flowid, "删除子表("+ tbid + ")数据");
                    return Json(SuccessTip("删除成功"));
                }
                else
                {
                    return Json(ErrorTip("操作失败"));
                }
            }
            else
            {
                return Json(ErrorTip("参数错误"));
            }
        }

        /// <summary>
        /// 子表添加一条数据
        /// </summary>
        /// <param name="err">错误信息</param>
        /// <param name="model">主表model</param>
        /// <param name="gridlist">子表数据列表</param>
        /// <param name="tbid">子表编码</param>
        /// <param name="listid">主表自增长id：基础信息表对应主表listid，一般录入表对应表flowrunlist中的listid</param>
        /// <returns></returns>
        private int AddItem(ref string err, Demo04Model model, CurrentUserEntity user, IEnumerable<GridListEntity> gridlist, string tbid, int listid)
        {
            int newid = 0;

            //添加一条子表数据，可包含多个子表，以子表id判断属哪个子表添加数据
			if (tbid == "tb_Subtable01")
			{
				List<Subtable01Model> mlist = Subtable01Service.GetGridFormList(gridlist, "#tb_Subtable01#0#0#", listid, ref err);
				if (err == "")
				{
					if (listid == 0)
					{
						 if (_flowid.StartsWith("bi_"))
						{
							newid = PageCom.InsertEmptyReturnID(ref err, _maintbid, _flowid, _tbname, user, null);
						}
						else
						{
							CurrentPrcsEntity mPrcs = FlowCom.GetFistFlowStept(_flowid);
							newid = PageCom.InsertEmptyReturnID(ref err, _maintbid, _flowid, _tbname, user, mPrcs);
						}
					}
					else
					{
						newid = listid;
					}
					if (newid > 0)
					{
						foreach (Subtable01Model sel in mlist)
						{
							sel.ListId = newid;
							Subtable01Service server = new Subtable01Service();
							
							//主键和唯一检测
							err = CheckCom.CheckGridTbValueBKAndUnique(sel.ListId, sel.TbId, sel, DataTableHelp.ModelToDataTable<Subtable01Model>(sel));
							if(string.IsNullOrEmpty(err))
							{
								server.Insert(sel);
							}
						}
					}
					else
					{
						 err = "主表插入数据错误";
					}
				}
			}

            return newid;
        }
        #endregion 子表处理

        #region 弹出处理
        /// <summary>
        /// 弹出数据选择页面
        /// </summary>
		/// <param name="tbid">_main或子表编码</param>
		/// <param name="indexid">触发弹出页面的指标编码</param>
        /// <param name="id">子表数据列表id或空</param>
        /// <param name="strv">子表哪一列触发如v1、v2</param>
        /// <param name="from">list或edit，list时会解除条件参数中包含的系统指标，如用户登录名等</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult PopUpSelectValue(string tbid, string indexid, string id, string strv, string from)
        {
           object param = BasicHelp.GetParamObject(CurrentUser);

            //处理弹出页面 若无弹出页面可删除
            ViewBag.PuTbId = tbid == null ? "" : tbid;
            ViewBag.PuIndexId = indexid == null ? "" : indexid;
            ViewBag.PuId = id == null ? "" : id;
            ViewBag.PuV = strv == null ? "" : strv.Replace("v","");
            ViewBag.PuFrom = from == null ? "" : from;
            //弹出页面的查询条件有下拉选择、多选、单选时，为其绑定动态数据源
            ViewBag.SelectList = Demo04Service.GetSelectList(tbid, indexid, from, param);
            return View("Grid_" + indexid);
        }

        /// <summary>
        /// 弹出页面获得数据列表
        /// </summary>
        /// <param name="pageInfo">页面信息，包括行数、排序等</param>
        /// <param name="indexid">主表触发时为指标编码，子表触发时为子表编码+指标编码</param>
        /// <param name="list">封装查询条件</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetPopUpTablePageList(PageInfoEntity pageInfo, string indexid, string from, IEnumerable<valueTextEntity> list)
        {
            long total = 0;
            IEnumerable<dynamic> _list = Demo04Service.GetPopUpTableList(ref total, pageInfo, indexid, from, list);
            
            if (_list == null)
            {
                return Json(ErrorTip("参数错误"));
            }
            else
            {
                var result = new { code = 0, msg = "", count = total, data = _list };
                return Json(result);
            }
        }

        /// <summary>
        /// 弹出数据选择页面点击选择确定后调用 默认什么也不做--可自定义
        /// </summary>
        /// <param name="tbid">_main或子表编码</param>
        /// <param name="indexid">触发弹出页面的指标编码</param>
        /// <param name="id">子表数据列表id或空</param>
        /// <param name="value">选择的值</param>
        /// <param name="v">子表弹出时使用 第几列</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult PopUpAfterSelect(string tbid, string indexid, string id, string value, string v)
        {
            //默认什么也不做--可自定义
            if (tbid == "_main")//主表
            {
                 //自定义 如
                //if (indexid == "xxx")
                //{
                //}
            }
            else//子表
            {
                //自定义 如
                //if (indexid == "xxx")
                //{
                //}
            }
            return Json("");
        }
        #endregion 弹出处理
    }
}