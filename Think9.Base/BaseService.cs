﻿using System.Collections.Generic;
using Think9.Models;
using Think9.Repository;

namespace Think9.Services.Base
{
    public class BaseService<T> where T : class, new()
    {
        public BaseRepository<T> BaseRepository = new BaseRepository<T>();

        /// <summary>
        /// 获取分页数据 联合查询--新加的
        /// </summary>
        public IEnumerable<dynamic> GetPageUnite(ref long total, T filter, PageInfoEntity pageInfo, string where = null)
        {
            string _orderBy = string.Empty;
            if (!string.IsNullOrEmpty(pageInfo.field))
            {
                _orderBy = string.Format(" ORDER BY {0} {1}", pageInfo.field, pageInfo.order);
            }
            else
            {
                //_orderBy = " ORDER BY createTime desc";
            }

            var list = BaseRepository.GetByPageUnite(new SearchFilterEntity { pageIndex = pageInfo.page, pageSize = pageInfo.limit, returnFields = pageInfo.returnFields, param = filter, where = where, orderBy = _orderBy }, out total);

            return list;
        }

        /// <summary>
        /// 获取分页数据--新加的
        /// </summary>
        public IEnumerable<dynamic> GetPageList(ref long total, PageInfoEntity pageInfo, string some, string tableName, string where, string orderby, object param)
        {
            var list = BaseRepository.GetPageList(out total, some, tableName, where, orderby, pageInfo.page, pageInfo.limit, param);

            return list;
        }

        /// <summary>
        /// 获取分页数据--新加的
        /// </summary>
        public IEnumerable<dynamic> GetPageByFilter(ref long total, T filter, PageInfoEntity pageInfo, string where = null)
        {
            string _orderBy = string.Empty;
            if (!string.IsNullOrEmpty(pageInfo.field))
            {
                _orderBy = string.Format(" ORDER BY {0} {1}", pageInfo.field, pageInfo.order);
            }
            else
            {
                //_orderBy = " ORDER BY createTime desc";
            }

            var list = BaseRepository.GetByPage(new SearchFilterEntity { pageIndex = pageInfo.page, pageSize = pageInfo.limit, returnFields = pageInfo.returnFields, param = filter, where = where, orderBy = _orderBy }, out total);

            return list;
        }

        /// <summary>
        /// 获取分页数据--新加的
        /// </summary>
        public dynamic GetPageByFilter2(ref long total, T filter, PageInfoEntity pageInfo, string where = null)
        {
            string _orderBy = string.Empty;
            if (!string.IsNullOrEmpty(pageInfo.field))
            {
                _orderBy = string.Format(" ORDER BY {0} {1}", pageInfo.field, pageInfo.order);
            }
            else
            {
                //_orderBy = " ORDER BY createTime desc";
            }

            var list = BaseRepository.GetByPage(new SearchFilterEntity { pageIndex = pageInfo.page, pageSize = pageInfo.limit, returnFields = pageInfo.returnFields, param = filter, where = where, orderBy = _orderBy }, out total);

            return list;
        }

        /// <summary>
        /// 获取分页数据
        /// </summary>
        public dynamic GetPageByFilter(T filter, PageInfoEntity pageInfo, string where = null)
        {
            string _orderBy = string.Empty;
            if (!string.IsNullOrEmpty(pageInfo.field))
            {
                _orderBy = string.Format(" ORDER BY {0} {1}", pageInfo.field, pageInfo.order);
            }
            else
            {
                //_orderBy = " ORDER BY createTime desc";
            }

            long total = 0;

            var list = BaseRepository.GetByPage(new SearchFilterEntity { pageIndex = pageInfo.page, pageSize = pageInfo.limit, returnFields = pageInfo.returnFields, param = filter, where = where, orderBy = _orderBy }, out total);

            return PagerEntity.Paging(list, total);
        }

        /// <summary>
        /// 获取分页数据 联合查询
        /// </summary>
        public dynamic GetPageUnite(T filter, PageInfoEntity pageInfo, string where = null)
        {
            string _orderBy = string.Empty;
            if (!string.IsNullOrEmpty(pageInfo.field))
            {
                _orderBy = string.Format(" ORDER BY {0} {1}", pageInfo.field, pageInfo.order);
            }
            else
            {
                //_orderBy = " ORDER BY createTime desc";
            }

            string _where = where;

            long total = 0;
            var list = BaseRepository.GetByPageUnite(new SearchFilterEntity { pageIndex = pageInfo.page, pageSize = pageInfo.limit, returnFields = pageInfo.returnFields, param = filter, where = _where, orderBy = _orderBy }, out total);

            return PagerEntity.Paging(list, total);
        }

        /// <summary>
        /// 根据条件修改数据
        /// </summary>
        public int UpdateByWhere(string where, string updateFields, object entity)
        {
            return BaseRepository.UpdateByWhere(where, updateFields, entity);
        }

        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        public T GetByWhereFirst(string where = null, object param = null)
        {
            return BaseRepository.GetByWhereFirst(where, param);
        }

        /// <summary>
        /// 根据主键返回实体
        /// </summary>
        public IEnumerable<T> GetByIdsBase(object ids)
        {
            return BaseRepository.GetByIdsBase(ids);
        }

        /// <summary>
        /// 获取总数
        /// </summary>
        public long GetTotal(string where = null, object param = null)
        {
            return BaseRepository.GetTotal(where, param);
        }

        #region CRUD

        /// <summary>
        /// 根据主键返回实体
        /// </summary>
        public T GetById(int Id)
        {
            return BaseRepository.GetById(Id);
        }

        /// <summary>
        /// 新增
        /// </summary>
        public bool Insert(T model)
        {
            return BaseRepository.Insert(model) > 0 ? true : false;
        }

        /// <summary>
        /// 新增
        /// </summary>
        public int InsertReturnID(T model)
        {
            return BaseRepository.Insert(model);
        }

        /// <summary>
        /// 根据主键修改数据
        /// </summary>
        public bool UpdateById(T model)
        {
            return BaseRepository.UpdateById(model) > 0 ? true : false;
        }

        /// <summary>
        /// 根据主键修改数据 修改指定字段
        /// </summary>
        public bool UpdateById(T model, string updateFields)
        {
            return BaseRepository.UpdateById(model, updateFields) > 0 ? true : false;
        }

        /// <summary>
        /// 根据主键删除数据
        /// </summary>
        public bool DeleteById(int Id)
        {
            return BaseRepository.DeleteById(Id) > 0 ? true : false;
        }

        /// <summary>
        /// 根据主键批量删除数据
        /// </summary>
        public bool DeleteByIds(object Ids)
        {
            return BaseRepository.DeleteByIds(Ids) > 0 ? true : false;
        }

        /// <summary>
        /// 根据条件删除
        /// </summary>
        public bool DeleteByWhere(string where)
        {
            return BaseRepository.DeleteByWhere(where) > 0 ? true : false;
        }

        #endregion CRUD

        /// <summary>
        /// 返回整张表数据
        /// returnFields需要返回的列，用逗号隔开。默认null，返回所有列
        /// </summary>
        public IEnumerable<T> GetAll(string returnFields = null, string orderby = null)
        {
            return BaseRepository.GetAll(returnFields, orderby);
        }

        /// <summary>
        /// 创建时间范围条件
        /// </summary>
        protected string CreateTimeWhereStr(string StartEndDate, string _where, string prefix = null)
        {
            if (!string.IsNullOrEmpty(StartEndDate) && StartEndDate != " ~ ")
            {
                if (StartEndDate.Contains("~"))
                {
                    if (StartEndDate.Contains("+"))
                    {
                        StartEndDate = StartEndDate.Replace("+", "");
                    }
                    var dts = StartEndDate.Split('~');
                    var start = dts[0].Trim();
                    var end = dts[1].Trim();
                    if (!string.IsNullOrEmpty(start))
                    {
                        if (!string.IsNullOrEmpty(prefix))
                        {
                            _where += string.Format(" and {1}createTime>='{0} 00:00'", start, prefix);
                        }
                        else
                        {
                            _where += string.Format(" and createTime>='{0} 00:00'", start);
                        }
                    }
                    if (!string.IsNullOrEmpty(end))
                    {
                        if (!string.IsNullOrEmpty(prefix))
                        {
                            _where += string.Format(" and {1}createTime<='{0} 59:59'", end, prefix);
                        }
                        else
                        {
                            _where += string.Format(" and createTime<='{0} 59:59'", end);
                        }
                    }
                }
            }
            return _where;
        }

        /// <summary>
        /// 根据查询条件获取数据
        /// </summary>
        public IEnumerable<T> GetByWhere(string where = null, object param = null, string returnFields = null, string orderby = null)
        {
            return BaseRepository.GetByWhere(where, param, returnFields, orderby);
        }
    }
}