﻿using System;
using System.Collections.Generic;

namespace Think9.Services.Base
{
    public class Record
    {
        //public static void Add(string listid, string flowid, string info, CurrentUserEntity user)
        //{
        //    ComService ComService = new ComService();

        //    List<string> columns = new List<string>();
        //    columns.Add("OperateTime");
        //    columns.Add("OperatePerson");
        //    columns.Add("ListId");
        //    columns.Add("FlowId");
        //    columns.Add("TbId");
        //    columns.Add("Info");

        //    object param = new { OperateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), OperatePerson = user == null ? "un_defined" : user.Account, ListId = listid, FlowId = flowid, TbId = flowid.Replace("bi_", "tb_").Replace("fw_", "tb_"), Info = info };

        //    ComService.InsertAndReturnID("recordrun", columns, param);
        //}

        public static void Add(string userid, string listid, string flowid, string info)
        {
            ComService ComService = new ComService();

            List<string> columns = new List<string>();
            columns.Add("OperateTime");
            columns.Add("OperatePerson");
            columns.Add("ListId");
            columns.Add("FlowId");
            columns.Add("TbId");
            columns.Add("Info");

            object param = new { OperateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), OperatePerson = userid, ListId = listid, FlowId = flowid, TbId = flowid.Replace("bi_", "tb_").Replace("fw_", "tb_"), Info = info };

            ComService.InsertAndReturnID("recordrun", columns, param);
        }

        public static void AddAttInfo(string userid, string listid, string flowid, string info, string flag)
        {
            ComService ComService = new ComService();

            List<string> columns = new List<string>();
            columns.Add("OperateTime");
            columns.Add("OperatePerson");
            columns.Add("ListId");
            columns.Add("FlowId");
            columns.Add("TbId");
            columns.Add("Info");
            columns.Add("RecordFlag");

            object param = new { OperateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), OperatePerson = userid, ListId = listid, FlowId = flowid, TbId = flowid.Replace("bi_", "tb_").Replace("fw_", "tb_"), Info = info, RecordFlag = flag };

            ComService.InsertAndReturnID("recordrun", columns, param);
        }

        public static void AddInfo(string userid, string objectid, string info)
        {
            ComService ComService = new ComService();

            List<string> columns = new List<string>();
            columns.Add("OperateTime");
            columns.Add("OperatePerson");
            columns.Add("ObjectId");
            columns.Add("Info");

            object param = new { OperateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), OperatePerson = userid, ObjectId = objectid, Info = info };

            ComService.InsertAndReturnID("recordset", columns, param);
        }
    }
}