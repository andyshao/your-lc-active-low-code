﻿using System.ComponentModel;

namespace Think9.Services.Base
{
    public enum SysParameterEnum
    {
        [Description("当前用户真实姓名")]
        sysusername,

        [Description("当前用户登陆名")]
        sysuserid,

        [Description("当前用户单位(部门)编码")]
        sysdeptno,

        [Description("当前用户单位(部门)名称")]
        sysdeptname,

        [Description("当前用户角色编码")]
        sysroleno,

        [Description("当前用户角色名称")]
        sysrolename,

        [Description("当前日期")]
        sysday,

        [Description("当前时间")]
        systimenow
    }

    public enum ValidateEnum
    {
        ////Email/电子邮箱;IdCart/身份证号码;Mobile手机号码;QQ/QQ号码;Phone/电话号码;Zip/邮政编码;
        [Description("电子邮箱")]
        Email,

        [Description("身份证号码")]
        IdCart,

        [Description("手机号码")]
        Phone,

        [Description("网址")]
        Url,

        [Description("数值")]
        Number
    }

    public enum StatusEnum
    {
        [Description("启用")]
        Yes = 1,

        [Description("禁用")]
        No = 0
    }

    public enum IsEnum
    {
        [Description("是")]
        Yes = 1,

        [Description("否")]
        No = 0
    }

    public enum DBType
    {
        [Description("MYSQL")]
        mysql,

        [Description("SQLSERVER")]
        sqlserver
    }
}