﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Think9.Models;

namespace Think9.Services.Base
{
    public class BaseController : Controller
    {
        protected const string SuccessText = "操作成功！";
        protected const string ErrorText = "操作失败！";

        //public readonly ILogger<BaseController> _logger;
        //public readonly IWebHostEnvironment _webHostEnvironment;

        //public BaseController(ILogger<BaseController> logger, IWebHostEnvironment webHostEnvironment)
        //{
        //    _logger = logger;
        //    _webHostEnvironment = webHostEnvironment;
        //    System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
        //}

        public CurrentUserEntity CurrentUser
        {
            get { return new OperatorProvider(HttpContext).GetCurrent(); }
        }

        public CurrentUserEntity GetCurrentUser()
        {
            return new OperatorProvider(HttpContext).GetCurrent();
        }

        // GET: Base
        public virtual ActionResult Index(int? id)
        {
            var _menuId = id == null ? 0 : id.Value;
            return View();
        }

        /// <summary>
        /// 操作成功
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected virtual AjaxResult SuccessTip(string message = SuccessText)
        {
            return new AjaxResult { state = ResultType.success.ToString(), message = message };
        }

        /// <summary>
        /// 操作成功 并传递数值
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected virtual AjaxResult SuccessTip(string message, string extra)
        {
            return new AjaxResult { extra = extra, state = ResultType.success.ToString(), message = message };
        }

        /// <summary>
        /// 操作成功 并传递数值
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected virtual AjaxResult SuccessTip(string message, string extra, string extra2)
        {
            return new AjaxResult { extra = extra, extra2 = extra2, state = ResultType.success.ToString(), message = message };
        }

        /// <summary>
        /// 操作成功 并传递数值
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected virtual AjaxResult SuccessTip(string message, List<ControlEntity> list, string extra)
        {
            return new AjaxResult { extra = extra, list = list, state = ResultType.success.ToString(), message = message };
        }

        /// <summary>
        /// 操作失败
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        protected virtual AjaxResult ErrorTip(string message = ErrorText)
        {
            return new AjaxResult { state = ResultType.error.ToString(), message = message };
        }
    }
}