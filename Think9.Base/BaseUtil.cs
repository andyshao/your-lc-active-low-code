﻿using System;
using System.Data;
using System.Text.Encodings.Web;
using System.Text.RegularExpressions;

namespace Think9.Services.Base
{
    public class BaseUtil
    {
        /// <summary>
        /// 子表最多列数
        /// </summary>
        public const int MaxColumn = 25;

        /// <summary>
        /// 字符分割 用于多选项的分割等 如split+选项1+split+选项2+split
        /// </summary>
        public const string ComSplit = "Ξ";

        private static string Split = "&Ξ#";

        /// <summary>
        /// 去除HTML标记
        /// </summary>
        /// <param name="NoHTML">包括HTML的源码 </param>
        /// <returns>已经去除后的文字</returns>
        public static string NoHtml(string Htmlstring)
        {
            //删除脚本
            Htmlstring = Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
            //删除HTML
            Htmlstring = Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"-->", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<!--.*", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(nbsp|#160);", " ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&#(\d+);", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&hellip;", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&mdash;", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&ldquo;", "", RegexOptions.IgnoreCase);
            Htmlstring.Replace("<", "");
            Htmlstring = Regex.Replace(Htmlstring, @"&rdquo;", "", RegexOptions.IgnoreCase);
            Htmlstring.Replace(">", "");
            Htmlstring.Replace("\r\n", "");

            Htmlstring = HtmlEncoder.Default.Encode(Htmlstring).Trim();
            return Htmlstring;
        }

        /// <summary>
        /// 把 string[] 按照分隔符组装成 string
        /// </summary>
        /// <param name="sOutput"></param>
        /// <returns></returns>
        public static string[] GetStrArray(string sOutput, string sPeater)
        {
            string[] stringSeparators = new string[] { sPeater };
            string[] stReturn = sOutput.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            return stReturn;
        }

        /// <summary>
        /// 得到树形列表 按照id前缀
        /// </summary>
        public static DataTable GetTreeDt(DataTable dtAll, string classid)
        {
            DataTable dtReturn = new DataTable("valueText");
            dtReturn.Columns.Add("ClassID", typeof(String));
            dtReturn.Columns.Add("Value", typeof(String));
            dtReturn.Columns.Add("Text", typeof(String));
            dtReturn.Columns.Add("Exa", typeof(String));

            string OldId = Split;
            string Add = "╋";
            foreach (DataRow dr in dtAll.Rows)
            {
                if (!OldId.Contains(Split + dr["id"].ToString().Trim() + Split))
                {
                    DataRow row = dtReturn.NewRow();
                    row["Value"] = dr["id"].ToString().Trim();
                    row["Text"] = Add + dr["name"].ToString().Trim();
                    row["Exa"] = dr["name"].ToString().Trim();
                    row["ClassID"] = classid;
                    dtReturn.Rows.Add(row);
                    OldId += dr["id"].ToString().Trim() + Split;

                    BaseUtil.CreateRow(ref OldId, dr["id"].ToString().Trim(), classid, dtAll, dtReturn, "├『");//
                }
            }

            return dtReturn;
        }

        /// <summary>
        /// 得到树形列表 按照id前缀
        /// </summary>
        public static void GetTreeDt(DataTable dtReturn, DataTable dtAll, string classid)
        {
            string OldId = Split;
            string Add = "╋";
            foreach (DataRow dr in dtAll.Rows)
            {
                if (!OldId.Contains(Split + dr["id"].ToString().Trim() + Split))
                {
                    DataRow row = dtReturn.NewRow();
                    row["Value"] = dr["id"].ToString().Trim();
                    row["Text"] = Add + dr["name"].ToString().Trim();
                    row["Exa"] = dr["name"].ToString().Trim();
                    row["ClassID"] = classid;
                    dtReturn.Rows.Add(row);
                    OldId += dr["id"].ToString().Trim() + Split;

                    BaseUtil.CreateRow(ref OldId, dr["id"].ToString().Trim(), classid, dtAll, dtReturn, "├『");//
                }
            }
        }

        /// <summary>
        /// 添加一个row
        /// </summary>
        private static void CreateRow(ref string strOldId, string id, string classid, DataTable dtAll, DataTable dtReturn, string strAdd)
        {
            strAdd = "---" + strAdd + "";
            foreach (DataRow dr in dtAll.Rows)
            {
                if (dr["id"].ToString().Trim().StartsWith(id) && !strOldId.Contains(Split + dr["id"].ToString().Trim() + Split))
                {
                    DataRow row = dtReturn.NewRow();
                    row["Value"] = dr["id"].ToString().Trim();
                    row["Text"] = strAdd + dr["name"].ToString() + "』";
                    row["Exa"] = dr["name"].ToString().Trim();
                    row["ClassID"] = classid;
                    dtReturn.Rows.Add(row);
                    strOldId += dr["id"].ToString().Trim() + Split;

                    BaseUtil.CreateRow(ref strOldId, dr["Id"].ToString(), classid, dtAll, dtReturn, strAdd);
                }
            }
        }

        /// <summary>
        /// 得到小数位数
        /// </summary>
        public static int GetDataDigitByDataType(string DataType)
        {
            int iDigit = 0;
            //日期
            if (DataType.Substring(0, 1) == "1")
            {
                iDigit = 0;
            }
            //文字
            if (DataType.Substring(0, 1) == "2")
            {
                iDigit = 0;
            }
            //数字
            if (DataType.Substring(0, 1) == "3")
            {
                iDigit = int.Parse(DataType.Substring(2, 2));
            }
            //图片
            if (DataType.Substring(0, 1) == "5")
            {
                iDigit = 0;
            }

            return iDigit;
        }

        /// <summary>
        /// 得到长度
        /// </summary>
        public static int GetDataMlenByDataType(string sDataTypeId)
        {
            int iMlen = 0;
            //日期
            if (sDataTypeId.Substring(0, 1) == "1")
            {
                iMlen = 8;
            }
            //文字
            if (sDataTypeId.Substring(0, 1) == "2")
            {
                iMlen = int.Parse(sDataTypeId.Substring(1, 3));
            }
            //数字
            if (sDataTypeId.Substring(0, 1) == "3")
            {
                iMlen = 9;
            }
            //图片
            if (sDataTypeId.Substring(0, 1) == "5")
            {
                iMlen = 200;
            }

            return iMlen;
        }

        public static string GetRunName(string userid, string flowid, string flname, DataTable dt = null, string info = null)
        {
            return "【" + userid + "-" + DateTime.Now.ToString("yyyyMMdd") + "】" + flname;

            //if (flowid == "***")
            //{
            //    if (dt != null)
            //    {
            //        //可在此自定义
            //    }
            //}
        }

        /// <summary>
        /// 得到编号
        /// </summary>
        public static string GetRuNumber(string flowid, int listid)
        {
            if (flowid.StartsWith("bi_"))
            {
                return flowid.Replace("bi_", "") + listid.ToString();
            }
            else
            {
                int i = (listid + 37) * 3 + 23;

                return DateTime.Now.Year.ToString() + i.ToString().PadLeft(8, '0') + DateTime.Now.Millisecond.ToString().PadLeft(4, '0');
            }
        }
    }
}