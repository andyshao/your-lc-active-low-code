﻿using System.Collections.Generic;
using System.Data;
using Think9.Repository;

namespace Think9.Services.Base
{
    public class ComService
    {
        public ComRepository ComRepository = new ComRepository();

        /// <summary>
        /// 获得数据表包含字段列表  新加的
        /// </summary>
        public DataTable GetDataTablePageList(string tableName, string files, string where, string orderby, int pageIndex, int pageSize, object param)
        {
            return ComRepository.GetDataTablePageList(tableName, files, where, orderby, pageIndex, pageSize, param);
        }

        /// <summary>
        /// 获得数据表包含字段列表  新加的
        /// </summary>
        public DataTable GetTbFieldist(string tbid)
        {
            return ComRepository.GetTbFieldist(tbid);
        }

        /// <summary>
        ///   求值 可以是正常的sql 也可以是算术表达式 新加的
        /// </summary>
        public string GetSingle(string sql)
        {
            return ComRepository.GetSingle(sql);
        }

        /// <summary>
        ///  得到最大值    新加的
        /// </summary>
        public int GetMaxID(string FieldName, string TableName, string Where)
        {
            return ComRepository.GetMaxID(FieldName, TableName, Where);
        }

        /// <summary>
        ///  返回自动增长列id  新加的
        /// </summary>
        public int InsertAndReturnID(string tableName, List<string> columns, object param)
        {
            return ComRepository.InsertAndReturnID(tableName, columns, param);
        }

        /// <summary>
        /// 判断是否存在某表的某个字段  新加的
        /// </summary>
        public bool ColumnExists(string TableName, string columnName)
        {
            return ComRepository.ColumnExists(TableName, columnName);
        }

        /// <summary>
        /// 获得数据表包含字段的字符串以#分割  新加的
        /// </summary>
        public string GetTbColumnStr(string tbid)
        {
            if (!string.IsNullOrEmpty(tbid))
            {
                return ComRepository.GetTbColumnStr(tbid);
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// 表是否存在  新加的
        /// </summary>
        public bool TabExists(string TableName)
        {
            return ComRepository.TabExists(TableName);
        }

        /// <summary>
        /// 根据录入表编码、指标编码、及类型生成数据库插入字段sql
        /// </summary>
        public string GetAddTbFieldSql(string tbid, string indexid, string datatype)
        {
            return ComRepository.GetAddTbFieldSql(tbid, indexid, datatype);
        }

        /// <summary>
        /// 根据录入表编码、指标编码、及类型生成数据库修改字段sql
        /// </summary>
        public string GetAlterTbFieldSql(string tbid, string indexid, string datatype)
        {
            return ComRepository.GetAlterTbFieldSql(tbid, indexid, datatype);
        }

        /// <summary>
        /// 根据设置的指标，生成数据表语句  新加的
        /// </summary>
        public string GetCreatDBStr(string tbid, string type, string fwid)
        {
            return ComRepository.GetCreatDBSql(tbid, type, fwid);
        }

        /// <summary>
        /// 执行sql语句 新加的
        /// </summary>
        public int ExecuteSql(string sql, object param = null)
        {
            return ComRepository.ExecuteSql(sql, param);
        }

        /// <summary>
        /// 生成自动编号数据建表语句 新加的
        /// </summary>
        public string GetCreatRuleAutoTbStr(string RuleId)
        {
            return ComRepository.GetCreatRuleAutoTbSql(RuleId);
        }

        /// <summary>
        /// 返回DataTable
        /// </summary>
        public DataTable GetDataTable(string sql, object param = null)
        {
            return ComRepository.GetDataTable(sql, param);
        }

        /// <summary>
        /// 返回DataTable
        /// </summary>
        public DataTable GetDataTable(string tbname, string some, string where, string order, object param = null)
        {
            string sql;
            if (string.IsNullOrEmpty(where))
            {
                sql = "SELECT  " + some + " FROM " + tbname + "  " + order;
            }
            else
            {
                string _where = where.ToLower().StartsWith("where ") ? where : "WHERE " + where;
                sql = "SELECT  " + some + " FROM " + tbname + "  " + _where + "  " + order;
            }
            return ComRepository.GetDataTable(sql, param);
        }

        /// <summary>
        /// 获取总数
        /// </summary>
        public long GetTotal(string tablename, string where = null, object param = null)
        {
            return ComRepository.GetTotal(tablename, where, param);
        }
    }
}