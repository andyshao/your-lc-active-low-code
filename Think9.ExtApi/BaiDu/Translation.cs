﻿namespace Think9.TransAPI
{
    public class Translation
    {
        public string Src { get; set; }
        public string Dst { get; set; }
    }
}