﻿namespace Think9.TransAPI
{
    public class TranslationResult
    {
        //错误码，翻译结果无法正常返回
        public string Error_code { get; set; }

        public string Error_msg { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Query { get; set; }

        //翻译正确，返回的结果
        //这里是数组的原因是百度翻译支持多个单词或多段文本的翻译，在发送的字段q中用换行符（\n）分隔
        public Translation[] Trans_result { get; set; }
    }
}